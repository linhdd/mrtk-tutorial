﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AnchorFeedbackScript::Awake()
extern void AnchorFeedbackScript_Awake_m8C7C5106D96097F43537AA4ED7E17629808B8BE4 (void);
// 0x00000002 System.Void AnchorFeedbackScript::Start()
extern void AnchorFeedbackScript_Start_m8633B37353754E7EA06489C1B19A7451706EE40D (void);
// 0x00000003 System.Void AnchorFeedbackScript::AnchorModuleScript_OnStartASASession()
extern void AnchorFeedbackScript_AnchorModuleScript_OnStartASASession_m1C0208D946F37B6FABDC1D8D392F40EF6162325C (void);
// 0x00000004 System.Void AnchorFeedbackScript::AnchorModuleScript_OnEndASASession()
extern void AnchorFeedbackScript_AnchorModuleScript_OnEndASASession_m44EB1F2F07C6617DDECFB86FF7806198D4DE5997 (void);
// 0x00000005 System.Void AnchorFeedbackScript::AnchorModuleScript_OnCreateAnchorStarted()
extern void AnchorFeedbackScript_AnchorModuleScript_OnCreateAnchorStarted_m3804F2E06F1481EBDD885C0EC8DDEEF974DC4025 (void);
// 0x00000006 System.Void AnchorFeedbackScript::AnchorModuleScript_OnCreateAnchorSucceeded()
extern void AnchorFeedbackScript_AnchorModuleScript_OnCreateAnchorSucceeded_m42E9F331602E2F6941EBCBB24D64A502AA4BE698 (void);
// 0x00000007 System.Void AnchorFeedbackScript::AnchorModuleScript_OnCreateAnchorFailed()
extern void AnchorFeedbackScript_AnchorModuleScript_OnCreateAnchorFailed_m6870E07E4F937ECE2AB3B4B52D4B81E01FD6CFFC (void);
// 0x00000008 System.Void AnchorFeedbackScript::AnchorModuleScript_OnCreateLocalAnchor()
extern void AnchorFeedbackScript_AnchorModuleScript_OnCreateLocalAnchor_m126A81B3DD3604B1C7AE5FFC233928DB8E1192F4 (void);
// 0x00000009 System.Void AnchorFeedbackScript::AnchorModuleScript_OnRemoveLocalAnchor()
extern void AnchorFeedbackScript_AnchorModuleScript_OnRemoveLocalAnchor_m2C10AABEB867CA1CD4EF43ACD5E3162A7034FF8D (void);
// 0x0000000A System.Void AnchorFeedbackScript::AnchorModuleScript_OnFindASAAnchor()
extern void AnchorFeedbackScript_AnchorModuleScript_OnFindASAAnchor_mDAC84A6D053DF688EACCE64D562BB052D0972765 (void);
// 0x0000000B System.Void AnchorFeedbackScript::AnchorModuleScript_OnASAAnchorLocated()
extern void AnchorFeedbackScript_AnchorModuleScript_OnASAAnchorLocated_m62B31964A1E7E3A95B76D1E6D4FBCDBC6934EB01 (void);
// 0x0000000C System.Void AnchorFeedbackScript::.ctor()
extern void AnchorFeedbackScript__ctor_m9DB66C384CAA23363AA1B58971A7D9EEB243439F (void);
// 0x0000000D System.Void AnchorModuleScript::Start()
extern void AnchorModuleScript_Start_mFBE9F27E302E395093F56B907EE29613BEDB5A4F (void);
// 0x0000000E System.Void AnchorModuleScript::Update()
extern void AnchorModuleScript_Update_m73B821FCF903C4D4996A1CCD8980859B16CECD89 (void);
// 0x0000000F System.Void AnchorModuleScript::OnDestroy()
extern void AnchorModuleScript_OnDestroy_m5E5C1EF97883D304B71AF7DA2DC44722CD541F0D (void);
// 0x00000010 System.Void AnchorModuleScript::StartAzureSession()
extern void AnchorModuleScript_StartAzureSession_mE0DE4CD1D9D1AC9A39CB3639AD349FE34A0CCDB2 (void);
// 0x00000011 System.Void AnchorModuleScript::StopAzureSession()
extern void AnchorModuleScript_StopAzureSession_mC6B5843F849B5644C1284327C4A5FDE3476F6A4A (void);
// 0x00000012 System.Void AnchorModuleScript::CreateAzureAnchor(UnityEngine.GameObject)
extern void AnchorModuleScript_CreateAzureAnchor_mC1B80FAF523304BC6F8CF3569EA530D35285A53B (void);
// 0x00000013 System.Void AnchorModuleScript::RemoveLocalAnchor(UnityEngine.GameObject)
extern void AnchorModuleScript_RemoveLocalAnchor_m778A9ACBF51C57013126B2D77DC7D66EE7029919 (void);
// 0x00000014 System.Void AnchorModuleScript::FindAzureAnchor(System.String)
extern void AnchorModuleScript_FindAzureAnchor_m196572032AD1ABFDC99646EB5EC5CADE8B2E33F8 (void);
// 0x00000015 System.Void AnchorModuleScript::DeleteAzureAnchor()
extern void AnchorModuleScript_DeleteAzureAnchor_mED2B07FEDB0CA7B49E2F58B838C699F2051689BE (void);
// 0x00000016 System.Void AnchorModuleScript::SaveAzureAnchorIdToDisk()
extern void AnchorModuleScript_SaveAzureAnchorIdToDisk_mDC61452065EB769DD04D8917764C278E470E7A37 (void);
// 0x00000017 System.Void AnchorModuleScript::GetAzureAnchorIdFromDisk()
extern void AnchorModuleScript_GetAzureAnchorIdFromDisk_mFDCEE50EC71AC86EFA284416C6A82C4427911582 (void);
// 0x00000018 System.Void AnchorModuleScript::ShareAzureAnchorIdToNetwork()
extern void AnchorModuleScript_ShareAzureAnchorIdToNetwork_m4FA0D227017FDF14ABC7FE515FAFCA704ACC7C87 (void);
// 0x00000019 System.Void AnchorModuleScript::GetAzureAnchorIdFromNetwork()
extern void AnchorModuleScript_GetAzureAnchorIdFromNetwork_mD30FB0E386DC4A73B668C1147935F208B99D0479 (void);
// 0x0000001A System.Void AnchorModuleScript::CloudManager_AnchorLocated(System.Object,Microsoft.Azure.SpatialAnchors.AnchorLocatedEventArgs)
extern void AnchorModuleScript_CloudManager_AnchorLocated_m4901EA6011DEB0AB2D36E65CA39854E9F9D4AF26 (void);
// 0x0000001B System.Void AnchorModuleScript::QueueOnUpdate(System.Action)
extern void AnchorModuleScript_QueueOnUpdate_m6CCB426D379767DBB1876F81F7510C0F16465F8D (void);
// 0x0000001C System.Collections.IEnumerator AnchorModuleScript::ShareAzureAnchorIdToNetworkCoroutine()
extern void AnchorModuleScript_ShareAzureAnchorIdToNetworkCoroutine_m8043DC685DD62288B7A367E5F3A0905D489B8BCD (void);
// 0x0000001D System.Collections.IEnumerator AnchorModuleScript::GetSharedAzureAnchorIDCoroutine(System.String)
extern void AnchorModuleScript_GetSharedAzureAnchorIDCoroutine_mD8F93F5CFE0735475DFC9812335C117414208457 (void);
// 0x0000001E System.Void AnchorModuleScript::add_OnStartASASession(AnchorModuleScript/StartASASessionDelegate)
extern void AnchorModuleScript_add_OnStartASASession_m9E1F126DFF9DE26602B62250B2525EF3FD8873EA (void);
// 0x0000001F System.Void AnchorModuleScript::remove_OnStartASASession(AnchorModuleScript/StartASASessionDelegate)
extern void AnchorModuleScript_remove_OnStartASASession_m7A99EB51CB4AA53E306BD7161A054466CC14CB0F (void);
// 0x00000020 System.Void AnchorModuleScript::add_OnEndASASession(AnchorModuleScript/EndASASessionDelegate)
extern void AnchorModuleScript_add_OnEndASASession_mC42AB0B84878682D0610DBBD8DE563ED101BAB8D (void);
// 0x00000021 System.Void AnchorModuleScript::remove_OnEndASASession(AnchorModuleScript/EndASASessionDelegate)
extern void AnchorModuleScript_remove_OnEndASASession_m48A1D28B60921A565048325984084CF05899550D (void);
// 0x00000022 System.Void AnchorModuleScript::add_OnCreateAnchorStarted(AnchorModuleScript/CreateAnchorDelegate)
extern void AnchorModuleScript_add_OnCreateAnchorStarted_m290AEAF30C103B0FE2F2954261B161122AF460CC (void);
// 0x00000023 System.Void AnchorModuleScript::remove_OnCreateAnchorStarted(AnchorModuleScript/CreateAnchorDelegate)
extern void AnchorModuleScript_remove_OnCreateAnchorStarted_mF4734B1784E83F0A6DE263BF832ADFA1D6AA8891 (void);
// 0x00000024 System.Void AnchorModuleScript::add_OnCreateAnchorSucceeded(AnchorModuleScript/CreateAnchorDelegate)
extern void AnchorModuleScript_add_OnCreateAnchorSucceeded_m2B97E028C8CB1F40CA8C470F6A705D406FBAAA8F (void);
// 0x00000025 System.Void AnchorModuleScript::remove_OnCreateAnchorSucceeded(AnchorModuleScript/CreateAnchorDelegate)
extern void AnchorModuleScript_remove_OnCreateAnchorSucceeded_m597DCF9B72826687D8AB6B56577C6626460949C1 (void);
// 0x00000026 System.Void AnchorModuleScript::add_OnCreateAnchorFailed(AnchorModuleScript/CreateAnchorDelegate)
extern void AnchorModuleScript_add_OnCreateAnchorFailed_m0D63C171B5CDE942BD811EEB4BAE607BF2B706A0 (void);
// 0x00000027 System.Void AnchorModuleScript::remove_OnCreateAnchorFailed(AnchorModuleScript/CreateAnchorDelegate)
extern void AnchorModuleScript_remove_OnCreateAnchorFailed_mBEF0CF8B5AF568E0547729AB5D42036BBD557C40 (void);
// 0x00000028 System.Void AnchorModuleScript::add_OnCreateLocalAnchor(AnchorModuleScript/CreateLocalAnchorDelegate)
extern void AnchorModuleScript_add_OnCreateLocalAnchor_m90DC5E7F24EBADD06F73A7DA8DD24820AB9428CB (void);
// 0x00000029 System.Void AnchorModuleScript::remove_OnCreateLocalAnchor(AnchorModuleScript/CreateLocalAnchorDelegate)
extern void AnchorModuleScript_remove_OnCreateLocalAnchor_mF30CF49094AB87B4EDD5B83DC1D8A1F2423575DF (void);
// 0x0000002A System.Void AnchorModuleScript::add_OnRemoveLocalAnchor(AnchorModuleScript/RemoveLocalAnchorDelegate)
extern void AnchorModuleScript_add_OnRemoveLocalAnchor_m8C730185093BB514F7EBFFEC742F4EEE222C4E6F (void);
// 0x0000002B System.Void AnchorModuleScript::remove_OnRemoveLocalAnchor(AnchorModuleScript/RemoveLocalAnchorDelegate)
extern void AnchorModuleScript_remove_OnRemoveLocalAnchor_m72122C75147589814951FAD4ACF15EF5BE5CD41A (void);
// 0x0000002C System.Void AnchorModuleScript::add_OnFindASAAnchor(AnchorModuleScript/FindAnchorDelegate)
extern void AnchorModuleScript_add_OnFindASAAnchor_m1FDC5B6B678C0C1C033466257B43B08C087590EB (void);
// 0x0000002D System.Void AnchorModuleScript::remove_OnFindASAAnchor(AnchorModuleScript/FindAnchorDelegate)
extern void AnchorModuleScript_remove_OnFindASAAnchor_mA25D92B8EFCA5557F3575F446747741EAAC888C5 (void);
// 0x0000002E System.Void AnchorModuleScript::add_OnASAAnchorLocated(AnchorModuleScript/AnchorLocatedDelegate)
extern void AnchorModuleScript_add_OnASAAnchorLocated_m9912B081D7CF6F7BFB8A9D82C7FDD3A893C38FAC (void);
// 0x0000002F System.Void AnchorModuleScript::remove_OnASAAnchorLocated(AnchorModuleScript/AnchorLocatedDelegate)
extern void AnchorModuleScript_remove_OnASAAnchorLocated_m36541F05B4C456E131368E31E6439301F74928EE (void);
// 0x00000030 System.Void AnchorModuleScript::add_OnDeleteASAAnchor(AnchorModuleScript/DeleteASAAnchorDelegate)
extern void AnchorModuleScript_add_OnDeleteASAAnchor_m1884E831956AB59EC774AB213EF4EEA8985DEF32 (void);
// 0x00000031 System.Void AnchorModuleScript::remove_OnDeleteASAAnchor(AnchorModuleScript/DeleteASAAnchorDelegate)
extern void AnchorModuleScript_remove_OnDeleteASAAnchor_m65E2D188E27BDCE82ABAD2430C113E957C7AAAE0 (void);
// 0x00000032 System.Void AnchorModuleScript::.ctor()
extern void AnchorModuleScript__ctor_m758023C408A657AD13AF0FCD075D9720CDDC4211 (void);
// 0x00000033 System.Void AnchorModuleScript/StartASASessionDelegate::.ctor(System.Object,System.IntPtr)
extern void StartASASessionDelegate__ctor_m9D427688C3705D2DBC2B1F1B512AD63DC630174E (void);
// 0x00000034 System.Void AnchorModuleScript/StartASASessionDelegate::Invoke()
extern void StartASASessionDelegate_Invoke_m45223B8A76264C5AD2C10E0068BB1E6115C74898 (void);
// 0x00000035 System.IAsyncResult AnchorModuleScript/StartASASessionDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void StartASASessionDelegate_BeginInvoke_mD2D1E587CACD9D0F043027E0D2B72DD4FEFB68C3 (void);
// 0x00000036 System.Void AnchorModuleScript/StartASASessionDelegate::EndInvoke(System.IAsyncResult)
extern void StartASASessionDelegate_EndInvoke_mB711351D9CEA770550C66456BBA17E1A7066E113 (void);
// 0x00000037 System.Void AnchorModuleScript/EndASASessionDelegate::.ctor(System.Object,System.IntPtr)
extern void EndASASessionDelegate__ctor_mCEB47632974ECB3802740517303F1784F27FC33E (void);
// 0x00000038 System.Void AnchorModuleScript/EndASASessionDelegate::Invoke()
extern void EndASASessionDelegate_Invoke_m6C360E6B6D453CB6909E259993C990802BA98613 (void);
// 0x00000039 System.IAsyncResult AnchorModuleScript/EndASASessionDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void EndASASessionDelegate_BeginInvoke_m052A51473B60FE826B9A7BA832A1C678752833D9 (void);
// 0x0000003A System.Void AnchorModuleScript/EndASASessionDelegate::EndInvoke(System.IAsyncResult)
extern void EndASASessionDelegate_EndInvoke_m1F7184B0119F0C0E09404D1DD0C84039BD51468E (void);
// 0x0000003B System.Void AnchorModuleScript/CreateAnchorDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateAnchorDelegate__ctor_m5FB74DEA4AE88293C06705290538C53F870404BA (void);
// 0x0000003C System.Void AnchorModuleScript/CreateAnchorDelegate::Invoke()
extern void CreateAnchorDelegate_Invoke_m04854CF72C1189DF92AC3721645745CAC0A5A46D (void);
// 0x0000003D System.IAsyncResult AnchorModuleScript/CreateAnchorDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void CreateAnchorDelegate_BeginInvoke_m7223440DD7A6138AD1A2006F66CBBF828CFB1EE0 (void);
// 0x0000003E System.Void AnchorModuleScript/CreateAnchorDelegate::EndInvoke(System.IAsyncResult)
extern void CreateAnchorDelegate_EndInvoke_mD6CB12A5C01C1969420EAD6114AF1A9A972FCE04 (void);
// 0x0000003F System.Void AnchorModuleScript/CreateLocalAnchorDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateLocalAnchorDelegate__ctor_mAAD201999767CC679BB6938B1DB042DE0E3E65BD (void);
// 0x00000040 System.Void AnchorModuleScript/CreateLocalAnchorDelegate::Invoke()
extern void CreateLocalAnchorDelegate_Invoke_m567AD8D9AA96A42F930DC5CE80FEFBB6C3B8288F (void);
// 0x00000041 System.IAsyncResult AnchorModuleScript/CreateLocalAnchorDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void CreateLocalAnchorDelegate_BeginInvoke_m312DDDD72D5C559323A044C7497C4C81A184E9AF (void);
// 0x00000042 System.Void AnchorModuleScript/CreateLocalAnchorDelegate::EndInvoke(System.IAsyncResult)
extern void CreateLocalAnchorDelegate_EndInvoke_m8927B83EF93B4B9F3FBCEA1E825A350094713A0D (void);
// 0x00000043 System.Void AnchorModuleScript/RemoveLocalAnchorDelegate::.ctor(System.Object,System.IntPtr)
extern void RemoveLocalAnchorDelegate__ctor_m3583EDF10B64499FECF07E3906CB8E8CDAF7084C (void);
// 0x00000044 System.Void AnchorModuleScript/RemoveLocalAnchorDelegate::Invoke()
extern void RemoveLocalAnchorDelegate_Invoke_mDE3FC06E7812F6374D403B629D800D6FD2C2F1BD (void);
// 0x00000045 System.IAsyncResult AnchorModuleScript/RemoveLocalAnchorDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void RemoveLocalAnchorDelegate_BeginInvoke_m1445301D7DF64AA6D908750360E94870BFE0A148 (void);
// 0x00000046 System.Void AnchorModuleScript/RemoveLocalAnchorDelegate::EndInvoke(System.IAsyncResult)
extern void RemoveLocalAnchorDelegate_EndInvoke_mCA219DBEE9E2D466B6BF2DAB7B7BCF7A0AF6EC4D (void);
// 0x00000047 System.Void AnchorModuleScript/FindAnchorDelegate::.ctor(System.Object,System.IntPtr)
extern void FindAnchorDelegate__ctor_m4E4AE8E63A5932F12B26B246C37EFBC5CDD170E8 (void);
// 0x00000048 System.Void AnchorModuleScript/FindAnchorDelegate::Invoke()
extern void FindAnchorDelegate_Invoke_mFDF2FE513B8150280949A5B20B456DEFB11D70C3 (void);
// 0x00000049 System.IAsyncResult AnchorModuleScript/FindAnchorDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void FindAnchorDelegate_BeginInvoke_mE0AF6C5526E5D65CEC381888AEB28F658EAFAC48 (void);
// 0x0000004A System.Void AnchorModuleScript/FindAnchorDelegate::EndInvoke(System.IAsyncResult)
extern void FindAnchorDelegate_EndInvoke_m560106CE5454CD670233A809653CF03125D135E6 (void);
// 0x0000004B System.Void AnchorModuleScript/AnchorLocatedDelegate::.ctor(System.Object,System.IntPtr)
extern void AnchorLocatedDelegate__ctor_m65D7C380783E0F5B49F4D59E5D2AD88969A65AF1 (void);
// 0x0000004C System.Void AnchorModuleScript/AnchorLocatedDelegate::Invoke()
extern void AnchorLocatedDelegate_Invoke_mFAB4D550DD52EC5CB81467911F20F3AE0645346B (void);
// 0x0000004D System.IAsyncResult AnchorModuleScript/AnchorLocatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void AnchorLocatedDelegate_BeginInvoke_mC1E0AF54A704F3C0642AA23FD947633A02364E53 (void);
// 0x0000004E System.Void AnchorModuleScript/AnchorLocatedDelegate::EndInvoke(System.IAsyncResult)
extern void AnchorLocatedDelegate_EndInvoke_m44D4D7E6BB3358A3CAB46BDFECC4F34309FAF4CA (void);
// 0x0000004F System.Void AnchorModuleScript/DeleteASAAnchorDelegate::.ctor(System.Object,System.IntPtr)
extern void DeleteASAAnchorDelegate__ctor_m207B762CC3BF99A4B0DCB3C33B23096C4CD7ECC5 (void);
// 0x00000050 System.Void AnchorModuleScript/DeleteASAAnchorDelegate::Invoke()
extern void DeleteASAAnchorDelegate_Invoke_mCB11AF4379DB50FC161DF2DCD5F09B43CB951F6E (void);
// 0x00000051 System.IAsyncResult AnchorModuleScript/DeleteASAAnchorDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void DeleteASAAnchorDelegate_BeginInvoke_mF6907845BDFCEEFEF74ADFAAF08CBF2CEE43ADE4 (void);
// 0x00000052 System.Void AnchorModuleScript/DeleteASAAnchorDelegate::EndInvoke(System.IAsyncResult)
extern void DeleteASAAnchorDelegate_EndInvoke_m5A19B52E0D9C8C049A75944826FA38610435DE9D (void);
// 0x00000053 System.Void AnchorModuleScript/<StartAzureSession>d__11::MoveNext()
extern void U3CStartAzureSessionU3Ed__11_MoveNext_m096E8B5FFFFF21934958199F5E704B0A35030F86 (void);
// 0x00000054 System.Void AnchorModuleScript/<StartAzureSession>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartAzureSessionU3Ed__11_SetStateMachine_mC14F49D42F4196CE29AC0BB8D170C8822B39EDBA (void);
// 0x00000055 System.Void AnchorModuleScript/<StopAzureSession>d__12::MoveNext()
extern void U3CStopAzureSessionU3Ed__12_MoveNext_mB56F54C704AA91C36474482A1D72AD4ADCCECDBF (void);
// 0x00000056 System.Void AnchorModuleScript/<StopAzureSession>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStopAzureSessionU3Ed__12_SetStateMachine_m7026DDC7EDB620D4ADDD1EC4AB67208C3067FEC1 (void);
// 0x00000057 System.Void AnchorModuleScript/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m999AF18E0C633FF39F108F9997E3C306D9EC4111 (void);
// 0x00000058 System.Void AnchorModuleScript/<>c__DisplayClass13_0::<CreateAzureAnchor>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CCreateAzureAnchorU3Eb__0_m1A18F3105143BA74FD4DAD42106E0401F7689A8C (void);
// 0x00000059 System.Void AnchorModuleScript/<CreateAzureAnchor>d__13::MoveNext()
extern void U3CCreateAzureAnchorU3Ed__13_MoveNext_mC928D721B690A52C63E28A6F87DCA0B85C4016EB (void);
// 0x0000005A System.Void AnchorModuleScript/<CreateAzureAnchor>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateAzureAnchorU3Ed__13_SetStateMachine_m1CB417E804D0F9776B38FE512446F426683AEC70 (void);
// 0x0000005B System.Void AnchorModuleScript/<DeleteAzureAnchor>d__16::MoveNext()
extern void U3CDeleteAzureAnchorU3Ed__16_MoveNext_m57179936926C706B648D169308C85A22C5F69FD9 (void);
// 0x0000005C System.Void AnchorModuleScript/<DeleteAzureAnchor>d__16::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDeleteAzureAnchorU3Ed__16_SetStateMachine_m81A308F54E85613A92D46E6623D04E5ADFD383C5 (void);
// 0x0000005D System.Void AnchorModuleScript/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mCC87BAED3EC57EBA53AA64FB246DABBFD1DD71AD (void);
// 0x0000005E System.Void AnchorModuleScript/<>c__DisplayClass21_0::<CloudManager_AnchorLocated>b__1()
extern void U3CU3Ec__DisplayClass21_0_U3CCloudManager_AnchorLocatedU3Eb__1_m37751B6CA50FFFC0FB18973E0AB2C08E70FE6930 (void);
// 0x0000005F System.Void AnchorModuleScript/<>c__DisplayClass21_0::<CloudManager_AnchorLocated>b__2()
extern void U3CU3Ec__DisplayClass21_0_U3CCloudManager_AnchorLocatedU3Eb__2_mD6A7F8D836409E1FA099392BA6B63B1AE1537B2B (void);
// 0x00000060 System.Void AnchorModuleScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m5AC2E6FC709218D26C6DD25B9129C8530A7E2D1D (void);
// 0x00000061 System.Void AnchorModuleScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m536152BE87B22D0313FBECB2D9062B85BB2B09FB (void);
// 0x00000062 System.Void AnchorModuleScript/<>c::<CloudManager_AnchorLocated>b__21_0()
extern void U3CU3Ec_U3CCloudManager_AnchorLocatedU3Eb__21_0_m356E27F42FF9C0FFEBDD6649EF07643130A54FDD (void);
// 0x00000063 System.Void AnchorModuleScript/<ShareAzureAnchorIdToNetworkCoroutine>d__23::.ctor(System.Int32)
extern void U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23__ctor_m6C041396A7D4FC8EBABA2129E8C77E1CF2E758EF (void);
// 0x00000064 System.Void AnchorModuleScript/<ShareAzureAnchorIdToNetworkCoroutine>d__23::System.IDisposable.Dispose()
extern void U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_System_IDisposable_Dispose_m2157092318FCCFF36DE8345E71BD60E0772E2BED (void);
// 0x00000065 System.Boolean AnchorModuleScript/<ShareAzureAnchorIdToNetworkCoroutine>d__23::MoveNext()
extern void U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_MoveNext_m8B29D15F7EBA0023CB6FD0E1FC9FA2A301A4B617 (void);
// 0x00000066 System.Object AnchorModuleScript/<ShareAzureAnchorIdToNetworkCoroutine>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49B75272B861E6173A999958AA0F6408D75E52D5 (void);
// 0x00000067 System.Void AnchorModuleScript/<ShareAzureAnchorIdToNetworkCoroutine>d__23::System.Collections.IEnumerator.Reset()
extern void U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_System_Collections_IEnumerator_Reset_mA948E2B23CA6BA09C43E944E99B1F3576A06437E (void);
// 0x00000068 System.Object AnchorModuleScript/<ShareAzureAnchorIdToNetworkCoroutine>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_System_Collections_IEnumerator_get_Current_m1AA3E2E5F17A260616C403C9553784B6F9F146B7 (void);
// 0x00000069 System.Void AnchorModuleScript/<GetSharedAzureAnchorIDCoroutine>d__24::.ctor(System.Int32)
extern void U3CGetSharedAzureAnchorIDCoroutineU3Ed__24__ctor_m167833FAEDD66EB43B9E52337A1388757894F307 (void);
// 0x0000006A System.Void AnchorModuleScript/<GetSharedAzureAnchorIDCoroutine>d__24::System.IDisposable.Dispose()
extern void U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_System_IDisposable_Dispose_m31FA364EBDCD64EA44E2CC9022A11BAAC7DCC9D2 (void);
// 0x0000006B System.Boolean AnchorModuleScript/<GetSharedAzureAnchorIDCoroutine>d__24::MoveNext()
extern void U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_MoveNext_mBEE753F6A9B63B03B7C08CAD4824382C8A0E3518 (void);
// 0x0000006C System.Void AnchorModuleScript/<GetSharedAzureAnchorIDCoroutine>d__24::<>m__Finally1()
extern void U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_U3CU3Em__Finally1_m1B3A37A8E00196BD5CE63FCFBC4C6AC6CAB7D96C (void);
// 0x0000006D System.Object AnchorModuleScript/<GetSharedAzureAnchorIDCoroutine>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C570B293C649BAF8E80AF9F799BE61DA72F2304 (void);
// 0x0000006E System.Void AnchorModuleScript/<GetSharedAzureAnchorIDCoroutine>d__24::System.Collections.IEnumerator.Reset()
extern void U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_System_Collections_IEnumerator_Reset_m96D368E30405AEA9B584C9013FFF43BB02BFAFE1 (void);
// 0x0000006F System.Object AnchorModuleScript/<GetSharedAzureAnchorIDCoroutine>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_System_Collections_IEnumerator_get_Current_mFB137B6F28CC285B8DABB29395499389EB924D0C (void);
// 0x00000070 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7 (void);
// 0x00000071 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46 (void);
// 0x00000072 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722 (void);
// 0x00000073 System.Void ChatController::.ctor()
extern void ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719 (void);
// 0x00000074 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F (void);
// 0x00000075 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B (void);
// 0x00000076 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435 (void);
// 0x00000077 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9 (void);
// 0x00000078 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138 (void);
// 0x00000079 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2 (void);
// 0x0000007A System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C (void);
// 0x0000007B System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0 (void);
// 0x0000007C System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565 (void);
// 0x0000007D System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52 (void);
// 0x0000007E System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA (void);
// 0x0000007F System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9 (void);
// 0x00000080 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0 (void);
// 0x00000081 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3 (void);
// 0x00000082 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D (void);
// 0x00000083 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3 (void);
// 0x00000084 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C (void);
// 0x00000085 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82 (void);
// 0x00000086 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5 (void);
// 0x00000087 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C (void);
// 0x00000088 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3 (void);
// 0x00000089 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908 (void);
// 0x0000008A System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427 (void);
// 0x0000008B TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D (void);
// 0x0000008C System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF (void);
// 0x0000008D System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052 (void);
// 0x0000008E System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1 (void);
// 0x0000008F System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A (void);
// 0x00000090 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6 (void);
// 0x00000091 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189 (void);
// 0x00000092 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475 (void);
// 0x00000093 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33 (void);
// 0x00000094 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6 (void);
// 0x00000095 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02 (void);
// 0x00000096 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB (void);
// 0x00000097 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E (void);
// 0x00000098 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963 (void);
// 0x00000099 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2 (void);
// 0x0000009A System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2 (void);
// 0x0000009B System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC (void);
// 0x0000009C System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9 (void);
// 0x0000009D System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4 (void);
// 0x0000009E System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69 (void);
// 0x0000009F System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D (void);
// 0x000000A0 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9 (void);
// 0x000000A1 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6 (void);
// 0x000000A2 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A (void);
// 0x000000A3 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD (void);
// 0x000000A4 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA (void);
// 0x000000A5 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB (void);
// 0x000000A6 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A (void);
// 0x000000A7 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC (void);
// 0x000000A8 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9 (void);
// 0x000000A9 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46 (void);
// 0x000000AA System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA (void);
// 0x000000AB System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A (void);
// 0x000000AC System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29 (void);
// 0x000000AD System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1 (void);
// 0x000000AE System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3 (void);
// 0x000000AF System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D (void);
// 0x000000B0 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6 (void);
// 0x000000B1 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90 (void);
// 0x000000B2 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27 (void);
// 0x000000B3 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788 (void);
// 0x000000B4 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5 (void);
// 0x000000B5 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F (void);
// 0x000000B6 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F (void);
// 0x000000B7 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878 (void);
// 0x000000B8 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72 (void);
// 0x000000B9 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695 (void);
// 0x000000BA System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A (void);
// 0x000000BB System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371 (void);
// 0x000000BC System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88 (void);
// 0x000000BD System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56 (void);
// 0x000000BE System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B (void);
// 0x000000BF System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D (void);
// 0x000000C0 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71 (void);
// 0x000000C1 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC (void);
// 0x000000C2 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95 (void);
// 0x000000C3 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA (void);
// 0x000000C4 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA (void);
// 0x000000C5 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8 (void);
// 0x000000C6 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE (void);
// 0x000000C7 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807 (void);
// 0x000000C8 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93 (void);
// 0x000000C9 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690 (void);
// 0x000000CA UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB (void);
// 0x000000CB System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362 (void);
// 0x000000CC System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22 (void);
// 0x000000CD System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2 (void);
// 0x000000CE System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51 (void);
// 0x000000CF System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E (void);
// 0x000000D0 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB (void);
// 0x000000D1 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5 (void);
// 0x000000D2 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5 (void);
// 0x000000D3 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D (void);
// 0x000000D4 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6 (void);
// 0x000000D5 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0 (void);
// 0x000000D6 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA (void);
// 0x000000D7 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2 (void);
// 0x000000D8 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45 (void);
// 0x000000D9 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA (void);
// 0x000000DA System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9 (void);
// 0x000000DB System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8 (void);
// 0x000000DC System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C (void);
// 0x000000DD System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39 (void);
// 0x000000DE System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95 (void);
// 0x000000DF System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689 (void);
// 0x000000E0 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A (void);
// 0x000000E1 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52 (void);
// 0x000000E2 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1 (void);
// 0x000000E3 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025 (void);
// 0x000000E4 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45 (void);
// 0x000000E5 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B (void);
// 0x000000E6 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE (void);
// 0x000000E7 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854 (void);
// 0x000000E8 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3 (void);
// 0x000000E9 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186 (void);
// 0x000000EA System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC (void);
// 0x000000EB System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5 (void);
// 0x000000EC System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C (void);
// 0x000000ED System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683 (void);
// 0x000000EE System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D (void);
// 0x000000EF System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E (void);
// 0x000000F0 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2 (void);
// 0x000000F1 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49 (void);
// 0x000000F2 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148 (void);
// 0x000000F3 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE (void);
// 0x000000F4 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E (void);
// 0x000000F5 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557 (void);
// 0x000000F6 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0 (void);
// 0x000000F7 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB (void);
// 0x000000F8 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8 (void);
// 0x000000F9 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A (void);
// 0x000000FA System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797 (void);
// 0x000000FB System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54 (void);
// 0x000000FC System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923 (void);
// 0x000000FD System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B (void);
// 0x000000FE System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4 (void);
// 0x000000FF System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041 (void);
// 0x00000100 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1 (void);
// 0x00000101 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55 (void);
// 0x00000102 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D (void);
// 0x00000103 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66 (void);
// 0x00000104 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233 (void);
// 0x00000105 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084 (void);
// 0x00000106 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C (void);
// 0x00000107 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839 (void);
// 0x00000108 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063 (void);
// 0x00000109 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1 (void);
// 0x0000010A System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A (void);
// 0x0000010B System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C (void);
// 0x0000010C System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A (void);
// 0x0000010D System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079 (void);
// 0x0000010E System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723 (void);
// 0x0000010F System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43 (void);
// 0x00000110 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E (void);
// 0x00000111 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6 (void);
// 0x00000112 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE (void);
// 0x00000113 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215 (void);
// 0x00000114 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79 (void);
// 0x00000115 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559 (void);
// 0x00000116 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779 (void);
// 0x00000117 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF (void);
// 0x00000118 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E (void);
// 0x00000119 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797 (void);
// 0x0000011A System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B (void);
// 0x0000011B System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A (void);
// 0x0000011C System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503 (void);
// 0x0000011D System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE (void);
// 0x0000011E System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87 (void);
// 0x0000011F System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B (void);
// 0x00000120 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D (void);
// 0x00000121 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE (void);
// 0x00000122 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF (void);
// 0x00000123 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98 (void);
// 0x00000124 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65 (void);
// 0x00000125 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97 (void);
// 0x00000126 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772 (void);
// 0x00000127 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398 (void);
// 0x00000128 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301 (void);
// 0x00000129 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491 (void);
// 0x0000012A System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F (void);
// 0x0000012B System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3 (void);
// 0x0000012C System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36 (void);
// 0x0000012D System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848 (void);
// 0x0000012E System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1 (void);
// 0x0000012F System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B (void);
// 0x00000130 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC (void);
// 0x00000131 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1 (void);
// 0x00000132 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262 (void);
// 0x00000133 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF (void);
// 0x00000134 System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60 (void);
// 0x00000135 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1 (void);
// 0x00000136 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899 (void);
// 0x00000137 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C (void);
// 0x00000138 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C (void);
// 0x00000139 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354 (void);
// 0x0000013A System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2 (void);
// 0x0000013B System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76 (void);
// 0x0000013C System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8 (void);
// 0x0000013D System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88 (void);
// 0x0000013E System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D (void);
// 0x0000013F System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A (void);
// 0x00000140 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8 (void);
// 0x00000141 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273 (void);
// 0x00000142 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405 (void);
// 0x00000143 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0 (void);
// 0x00000144 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9 (void);
// 0x00000145 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867 (void);
// 0x00000146 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304 (void);
// 0x00000147 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23 (void);
// 0x00000148 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E (void);
// 0x00000149 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77 (void);
// 0x0000014A System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38 (void);
// 0x0000014B System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB (void);
// 0x0000014C System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A (void);
// 0x0000014D System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7 (void);
// 0x0000014E System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934 (void);
// 0x0000014F System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE (void);
// 0x00000150 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90 (void);
// 0x00000151 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B (void);
// 0x00000152 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E (void);
// 0x00000153 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21 (void);
// 0x00000154 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5 (void);
// 0x00000155 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1 (void);
// 0x00000156 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D (void);
// 0x00000157 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F (void);
// 0x00000158 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198 (void);
// 0x00000159 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D (void);
// 0x0000015A System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8 (void);
// 0x0000015B System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348 (void);
// 0x0000015C System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845 (void);
// 0x0000015D System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A (void);
// 0x0000015E System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0 (void);
// 0x0000015F System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0 (void);
// 0x00000160 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47 (void);
// 0x00000161 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47 (void);
// 0x00000162 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB (void);
// 0x00000163 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7 (void);
// 0x00000164 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0 (void);
// 0x00000165 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422 (void);
// 0x00000166 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A (void);
// 0x00000167 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A (void);
// 0x00000168 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6 (void);
// 0x00000169 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E (void);
// 0x0000016A System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E (void);
// 0x0000016B System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B (void);
// 0x0000016C System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821 (void);
// 0x0000016D System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C (void);
// 0x0000016E System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F (void);
// 0x0000016F System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809 (void);
// 0x00000170 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF (void);
// 0x00000171 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98 (void);
// 0x00000172 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795 (void);
// 0x00000173 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB (void);
// 0x00000174 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53 (void);
// 0x00000175 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02 (void);
// 0x00000176 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3 (void);
// 0x00000177 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42 (void);
// 0x00000178 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19 (void);
// 0x00000179 Microsoft.MixedReality.Toolkit.Dwell.DwellHandler Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::get_DwellHandler()
extern void BaseDwellSample_get_DwellHandler_m2098A2AD0AC23E583BAD69EFF25D63BAB03AD8D8 (void);
// 0x0000017A System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::set_DwellHandler(Microsoft.MixedReality.Toolkit.Dwell.DwellHandler)
extern void BaseDwellSample_set_DwellHandler_mA13C8281843B65F232805CE69962EAF37307A090 (void);
// 0x0000017B System.Boolean Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::get_IsDwelling()
extern void BaseDwellSample_get_IsDwelling_mAFB7EEEDBF99E3CDF7CF08BD6F9A14FE867C4EAD (void);
// 0x0000017C System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::set_IsDwelling(System.Boolean)
extern void BaseDwellSample_set_IsDwelling_mA8F2120E1D25CD3692EC4B8D594B6AA39DE71742 (void);
// 0x0000017D System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::Awake()
extern void BaseDwellSample_Awake_m8DDDC7BFB655CB5B9A0A8370C66892BEC288BA62 (void);
// 0x0000017E System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellStarted_m30E7E408EF442CEA9AFB7747260944CCBE37D5B9 (void);
// 0x0000017F System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellIntended(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellIntended_mA5CC33B44F7BE0491EA3F88A504F2C1647596C40 (void);
// 0x00000180 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellCanceled_m7C036A8AC4593510B2A1B6383BC3535B4CF94C81 (void);
// 0x00000181 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellCompleted_m7962EC121A5E4561000D9316100224109E7402AD (void);
// 0x00000182 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::ButtonExecute()
extern void BaseDwellSample_ButtonExecute_m23A7395A20886C88FF24EBF7232D8F766EB8F4B9 (void);
// 0x00000183 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::.ctor()
extern void BaseDwellSample__ctor_m123AEDB828DB2A723807218F667DFE90B59883FA (void);
// 0x00000184 System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::Update()
extern void InstantDwellSample_Update_m6AEF94A44BD0C995B3A7649B305C29A37770F43F (void);
// 0x00000185 System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void InstantDwellSample_DwellCompleted_m4B9A0A068D6E739C947F55DB4156D4CD1838DF79 (void);
// 0x00000186 System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::ButtonExecute()
extern void InstantDwellSample_ButtonExecute_m6CCDCA462AA91ABB0DE2D8E280662F6B4D3E4885 (void);
// 0x00000187 System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::.ctor()
extern void InstantDwellSample__ctor_m12EFAE67E2B1D9767F06B4940B4EC86C7FC30711 (void);
// 0x00000188 System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::Awake()
extern void ListItemDwell_Awake_m920339120548A256F675E5EBA460ED62BF033E2C (void);
// 0x00000189 System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::Update()
extern void ListItemDwell_Update_m5663E17AFABCCDDC895EB45CFA19E134AAF2B53A (void);
// 0x0000018A System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ListItemDwell_DwellCompleted_m5B5121D3A3CE81586640096AE6AECDAFC7AB5933 (void);
// 0x0000018B System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::ButtonExecute()
extern void ListItemDwell_ButtonExecute_mCE5D5793F81B0FA3E5901086BD890C46C1E83214 (void);
// 0x0000018C System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::.ctor()
extern void ListItemDwell__ctor_m3B090DE69C79E75F6843F7084CC87EFDD002185D (void);
// 0x0000018D System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::Update()
extern void ToggleDwellSample_Update_mA0AC5EE22276C7A732470373820AD7DC6794A722 (void);
// 0x0000018E System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellIntended(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellIntended_m9A5CE0A95AC3B91BF966C10BA925392A61647E6C (void);
// 0x0000018F System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellStarted_mD724801088A9B49F13B3194EC965B7FDB21889E1 (void);
// 0x00000190 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellCanceled_mE888D66A5061485F06E4BCF7BC32F7FF40450A8F (void);
// 0x00000191 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellCompleted_m11BB146A8D26534BE015E9657D879EA0FB7C568E (void);
// 0x00000192 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::ButtonExecute()
extern void ToggleDwellSample_ButtonExecute_mF10F16BFE4C3006A9C9450AC3E5B5B1CEA2FEF09 (void);
// 0x00000193 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::.ctor()
extern void ToggleDwellSample__ctor_m78C4026657BE2B8EF319437F219206BE04F4945A (void);
// 0x00000194 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::Start()
extern void DemoSceneUnderstandingController_Start_m4B17A37DE3F2D508CE71E7157EFB5858D8DE1A72 (void);
// 0x00000195 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnEnable()
extern void DemoSceneUnderstandingController_OnEnable_m62D4ECA9AC80E65CBDA49E9C4C87008D15E695F8 (void);
// 0x00000196 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnDisable()
extern void DemoSceneUnderstandingController_OnDisable_mE0875C4DEB026A2F8EBA297A467F359C01732377 (void);
// 0x00000197 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnDestroy()
extern void DemoSceneUnderstandingController_OnDestroy_m7B4DE4D879BCB3E79C8D7852AD28842919B43CA2 (void);
// 0x00000198 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnObservationAdded(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject>)
extern void DemoSceneUnderstandingController_OnObservationAdded_m84010971D940DFE6E95431836A62A992ACAA4F70 (void);
// 0x00000199 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnObservationUpdated(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject>)
extern void DemoSceneUnderstandingController_OnObservationUpdated_m7F818491FD7C98EC618339AE440F336AD0E7551D (void);
// 0x0000019A System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnObservationRemoved(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject>)
extern void DemoSceneUnderstandingController_OnObservationRemoved_m1E046C51010DECB2363B9F3801847EC083612DC2 (void);
// 0x0000019B System.Collections.Generic.IReadOnlyDictionary`2<System.Int32,Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject> Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::GetSceneObjectsOfType(Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessSurfaceTypes)
extern void DemoSceneUnderstandingController_GetSceneObjectsOfType_mDF036B1CDEEF82C9633BBDF392FC99997C746AF8 (void);
// 0x0000019C System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::UpdateScene()
extern void DemoSceneUnderstandingController_UpdateScene_mE17222C56EA2D82BF65572D01FA3FC7176C8D51E (void);
// 0x0000019D System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::SaveScene()
extern void DemoSceneUnderstandingController_SaveScene_mFB615C0B6A54E752340E6554334330FAE8780006 (void);
// 0x0000019E System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ClearScene()
extern void DemoSceneUnderstandingController_ClearScene_m2ED847E5EE175C01C1EC6A3C45ABD03E9A6A1A64 (void);
// 0x0000019F System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleAutoUpdate()
extern void DemoSceneUnderstandingController_ToggleAutoUpdate_m3C611B4B65EED1376DCB3AA6B9787CF4E6A7E22C (void);
// 0x000001A0 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleOcclusionMask()
extern void DemoSceneUnderstandingController_ToggleOcclusionMask_m04728E2761F48FDAB3B59BE8523EED1B21E4E0C3 (void);
// 0x000001A1 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleGeneratePlanes()
extern void DemoSceneUnderstandingController_ToggleGeneratePlanes_m23789FAB660733BD5E19D9D5FAD10E1B052254D0 (void);
// 0x000001A2 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleGenerateMeshes()
extern void DemoSceneUnderstandingController_ToggleGenerateMeshes_mE6A79BD78FB9E33DF50B6E57255841EDA685B6DD (void);
// 0x000001A3 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleFloors()
extern void DemoSceneUnderstandingController_ToggleFloors_m35A7E2F082F6E89F4919192411A7E9201B57EF9C (void);
// 0x000001A4 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleWalls()
extern void DemoSceneUnderstandingController_ToggleWalls_m00F44B7BE89E42988789504618B8F5BB3EABEA53 (void);
// 0x000001A5 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleCeilings()
extern void DemoSceneUnderstandingController_ToggleCeilings_m4BD71B44E4B7BFA27B52025AA186054CADCA8A47 (void);
// 0x000001A6 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::TogglePlatforms()
extern void DemoSceneUnderstandingController_TogglePlatforms_m222359C5DC9FB50A5D0A1BA71C5ADEC639D007D4 (void);
// 0x000001A7 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleInferRegions()
extern void DemoSceneUnderstandingController_ToggleInferRegions_m8830563A5AD5879F8532180837342EF4B5BA78E3 (void);
// 0x000001A8 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleWorld()
extern void DemoSceneUnderstandingController_ToggleWorld_m3E9F262334FAAF1A791A12A31592ED7E8981BABF (void);
// 0x000001A9 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleBackground()
extern void DemoSceneUnderstandingController_ToggleBackground_m1FFA7D3A89471BDDB797FEE1123E005B351F61B0 (void);
// 0x000001AA System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleCompletelyInferred()
extern void DemoSceneUnderstandingController_ToggleCompletelyInferred_m43CBACE73783B42845163FA9DF489589F2DB6474 (void);
// 0x000001AB System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::InitToggleButtonState()
extern void DemoSceneUnderstandingController_InitToggleButtonState_m7F626A4293E8BDEB74CA31A2EECF39F31B535D49 (void);
// 0x000001AC UnityEngine.Color Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ColorForSurfaceType(Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessSurfaceTypes)
extern void DemoSceneUnderstandingController_ColorForSurfaceType_m7F0126FD9BA9B2A92F6EFAEAD93C9C84F4A02463 (void);
// 0x000001AD System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ClearAndUpdateObserver()
extern void DemoSceneUnderstandingController_ClearAndUpdateObserver_m5FEFE0F4BB79B9FD5A182483A9650FE26258C5B1 (void);
// 0x000001AE System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleObservedSurfaceType(Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessSurfaceTypes)
extern void DemoSceneUnderstandingController_ToggleObservedSurfaceType_mF295356D81B81233E36A180B6D7431EB017A0E8D (void);
// 0x000001AF System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::.ctor()
extern void DemoSceneUnderstandingController__ctor_m0A4DBA9966AB236B2D41CAAF71FE9E4ED6589EC4 (void);
// 0x000001B0 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void KeyboardTest_OnPointerDown_m0839A6FB3F723C3C2F3DF7FBB485DBD8146F264A (void);
// 0x000001B1 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::UpdateText(System.String)
extern void KeyboardTest_UpdateText_m38415F169A69BC935F5237D3168DFFA74E084C27 (void);
// 0x000001B2 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::DisableKeyboard(System.Object,System.EventArgs)
extern void KeyboardTest_DisableKeyboard_mAD37A7B9E6D0EF790B01F9E0006C1EEB46511D08 (void);
// 0x000001B3 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::.ctor()
extern void KeyboardTest__ctor_m0D784D016FBB427DEEEF6AA9F8F448863947247A (void);
// 0x000001B4 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::Start()
extern void JoystickSliders_Start_m70D17A04A3219B9D023FEF30E7358FBE6C42E9C7 (void);
// 0x000001B5 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::CalculateValues()
extern void JoystickSliders_CalculateValues_m5EE7E7E2344C15F497110F2A9E487AAE76B0EF78 (void);
// 0x000001B6 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::UpdateSliderValues()
extern void JoystickSliders_UpdateSliderValues_m9186287AB06D884A08BC8D820E6C64BA5FE27E3D (void);
// 0x000001B7 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::.ctor()
extern void JoystickSliders__ctor_m31DD8D30473443BEE176D3A944E7092F8BEB9EBC (void);
// 0x000001B8 System.Void Microsoft.MixedReality.Toolkit.Experimental.SurfacePulse.HideOnDevice::Start()
extern void HideOnDevice_Start_m67317CBC5E1DADFBA583D4F417DFB10E9A339CEB (void);
// 0x000001B9 System.Void Microsoft.MixedReality.Toolkit.Experimental.SurfacePulse.HideOnDevice::.ctor()
extern void HideOnDevice__ctor_m9DAF3310FFE059D3CF8DDEA3FE739838CB35B1B1 (void);
// 0x000001BA System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnEnable()
extern void GestureTester_OnEnable_m9AADD8392DD6B7F1426E64893B3F4BAC46A4BC07 (void);
// 0x000001BB System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureStarted(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureStarted_mCF1BE4F1FF22393623C32F1B0E6AC54F91BE297E (void);
// 0x000001BC System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureUpdated_mF46446A8F728105F119FB700455D39E512163CD4 (void);
// 0x000001BD System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<UnityEngine.Vector3>)
extern void GestureTester_OnGestureUpdated_m87C7C00F3CDF1CD9EEB1ADE6FB76213503ACB3CB (void);
// 0x000001BE System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureCompleted(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureCompleted_m4666AFD1E4F059552A9D39D985F71F8E84F8E0CE (void);
// 0x000001BF System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureCompleted(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<UnityEngine.Vector3>)
extern void GestureTester_OnGestureCompleted_m7C0B5A3C601B167666063099914A8F2E918A68AC (void);
// 0x000001C0 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureCanceled(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureCanceled_mB0BAF7B49FCA4856600B4D0BABB7EDC6A04FEB3E (void);
// 0x000001C1 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::SetIndicator(UnityEngine.GameObject,System.String,UnityEngine.Material)
extern void GestureTester_SetIndicator_mD5DDB1BCEE0E02E3B3F15978868F540017B13CE8 (void);
// 0x000001C2 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::SetIndicator(UnityEngine.GameObject,System.String,UnityEngine.Material,UnityEngine.Vector3)
extern void GestureTester_SetIndicator_m28F3812D5118DB72C0471412FFDA3AF75BE9BE01 (void);
// 0x000001C3 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::ShowRails(UnityEngine.Vector3)
extern void GestureTester_ShowRails_mC8E3453E111E72C2E5D3A5154396B1B06C4837F8 (void);
// 0x000001C4 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::HideRails()
extern void GestureTester_HideRails_mF951FABE031588F25EEE7AFFAD0449CAA21650C1 (void);
// 0x000001C5 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::.ctor()
extern void GestureTester__ctor_m59B2BD41A6257BE9E46109314FDF03DBC13A5E22 (void);
// 0x000001C6 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::Awake()
extern void GrabTouchExample_Awake_mF1A7CF657D2BF3845A6F5013CAAA2ED3B4871518 (void);
// 0x000001C7 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnInputDown(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GrabTouchExample_OnInputDown_m4F2432C631F68039D2A4D58A9A191163F060FC1C (void);
// 0x000001C8 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnInputUp(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GrabTouchExample_OnInputUp_m3ABADEBABE435C956571436FA9DD6829B97206F3 (void);
// 0x000001C9 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnInputPressed(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<System.Single>)
extern void GrabTouchExample_OnInputPressed_m22B8A5F55A79FB4B22740CE58C862D2207686EA6 (void);
// 0x000001CA System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnPositionInputChanged(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<UnityEngine.Vector2>)
extern void GrabTouchExample_OnPositionInputChanged_mAFD365CD4F7BC572E1BB69E2BA93B7093B59902C (void);
// 0x000001CB System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnTouchCompleted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void GrabTouchExample_OnTouchCompleted_m6A65545CD38255D5C8A0E8DD3958B1428FCF61EB (void);
// 0x000001CC System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnTouchStarted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void GrabTouchExample_OnTouchStarted_m8509F80B2EFE35C934A2BE96A4096EBAAA9AEFF9 (void);
// 0x000001CD System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void GrabTouchExample_OnTouchUpdated_m602E0527A4C57151F74EA6CFA069228312CFAAF1 (void);
// 0x000001CE System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::.ctor()
extern void GrabTouchExample__ctor_m9EAD45E5FACBABD21087D76A284094AA74AF644B (void);
// 0x000001CF System.Void Microsoft.MixedReality.Toolkit.Examples.LeapMotionOrientationDisplay::.ctor()
extern void LeapMotionOrientationDisplay__ctor_mB03351C717D53DF299B4F817B344562D94AC9315 (void);
// 0x000001D0 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnEnable()
extern void RotateWithPan_OnEnable_m119D06CDEDB9960441C6A9EB52E0E3156526F6FC (void);
// 0x000001D1 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnDisable()
extern void RotateWithPan_OnDisable_mE66C37D891C63D16C57B9C42FB46DE6CCAE577D0 (void);
// 0x000001D2 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnPanEnded(Microsoft.MixedReality.Toolkit.UI.HandPanEventData)
extern void RotateWithPan_OnPanEnded_m5DCB7ADB80553546E0EB418824D1C193DB29F52E (void);
// 0x000001D3 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnPanning(Microsoft.MixedReality.Toolkit.UI.HandPanEventData)
extern void RotateWithPan_OnPanning_mAAEE5025B70EA41A405EFE3E97AF94FAC9547827 (void);
// 0x000001D4 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnPanStarted(Microsoft.MixedReality.Toolkit.UI.HandPanEventData)
extern void RotateWithPan_OnPanStarted_m53911BBAB7CC74996FF05BB38F1CA5CF74CCCDF3 (void);
// 0x000001D5 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::.ctor()
extern void RotateWithPan__ctor_m75092EEC95C572006C07A392B333F4862347E21D (void);
// 0x000001D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::Start()
extern void WidgetElasticDemo_Start_m6674F2EAD39D9E0D4AF6EC01D8923082C7E080CE (void);
// 0x000001D7 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::Update()
extern void WidgetElasticDemo_Update_mA5A7CFCC2851CE05FE57128B69AAB20ECDE510C4 (void);
// 0x000001D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::ToggleInflate()
extern void WidgetElasticDemo_ToggleInflate_m3943A9E8BE66A6D0D5CF6E1E91CF200466629947 (void);
// 0x000001D9 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::DeflateCoroutine()
extern void WidgetElasticDemo_DeflateCoroutine_m7A6B7ABCEC5B8DA37508114CEE6AA833D01623BB (void);
// 0x000001DA System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::InflateCoroutine()
extern void WidgetElasticDemo_InflateCoroutine_m531C85BAA6BF580ACB6A8739883D463E4663DAAD (void);
// 0x000001DB System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::.ctor()
extern void WidgetElasticDemo__ctor_mB3E21BF8959B7ED3A272375BADEEBB793B0624CA (void);
// 0x000001DC System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::.cctor()
extern void WidgetElasticDemo__cctor_m79261925C68C375709D216826C617477CFCE9BCE (void);
// 0x000001DD System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::.ctor(System.Int32)
extern void U3CDeflateCoroutineU3Ed__17__ctor_m5DFBE929B124C6BF2853DE555D5EED0665E98DF7 (void);
// 0x000001DE System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.IDisposable.Dispose()
extern void U3CDeflateCoroutineU3Ed__17_System_IDisposable_Dispose_m9C136F49502B8941857512F103A71FC82CF1DC55 (void);
// 0x000001DF System.Boolean Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::MoveNext()
extern void U3CDeflateCoroutineU3Ed__17_MoveNext_mBE4D780C9111C8E62B42F42031C7C0A8082CEB16 (void);
// 0x000001E0 System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeflateCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54B77CAAA405C67DCDD618942DAB395F0E61E404 (void);
// 0x000001E1 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.Collections.IEnumerator.Reset()
extern void U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m9F18AABEB42C0AF9CB87D5AA40EFDBEB8DBEBE50 (void);
// 0x000001E2 System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_m9602F0FA0E980A6B59E2E6AFFAC65C417D3F0438 (void);
// 0x000001E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::.ctor(System.Int32)
extern void U3CInflateCoroutineU3Ed__18__ctor_mDB85ED46DCB8408FD5640F914B6CFD808D4F5C6E (void);
// 0x000001E4 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.IDisposable.Dispose()
extern void U3CInflateCoroutineU3Ed__18_System_IDisposable_Dispose_m53123F5C53E12D7B7E287E0A0E7121CD20637451 (void);
// 0x000001E5 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::MoveNext()
extern void U3CInflateCoroutineU3Ed__18_MoveNext_m7DEF6D11C67B652DA43ECC61AA182EA3B60D2CCF (void);
// 0x000001E6 System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInflateCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B98603EDEBD08B16E03B196AE45DA4F8C90FA18 (void);
// 0x000001E7 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.Collections.IEnumerator.Reset()
extern void U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_mB0E951E48F412D0E6B14ECBC6CF01C41457B40B5 (void);
// 0x000001E8 System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_mB7A971F317E35D991918C1D40A27BA4B6B41E114 (void);
// 0x000001E9 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::get_DialogPrefabLarge()
extern void DialogExampleController_get_DialogPrefabLarge_m8FC664A339189AB78D7574D55DDE795ED3294F17 (void);
// 0x000001EA System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::set_DialogPrefabLarge(UnityEngine.GameObject)
extern void DialogExampleController_set_DialogPrefabLarge_m02063994FF1ED01FE783C99633D7A9222C787A99 (void);
// 0x000001EB UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::get_DialogPrefabMedium()
extern void DialogExampleController_get_DialogPrefabMedium_m9C5598B73A460A775FEA01085FC64D00426D782D (void);
// 0x000001EC System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::set_DialogPrefabMedium(UnityEngine.GameObject)
extern void DialogExampleController_set_DialogPrefabMedium_mEF1C9F41E989D976F991A71942F3C9449E3AB5DA (void);
// 0x000001ED UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::get_DialogPrefabSmall()
extern void DialogExampleController_get_DialogPrefabSmall_mB59482A7E606DAD8CF69B9A177304A2BDC55CAD8 (void);
// 0x000001EE System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::set_DialogPrefabSmall(UnityEngine.GameObject)
extern void DialogExampleController_set_DialogPrefabSmall_mEF0448A048EE6AD01E04B03FC1E517DDD1616FE7 (void);
// 0x000001EF System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenConfirmationDialogLarge()
extern void DialogExampleController_OpenConfirmationDialogLarge_mC6356B5C7E14DC444417ECCDDB4A452EC7E793F2 (void);
// 0x000001F0 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenChoiceDialogLarge()
extern void DialogExampleController_OpenChoiceDialogLarge_m79ECC12F22BEA31A9EF5434478A56E27318F8D74 (void);
// 0x000001F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenConfirmationDialogMedium()
extern void DialogExampleController_OpenConfirmationDialogMedium_m5FEA5A1E0E73AA8D1B4388BB83898D9FFAA085AF (void);
// 0x000001F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenChoiceDialogMedium()
extern void DialogExampleController_OpenChoiceDialogMedium_mFC4D1F728B865873A9BFAB20B2FD7CC55714B9DD (void);
// 0x000001F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenConfirmationDialogSmall()
extern void DialogExampleController_OpenConfirmationDialogSmall_m78866AEC7AF530584A4E624583050970CAF44D69 (void);
// 0x000001F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenChoiceDialogSmall()
extern void DialogExampleController_OpenChoiceDialogSmall_mB6658E027EE8DC1C789A82C94FC6A0751617ED79 (void);
// 0x000001F5 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OnClosedDialogEvent(Microsoft.MixedReality.Toolkit.UI.DialogResult)
extern void DialogExampleController_OnClosedDialogEvent_mFCA1C13D6EEF04C1204A52E0129CA6CFFBDBBB59 (void);
// 0x000001F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::.ctor()
extern void DialogExampleController__ctor_m3BC8BC7D42744DF5E24372839FBE629D789BD17B (void);
// 0x000001F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::Awake()
extern void BoundaryVisualizationDemo_Awake_m2A779C57EE3A389E1600C176FBA1CF8404A25C93 (void);
// 0x000001F8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::Start()
extern void BoundaryVisualizationDemo_Start_m83206DA28C5AA3F03645C62BB40CD5E41FAC98CB (void);
// 0x000001F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::Update()
extern void BoundaryVisualizationDemo_Update_m0C5779043B7681AA15F2188B330FE21A79CB2700 (void);
// 0x000001FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::OnEnable()
extern void BoundaryVisualizationDemo_OnEnable_mCD089D3906774A57D8F7DD860DBA64A4FF55894D (void);
// 0x000001FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::OnDisable()
extern void BoundaryVisualizationDemo_OnDisable_m28EE46D607457BA30CDD784F7DD61216406CFA5A (void);
// 0x000001FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::OnBoundaryVisualizationChanged(Microsoft.MixedReality.Toolkit.Boundary.BoundaryEventData)
extern void BoundaryVisualizationDemo_OnBoundaryVisualizationChanged_mF0A72330C0785EA3C594B14ED6A62F08222E3A72 (void);
// 0x000001FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::AddMarkers()
extern void BoundaryVisualizationDemo_AddMarkers_m60BA0DB0C980993299BFAEDDE47C5BF6F1239DBB (void);
// 0x000001FE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::.ctor()
extern void BoundaryVisualizationDemo__ctor_m3EE3DA09C389EC0C132EDFB40248828031E4A525 (void);
// 0x000001FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DebugTextOutput::SetTextWithTimestamp(System.String)
extern void DebugTextOutput_SetTextWithTimestamp_m62F3F7E77B050521F6378455ED8784C44FB82278 (void);
// 0x00000200 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DebugTextOutput::.ctor()
extern void DebugTextOutput__ctor_mC1F2AC69FFEC0E69567A3D525D2FA49E9E28B3A9 (void);
// 0x00000201 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Awake()
extern void DemoTouchButton_Awake_m83BAA3AAE898955C5D1A805ED49145A6B28BE6E0 (void);
// 0x00000202 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m022BD52F7B9375F565C9819C36F4E99FD7423588 (void);
// 0x00000203 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mB4D0A6210C60B243DAE610CEF15FC8D9B64F1A7A (void);
// 0x00000204 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m12F427DE21A0E72EBFFDA28770B73E72373503C4 (void);
// 0x00000205 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m13D7ABD2DD39019470E32028659BBD8476190E20 (void);
// 0x00000206 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::.ctor()
extern void DemoTouchButton__ctor_mF7D651379967BF470D6AE876C18D80D4594F6323 (void);
// 0x00000207 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Start()
extern void HandInteractionTouch_Start_mC4C9FE251E5C7099CFFF986F81A7DD8CB8DAD8B7 (void);
// 0x00000208 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchCompleted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchCompleted_mB132E11E21B6CA0B22CF0BE1176C396D07275B06 (void);
// 0x00000209 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchStarted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchStarted_mCC8AF20A81FD38F193253887392F484612DDEB48 (void);
// 0x0000020A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_mB2961B642338C4BAF9AFCBA197011B8B4DB2E116 (void);
// 0x0000020B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::.ctor()
extern void HandInteractionTouch__ctor_m2BAF73805C7E05C4BA70E87DE0C43EC77D38E7BC (void);
// 0x0000020C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouchRotate::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouchRotate_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_mAC47B7F4FFEB871D99BAB41BD24F1070EC7C394A (void);
// 0x0000020D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouchRotate::.ctor()
extern void HandInteractionTouchRotate__ctor_mD76C9936325A4491C17BA2317A07E0958ED9E6C8 (void);
// 0x0000020E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LaunchUri::Launch(System.String)
extern void LaunchUri_Launch_m3288F46EF18446AE7EFBCC5D012A4E7BB11BB43C (void);
// 0x0000020F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LaunchUri::.ctor()
extern void LaunchUri__ctor_m7FA1E45963920ECD98D27F0CCF13FE265224C265 (void);
// 0x00000210 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LeapCoreAssetsDetector::Start()
extern void LeapCoreAssetsDetector_Start_m23798D463B5CC0D836ED7BB4C22BD8A4149AD97A (void);
// 0x00000211 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LeapCoreAssetsDetector::.ctor()
extern void LeapCoreAssetsDetector__ctor_m16AB1C5D1007014CC9E23A0F530275A969A1E15A (void);
// 0x00000212 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverTrackedTargetType::ChangeTrackedTargetTypeHead()
extern void SolverTrackedTargetType_ChangeTrackedTargetTypeHead_m26CDA8F1F58F139EBB2950BF4FEC98CC4A2CDAFA (void);
// 0x00000213 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverTrackedTargetType::ChangeTrackedTargetTypeHandJoint()
extern void SolverTrackedTargetType_ChangeTrackedTargetTypeHandJoint_m5053FCEF017B9D73200DDB0FDF2886AB46A5A1A0 (void);
// 0x00000214 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverTrackedTargetType::.ctor()
extern void SolverTrackedTargetType__ctor_m53B326829C5A2A45B5E608A12A47908BF01415A4 (void);
// 0x00000215 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::OpenSystemKeyboard()
extern void SystemKeyboardExample_OpenSystemKeyboard_m9BD2817D17BAEB8FE0C5D35EDF4947240329A042 (void);
// 0x00000216 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::Start()
extern void SystemKeyboardExample_Start_m6B4779381CC80DC830F3C76406E75A3150A0564B (void);
// 0x00000217 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::Update()
extern void SystemKeyboardExample_Update_mEBC6768FDE887113E9C86D9EA09A2C9D1E851F23 (void);
// 0x00000218 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::.ctor()
extern void SystemKeyboardExample__ctor_mE3E324EF9F9159533B8AEDE89FA1F02216CC227D (void);
// 0x00000219 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::<Start>b__5_0()
extern void SystemKeyboardExample_U3CStartU3Eb__5_0_mBBC953AE9A3048F737E4E9F5EA6DFBFB520CA5B0 (void);
// 0x0000021A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::<Start>b__5_1()
extern void SystemKeyboardExample_U3CStartU3Eb__5_1_m2FA208FBE3F7D0001E43C0384E3C6B4363B19B26 (void);
// 0x0000021B System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::get_DistanceThreshold()
extern void TetheredPlacement_get_DistanceThreshold_m69455E3940DBC306E5D7B92D3C3BC9FC6F7E3264 (void);
// 0x0000021C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::set_DistanceThreshold(System.Single)
extern void TetheredPlacement_set_DistanceThreshold_m5524A2BBB89E21EC7B39D181562C28C6BE897169 (void);
// 0x0000021D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::Start()
extern void TetheredPlacement_Start_mEB6C4989B61651853D9AE6259765F5C67BD16092 (void);
// 0x0000021E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::LateUpdate()
extern void TetheredPlacement_LateUpdate_m9BBC1F0262D9430F60EB2353CB33BE36CC82ECB2 (void);
// 0x0000021F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::LockSpawnPoint()
extern void TetheredPlacement_LockSpawnPoint_m0F437637778DE779C046B8450C184B98D80DB230 (void);
// 0x00000220 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::.ctor()
extern void TetheredPlacement__ctor_mB4266A14A1AC6DB8282FC58FAA85C86EF5A45F25 (void);
// 0x00000221 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ToggleBoundingBox::Awake()
extern void ToggleBoundingBox_Awake_mCB0371F1DF5F44D3FAB2417C925A337275BA9D2B (void);
// 0x00000222 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ToggleBoundingBox::ToggleBoundingBoxActiveState()
extern void ToggleBoundingBox_ToggleBoundingBoxActiveState_mA19D26CE4211EAFA2C03278F28F4AA21F09F69A4 (void);
// 0x00000223 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ToggleBoundingBox::.ctor()
extern void ToggleBoundingBox__ctor_m3F71A30BF46D15049AAFE21F2547C66B1CDBE898 (void);
// 0x00000224 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::Start()
extern void DisablePointersExample_Start_mE95C8E48999035201BCC2E286BFB8293BC184D27 (void);
// 0x00000225 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::ResetExample()
extern void DisablePointersExample_ResetExample_m6BB2007914FA4A1BCA89649385223BFCBD289BAA (void);
// 0x00000226 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::Update()
extern void DisablePointersExample_Update_mE7086EDDD3BB7043A1BF51E486D27C64D21545E7 (void);
// 0x00000227 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::SetToggleHelper(Microsoft.MixedReality.Toolkit.UI.Interactable,System.String,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
// 0x00000228 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::.ctor()
extern void DisablePointersExample__ctor_m38D914A24180CA1F2527CC9A1F15A4AEBF19F8B8 (void);
// 0x00000229 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Rotator::Rotate()
extern void Rotator_Rotate_m9C30728AA6DDBB6414F95AD3C2F4EED9BB56F52F (void);
// 0x0000022A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Rotator::.ctor()
extern void Rotator__ctor_mDC4DCF4B852F99E46D355FF0F80732352E4593AB (void);
// 0x0000022B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExample::Update()
extern void InputDataExample_Update_m42D3EDD3EA92C407C6EAAA9C8C44A12EE5026A90 (void);
// 0x0000022C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExample::Start()
extern void InputDataExample_Start_m2BE6957A24DB43264CF4BC5DF4AEA2B219D8CC35 (void);
// 0x0000022D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExample::.ctor()
extern void InputDataExample__ctor_mE2D52779029ADD0152A1ECD4973935F9F8371B60 (void);
// 0x0000022E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExampleGizmo::SetIsDataAvailable(System.Boolean)
extern void InputDataExampleGizmo_SetIsDataAvailable_m7A9DF9F25D6D16DD610E5FA495D325626C41CA76 (void);
// 0x0000022F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExampleGizmo::Update()
extern void InputDataExampleGizmo_Update_m5FEDB21DF2A5124A80A44A77E00DAA7920C57694 (void);
// 0x00000230 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExampleGizmo::.ctor()
extern void InputDataExampleGizmo__ctor_mB0E72B20C8C8833255B0E6330E8EE1B83DD2C9BA (void);
// 0x00000231 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SpawnOnPointerEvent::Spawn(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void SpawnOnPointerEvent_Spawn_m28A42E905B34B797BEDB1276B50EE00C790BD76F (void);
// 0x00000232 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SpawnOnPointerEvent::.ctor()
extern void SpawnOnPointerEvent__ctor_mF4501DBA5DCB61B7BDF97C252644013AAC1BB242 (void);
// 0x00000233 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::OnEnable()
extern void PrimaryPointerHandlerExample_OnEnable_m373469C4B43BEA03EC91C4F85F846A0CA2557D44 (void);
// 0x00000234 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::OnPrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void PrimaryPointerHandlerExample_OnPrimaryPointerChanged_m056DBFB929005ED8B7AB9276FE9D02B0048A87E6 (void);
// 0x00000235 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::OnDisable()
extern void PrimaryPointerHandlerExample_OnDisable_mE71162D75DF5352D9265B07651786DEE8E868017 (void);
// 0x00000236 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::.ctor()
extern void PrimaryPointerHandlerExample__ctor_mF6CC650501FDA49A11D19F35C0B15E6E8B623E0B (void);
// 0x00000237 Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_ScrollView()
extern void ScrollableListPopulator_get_ScrollView_mE4359C6503DA8D9D908AE808BDBD39F6CF6EC165 (void);
// 0x00000238 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_ScrollView(Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection)
extern void ScrollableListPopulator_set_ScrollView_m111BE64C0713209CB73285869CC54F00F0529665 (void);
// 0x00000239 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_DynamicItem()
extern void ScrollableListPopulator_get_DynamicItem_m0272AD2829DCE59728C68F4FC4FD16D75512235C (void);
// 0x0000023A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_DynamicItem(UnityEngine.GameObject)
extern void ScrollableListPopulator_set_DynamicItem_m47A499A149076520900F0FD0C9C266AE067C0446 (void);
// 0x0000023B System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_NumItems()
extern void ScrollableListPopulator_get_NumItems_mEFA4395AEC9A2C322031AB38E6DDBE5F5F2204C9 (void);
// 0x0000023C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_NumItems(System.Int32)
extern void ScrollableListPopulator_set_NumItems_mCACE6E5E3AFF966F7E544AEB7EEE74248E50EEB6 (void);
// 0x0000023D System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_LazyLoad()
extern void ScrollableListPopulator_get_LazyLoad_m94D32263AA1ED4A7CD448885FCFE29ECDA7D7024 (void);
// 0x0000023E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_LazyLoad(System.Boolean)
extern void ScrollableListPopulator_set_LazyLoad_m706E6B3D480CEC9B14763AB17E3F974C1B8DD1BB (void);
// 0x0000023F System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_ItemsPerFrame()
extern void ScrollableListPopulator_get_ItemsPerFrame_m00817C2F73473CA49268B1DB02C1B632393DEDC2 (void);
// 0x00000240 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_ItemsPerFrame(System.Int32)
extern void ScrollableListPopulator_set_ItemsPerFrame_m28A9FD7D6B490D47906F9AEAA120E369F24156F2 (void);
// 0x00000241 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_Loader()
extern void ScrollableListPopulator_get_Loader_mB607C13954D90666E01D6CDF98E69B49ACD94B32 (void);
// 0x00000242 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_Loader(UnityEngine.GameObject)
extern void ScrollableListPopulator_set_Loader_mC3348F1248E6B894151EEF0D2A70A858E7960990 (void);
// 0x00000243 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::OnEnable()
extern void ScrollableListPopulator_OnEnable_m08DD662D16477AFA757FC7AFF3D5BFE977A34662 (void);
// 0x00000244 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::MakeScrollingList()
extern void ScrollableListPopulator_MakeScrollingList_m4E8AF1CB34021233F1D733A596F785B1FA35AD01 (void);
// 0x00000245 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::UpdateListOverTime(UnityEngine.GameObject,System.Int32)
extern void ScrollableListPopulator_UpdateListOverTime_m32ED0D3125568A81E660F540892F08F316978C03 (void);
// 0x00000246 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::MakeItem(UnityEngine.GameObject)
extern void ScrollableListPopulator_MakeItem_mF02B39783E169D75E4C1128208568C694B0F90C9 (void);
// 0x00000247 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::.ctor()
extern void ScrollableListPopulator__ctor_m8FDE50C9FD64C5CC70ADBC38DD02824965B7648D (void);
// 0x00000248 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::.ctor(System.Int32)
extern void U3CUpdateListOverTimeU3Ed__33__ctor_mACF3D61CE3F2A9BB63D04365DFEB3FBA33006A8E (void);
// 0x00000249 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.IDisposable.Dispose()
extern void U3CUpdateListOverTimeU3Ed__33_System_IDisposable_Dispose_m776D3F20FCEE5A4C9AB735AC719CDC73A3286A58 (void);
// 0x0000024A System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::MoveNext()
extern void U3CUpdateListOverTimeU3Ed__33_MoveNext_mEA913295CA5C6568754B5A6D6AAD232F39AF9675 (void);
// 0x0000024B System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateListOverTimeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD4405FFF14D4D5888134AE62DB8E397895FFFDAC (void);
// 0x0000024C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.Collections.IEnumerator.Reset()
extern void U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_Reset_m7F8F79192E94B0404CF67B7821AAD3A2BFDA198A (void);
// 0x0000024D System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_get_Current_mF8BFCF66510179224DDAD81021FCD10349DF4ABB (void);
// 0x0000024E Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::get_ScrollView()
extern void ScrollablePagination_get_ScrollView_m33BDA7C3EE18158EA0BB51C08CC9BE6ED40D552B (void);
// 0x0000024F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::set_ScrollView(Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection)
extern void ScrollablePagination_set_ScrollView_mE6F7D99CF48610748F72EFC7239C3BDE005D4FB1 (void);
// 0x00000250 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::ScrollByTier(System.Int32)
extern void ScrollablePagination_ScrollByTier_mD2F6CAB9042505021A68A4F9341AE62A1E128D50 (void);
// 0x00000251 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::.ctor()
extern void ScrollablePagination__ctor_m2BAE76DB581ED8BB855CACEB369E170421CABA67 (void);
// 0x00000252 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::Start()
extern void HideTapToPlaceLabel_Start_mCC773ED90C0381AB7541551FB618A5E0E03293EA (void);
// 0x00000253 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::AddTapToPlaceListeners()
extern void HideTapToPlaceLabel_AddTapToPlaceListeners_mC16C31B8CE4C898FEE72D7A2B9AA153413FF31FF (void);
// 0x00000254 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::.ctor()
extern void HideTapToPlaceLabel__ctor_m01E75563A70E3C414DE5B5159FDBE5224361BF0E (void);
// 0x00000255 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::<AddTapToPlaceListeners>b__3_0()
extern void HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_0_mA9B253B46801DD948048DE6952B66562AA3FD6F4 (void);
// 0x00000256 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::<AddTapToPlaceListeners>b__3_1()
extern void HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_1_m104568A79D4F1B5C5D6D483CEC339552D74AE3D0 (void);
// 0x00000257 Microsoft.MixedReality.Toolkit.Utilities.TrackedObjectType Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::get_TrackedType()
extern void SolverExampleManager_get_TrackedType_mE352E99180AD07F9BD7C10FAE1A96765517B60A3 (void);
// 0x00000258 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::set_TrackedType(Microsoft.MixedReality.Toolkit.Utilities.TrackedObjectType)
extern void SolverExampleManager_set_TrackedType_mA6B36A1455DF0BBABC4C5DD4FAA4736229FB3D71 (void);
// 0x00000259 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::Awake()
extern void SolverExampleManager_Awake_m45882BF5F2ED694554C4366071C5DE088C2DB73B (void);
// 0x0000025A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedHead()
extern void SolverExampleManager_SetTrackedHead_m2E7F32F58366D84BFB5D4E77E56452904B5340BC (void);
// 0x0000025B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedController()
extern void SolverExampleManager_SetTrackedController_mAFAF0B064B39DBF178D595CFF5BFE3E9C9A99434 (void);
// 0x0000025C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedHands()
extern void SolverExampleManager_SetTrackedHands_m0056AD29B22AA74401C34031CF2B5BBA60155971 (void);
// 0x0000025D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedCustom()
extern void SolverExampleManager_SetTrackedCustom_m415CCF839F500B28529B15321865B57ED98652D6 (void);
// 0x0000025E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetRadialView()
extern void SolverExampleManager_SetRadialView_m3C66B3B33501F01ACD9F015A658977E39EFD2B90 (void);
// 0x0000025F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetOrbital()
extern void SolverExampleManager_SetOrbital_m557A359F1A2CC5B55066062A3F6A217515A22477 (void);
// 0x00000260 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetSurfaceMagnetism()
extern void SolverExampleManager_SetSurfaceMagnetism_mB3BF53B2219E1028997E2528FD3372333F62725F (void);
// 0x00000261 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::AddSolver()
// 0x00000262 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::RefreshSolverHandler()
extern void SolverExampleManager_RefreshSolverHandler_mE22A621F894F2A36560C99AE3DEFA06E4457C632 (void);
// 0x00000263 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::DestroySolver()
extern void SolverExampleManager_DestroySolver_mB3C862B5A8492B236D5AFCC0B59F42CCDE1E0BFC (void);
// 0x00000264 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::.ctor()
extern void SolverExampleManager__ctor_m323026103252B8ADEBAAD5D83CD179B7F8725225 (void);
// 0x00000265 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ClearSpatialObservations::ToggleObservers()
extern void ClearSpatialObservations_ToggleObservers_mCBF8F34B48824CECE007EAED66B739A21FCA3D97 (void);
// 0x00000266 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ClearSpatialObservations::.ctor()
extern void ClearSpatialObservations__ctor_m7F145FA493156190DF93C07A9D7E3A77F238857B (void);
// 0x00000267 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ShowPlaneFindingInstructions::Start()
extern void ShowPlaneFindingInstructions_Start_mEC65BE9F96543B1D2242318482CFD33811D9608F (void);
// 0x00000268 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ShowPlaneFindingInstructions::.ctor()
extern void ShowPlaneFindingInstructions__ctor_m43706941098FB18156D44C75BFDC7FCF10412B99 (void);
// 0x00000269 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.MixedRealityCapabilityDemo::Start()
extern void MixedRealityCapabilityDemo_Start_m13FC8144EB56CD9B42115726843616A2D15596B7 (void);
// 0x0000026A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.MixedRealityCapabilityDemo::.ctor()
extern void MixedRealityCapabilityDemo__ctor_mEF59E783399C76E039623CAC4C96391B80B0EDFB (void);
// 0x0000026B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::OnEnable()
extern void BoundingBoxExampleTest_OnEnable_m65D4644D563AB28D1C2C7B3DF566353A85276F42 (void);
// 0x0000026C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::OnDisable()
extern void BoundingBoxExampleTest_OnDisable_mE5B2A1090E831F707BF7FA2CB96FE5171D4C0770 (void);
// 0x0000026D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::Start()
extern void BoundingBoxExampleTest_Start_mD1B2522A85F1B5148511B145F68525B00B9AFFE8 (void);
// 0x0000026E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::SetStatus(System.String)
extern void BoundingBoxExampleTest_SetStatus_mAB092B537F3279C2763D5CDEA8581C77AA6C9853 (void);
// 0x0000026F System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::Sequence()
extern void BoundingBoxExampleTest_Sequence_m52B3C449C585EB03563F40D49531BB49F96F31DB (void);
// 0x00000270 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::WaitForSpeechCommand()
extern void BoundingBoxExampleTest_WaitForSpeechCommand_mEF5751F616E21567794FCB92397AA9BC67EBAFD9 (void);
// 0x00000271 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void BoundingBoxExampleTest_OnSpeechKeywordRecognized_m222463231C0659533D4B9EAF6431BB38292E4798 (void);
// 0x00000272 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::.ctor()
extern void BoundingBoxExampleTest__ctor_m6C5C3EC8C9674650FD2FF831805C59C4871BEE93 (void);
// 0x00000273 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::<Sequence>b__12_0(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundingBoxExampleTest_U3CSequenceU3Eb__12_0_mFF939D0D67AA1DF5F1F85FE83E45D4FCDF6278C1 (void);
// 0x00000274 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::<Sequence>b__12_1(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundingBoxExampleTest_U3CSequenceU3Eb__12_1_m0575870681F4EA0D248901AB93FDD48BB8F56280 (void);
// 0x00000275 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::.ctor(System.Int32)
extern void U3CSequenceU3Ed__12__ctor_m7D5B9C81C79BDFDA12B9C2630156D85048EC8CF3 (void);
// 0x00000276 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.IDisposable.Dispose()
extern void U3CSequenceU3Ed__12_System_IDisposable_Dispose_m4E214E8D6DCF7575C01A264A47D3B88F9E77851C (void);
// 0x00000277 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::MoveNext()
extern void U3CSequenceU3Ed__12_MoveNext_m255898E02B9A76DA875999DFBB658DB7B5834114 (void);
// 0x00000278 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5FE29693EF5BB291C1BACB0F7950360E6F45AAC9 (void);
// 0x00000279 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.Collections.IEnumerator.Reset()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_mA28B956659A382EA5DF3C7E8EC322CB548EB49FC (void);
// 0x0000027A System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_m2FF6E0ECCBA04C71AD0BB20D0FFFCC605D72CFAF (void);
// 0x0000027B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__13::.ctor(System.Int32)
extern void U3CWaitForSpeechCommandU3Ed__13__ctor_m6FB0854046E56A3E0B22E796BCD6EED6EF8CF782 (void);
// 0x0000027C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__13::System.IDisposable.Dispose()
extern void U3CWaitForSpeechCommandU3Ed__13_System_IDisposable_Dispose_m283D88A6B0AD0FAE77E4453D509E33A61409117D (void);
// 0x0000027D System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__13::MoveNext()
extern void U3CWaitForSpeechCommandU3Ed__13_MoveNext_m4EC32F7EDA8014CBFBA9F3864E08E4588A143277 (void);
// 0x0000027E System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B3A3044C25185F3AEFF5BE7B1480F5BF0727C64 (void);
// 0x0000027F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__13::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSpeechCommandU3Ed__13_System_Collections_IEnumerator_Reset_m776239BEEA992B4EC07BF4820B48DF308E9BD601 (void);
// 0x00000280 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__13_System_Collections_IEnumerator_get_Current_m3C65E81450D102BAB16E193330897F48B3A9AE17 (void);
// 0x00000281 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::OnEnable()
extern void BoundsControlRuntimeExample_OnEnable_m5FEE3602036C62349B9537E4F7A3550ADA2FEF79 (void);
// 0x00000282 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::OnDisable()
extern void BoundsControlRuntimeExample_OnDisable_m2D717D12A295402AC6580E2787AFEFE606C78A61 (void);
// 0x00000283 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::Start()
extern void BoundsControlRuntimeExample_Start_m4984E137FCE0C1F2433910514EB8A60BBB636481 (void);
// 0x00000284 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::SetStatus(System.String)
extern void BoundsControlRuntimeExample_SetStatus_m00973213807CC01E95D059DF76F050A3E0E0D646 (void);
// 0x00000285 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::Sequence()
extern void BoundsControlRuntimeExample_Sequence_mA8BB9C4271B68A0FE6082F3C9398AE11ABE3AE0F (void);
// 0x00000286 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::WaitForSpeechCommand()
extern void BoundsControlRuntimeExample_WaitForSpeechCommand_m9FF69ADCD703DF08B2BBAF0F5AEF44F2932B2A5E (void);
// 0x00000287 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void BoundsControlRuntimeExample_OnSpeechKeywordRecognized_mD75A366084299899AD3FCF16A4A47F21174957BB (void);
// 0x00000288 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::.ctor()
extern void BoundsControlRuntimeExample__ctor_m7D81D7B40FB28D1F54B1678FE948BE6ED1D3C43D (void);
// 0x00000289 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::<Sequence>b__12_0(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundsControlRuntimeExample_U3CSequenceU3Eb__12_0_m56CBCF1CCB44BE9BF323DBE554BE7741A60EFA89 (void);
// 0x0000028A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::<Sequence>b__12_1(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundsControlRuntimeExample_U3CSequenceU3Eb__12_1_mEEDE0BEB4314834E65950313E42394FE4FE55402 (void);
// 0x0000028B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::.ctor(System.Int32)
extern void U3CSequenceU3Ed__12__ctor_mEE411CE274DF4BBEFA671D5E4915AA11AD47EA2E (void);
// 0x0000028C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.IDisposable.Dispose()
extern void U3CSequenceU3Ed__12_System_IDisposable_Dispose_m842C6733D840A95DDBE0C3946E5A79EB8A0DE880 (void);
// 0x0000028D System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::MoveNext()
extern void U3CSequenceU3Ed__12_MoveNext_mEC2CF8A264CC326E6EEED4581E3935DC2879DE75 (void);
// 0x0000028E System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA082D74DD5FA1A0A6FABB4726EC3E5F2E22122E (void);
// 0x0000028F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.Collections.IEnumerator.Reset()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m12DEA3D25C8E2D2B8DE6CFDCE72FA72030F3DD15 (void);
// 0x00000290 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_m04CCB53596226B302B9129FCB774C6A9F2171C47 (void);
// 0x00000291 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__13::.ctor(System.Int32)
extern void U3CWaitForSpeechCommandU3Ed__13__ctor_m7D65D5CDE2156C2AD5B4FB7E9B352CDD5A8EF794 (void);
// 0x00000292 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__13::System.IDisposable.Dispose()
extern void U3CWaitForSpeechCommandU3Ed__13_System_IDisposable_Dispose_m00C33208D9802F3CAEB3B9C28D77A965D3057B81 (void);
// 0x00000293 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__13::MoveNext()
extern void U3CWaitForSpeechCommandU3Ed__13_MoveNext_m5775500DE49A84D509EC4A786B552B6C1EB630D5 (void);
// 0x00000294 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19889DA87AF7EE3277D4E7AF0B6CA4A87FE82DFA (void);
// 0x00000295 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__13::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSpeechCommandU3Ed__13_System_Collections_IEnumerator_Reset_mACA03EFCCE267FDE046277176D400C200D8E97C8 (void);
// 0x00000296 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__13_System_Collections_IEnumerator_get_Current_mBF8E18BCB26595D70BE4393A9F47903A7A2ABE92 (void);
// 0x00000297 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::NextLayout()
extern void GridObjectLayoutControl_NextLayout_m63F33E41D821EEAF99FE89416D133368DAFDA796 (void);
// 0x00000298 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::PreviousLayout()
extern void GridObjectLayoutControl_PreviousLayout_m8833FFB21CFC47001DFAF3CF1F5F2DCF1309D256 (void);
// 0x00000299 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::RunTest()
extern void GridObjectLayoutControl_RunTest_mF5B8DC192FE845D4074630DC2185AF0E0860D663 (void);
// 0x0000029A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::Start()
extern void GridObjectLayoutControl_Start_m1A140D0FCD14E5F76D2DCF9209D4311BFB2F42A3 (void);
// 0x0000029B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::UpdateUI()
extern void GridObjectLayoutControl_UpdateUI_m92C6BA426052AC25EB90F414934D11D4CC289B37 (void);
// 0x0000029C System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::TestAnchors()
extern void GridObjectLayoutControl_TestAnchors_m460F0482E8D73A8E9749B93D35D1B8734D88825F (void);
// 0x0000029D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::PrintGrid(Microsoft.MixedReality.Toolkit.Utilities.GridObjectCollection,System.String,System.String,System.Text.StringBuilder)
extern void GridObjectLayoutControl_PrintGrid_mE315C6E513E6E21AB49DDA56E9CE753DB53D39BA (void);
// 0x0000029E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::.ctor()
extern void GridObjectLayoutControl__ctor_m34100C6FE9DFD6658FD3705FEDC8FB2A83F72D5F (void);
// 0x0000029F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::.ctor(System.Int32)
extern void U3CTestAnchorsU3Ed__7__ctor_mD8981BD02A905CEAC06D4E553E23F2BFE65F342F (void);
// 0x000002A0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.IDisposable.Dispose()
extern void U3CTestAnchorsU3Ed__7_System_IDisposable_Dispose_m9AE8C2217C68186641F8D54E874B247AD4745E5F (void);
// 0x000002A1 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::MoveNext()
extern void U3CTestAnchorsU3Ed__7_MoveNext_m5C729D7AD04897E69B72817DE63A876D32DC28D1 (void);
// 0x000002A2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::<>m__Finally1()
extern void U3CTestAnchorsU3Ed__7_U3CU3Em__Finally1_mE68AF14C40C20BB79882F1BC1C1AEF1E272FFD8B (void);
// 0x000002A3 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTestAnchorsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51B67DA947B38655EA84BBB181DD7D3963D89AE5 (void);
// 0x000002A4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_Reset_m2B037080877E9B04CEE6A653552909A5E84AD6F6 (void);
// 0x000002A5 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_get_Current_m6F94688A43879EBEBFFB8F5AFD784F5983F77EAA (void);
// 0x000002A6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::Start()
extern void ChangeManipulation_Start_mD91FF29A289DCAD9C6A28F127D682E146985F211 (void);
// 0x000002A7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::Update()
extern void ChangeManipulation_Update_m777E8F752BDF582DE203BAB159385964389C66B2 (void);
// 0x000002A8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::TryStopManipulation()
extern void ChangeManipulation_TryStopManipulation_m750D6248316C1A1223BDC8E27529437B2CA8F28C (void);
// 0x000002A9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::.ctor()
extern void ChangeManipulation__ctor_m6270F997C7154555604B8ED7F6D82969AD1E8B4A (void);
// 0x000002AA UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_FrontBounds()
extern void ReturnToBounds_get_FrontBounds_mB457BE973F0D448D74242EE9755157B225668ED1 (void);
// 0x000002AB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_FrontBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_FrontBounds_m555C5617E7CBECB7F45ED637388F0D50FFC7179F (void);
// 0x000002AC UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_BackBounds()
extern void ReturnToBounds_get_BackBounds_mB42CE86FF3653B03A925133CD8B217A2A8CE9507 (void);
// 0x000002AD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_BackBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_BackBounds_m7D68CDBB58F3858F9BD97FF0DFC5228DB11B1E19 (void);
// 0x000002AE UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_LeftBounds()
extern void ReturnToBounds_get_LeftBounds_m6E4FCE9513A5DE720BC582E8F4643978AF536530 (void);
// 0x000002AF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_LeftBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_LeftBounds_mD3DB7047DC4D610B07D55B2442820818D1905BB1 (void);
// 0x000002B0 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_RightBounds()
extern void ReturnToBounds_get_RightBounds_mA048A83709507E2ABD9CC8FFABB3BFF7C29BF727 (void);
// 0x000002B1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_RightBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_RightBounds_m60BFDC1C4E97FC752A7F2E6E4C2C5B7855672F0C (void);
// 0x000002B2 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_BottomBounds()
extern void ReturnToBounds_get_BottomBounds_m070E85502D0CF39ACA58234F68FF4520A3A1CC22 (void);
// 0x000002B3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_BottomBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_BottomBounds_mB81669774276DB28C307EE90B7CD3F059232B86F (void);
// 0x000002B4 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_TopBounds()
extern void ReturnToBounds_get_TopBounds_mC93B14634A492562AA098E6C89EF8C24241FF603 (void);
// 0x000002B5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_TopBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_TopBounds_m7C1A2A9216A2CCFB55A70C3F2A39FBD635B48FCA (void);
// 0x000002B6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::Start()
extern void ReturnToBounds_Start_mC403C2B3F83C8DD47D4A2B7B9FC9DF635AE7E69E (void);
// 0x000002B7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::Update()
extern void ReturnToBounds_Update_m601D4D3AF13BC46BE59524D62DFC990FDDE95A66 (void);
// 0x000002B8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::.ctor()
extern void ReturnToBounds__ctor_mCE711B7D1384A0B7F146CC5B326940C0D3EE718E (void);
// 0x000002B9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.IProgressIndicatorDemoObject::StartProgressBehavior()
// 0x000002BA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnClickAsyncMethod()
extern void ProgressIndicatorDemo_OnClickAsyncMethod_m4AFC89EC4E9860391D35149A26FBF2077B425CA2 (void);
// 0x000002BB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnClickAnimation()
extern void ProgressIndicatorDemo_OnClickAnimation_m8F9A6B01C33501EB349FFB2D08F3E3F7E6B39497 (void);
// 0x000002BC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnClickSceneLoad()
extern void ProgressIndicatorDemo_OnClickSceneLoad_mE17447FE149FA998570CE3F01D895AB8F1ED7AA2 (void);
// 0x000002BD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::HandleButtonClick(Microsoft.MixedReality.Toolkit.Examples.Demos.IProgressIndicatorDemoObject)
extern void ProgressIndicatorDemo_HandleButtonClick_m546F9C1E1AC95322A60BF294E495858C2D3113EE (void);
// 0x000002BE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::Update()
extern void ProgressIndicatorDemo_Update_m8405DC4D5740E287CFE059E8F16E0574955D1EAA (void);
// 0x000002BF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::.ctor()
extern void ProgressIndicatorDemo__ctor_mF2BC513AA5E94F646D8674BD176D2FA3BD2DB39E (void);
// 0x000002C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAnimation::Awake()
extern void ProgressIndicatorDemoAnimation_Awake_m4DD8C7D5A1F6184006B8962C4E4B4EEE242121EB (void);
// 0x000002C1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAnimation::StartProgressBehavior()
extern void ProgressIndicatorDemoAnimation_StartProgressBehavior_m5F5E6F76E2291F2ECAD20EB4B22484B993A4BD1C (void);
// 0x000002C2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAnimation::.ctor()
extern void ProgressIndicatorDemoAnimation__ctor_m78C8F5F9EE7AAD21AC90746925CAE8DAA9C92AC8 (void);
// 0x000002C3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAnimation/<StartProgressBehavior>d__5::MoveNext()
extern void U3CStartProgressBehaviorU3Ed__5_MoveNext_m843CD393E4AEA0499AC8C62E669E7FE3FE902BFB (void);
// 0x000002C4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAnimation/<StartProgressBehavior>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartProgressBehaviorU3Ed__5_SetStateMachine_m96AD4A4F814E2FF54ADAC5BE1C8586F426D9754A (void);
// 0x000002C5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAsyncMethod::Awake()
extern void ProgressIndicatorDemoAsyncMethod_Awake_m894FECFBA9C55ED31EBDD33159891A33E6954072 (void);
// 0x000002C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAsyncMethod::OnDisable()
extern void ProgressIndicatorDemoAsyncMethod_OnDisable_m7A25E3EEA0BAD28C269F60FD27EC0D745819DCB8 (void);
// 0x000002C7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAsyncMethod::StartProgressBehavior()
extern void ProgressIndicatorDemoAsyncMethod_StartProgressBehavior_m0CFCCE8B6DC603BD6CAEBC586D456EE5870DFCA5 (void);
// 0x000002C8 System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAsyncMethod::AsyncMethod(System.Threading.CancellationToken)
extern void ProgressIndicatorDemoAsyncMethod_AsyncMethod_m55E60A89B6C77FA28494E3316906DB72B7280012 (void);
// 0x000002C9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAsyncMethod::.ctor()
extern void ProgressIndicatorDemoAsyncMethod__ctor_m8F956C74FAF9AE455670C10DB108DC62AFE4C8BB (void);
// 0x000002CA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAsyncMethod/<StartProgressBehavior>d__8::MoveNext()
extern void U3CStartProgressBehaviorU3Ed__8_MoveNext_m62CDA140A7821419665E435C3EA5D8FB59D572AF (void);
// 0x000002CB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAsyncMethod/<StartProgressBehavior>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartProgressBehaviorU3Ed__8_SetStateMachine_m19D69CC62B297B3B0B8BAD06E33CEDF96E276168 (void);
// 0x000002CC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAsyncMethod/<AsyncMethod>d__9::MoveNext()
extern void U3CAsyncMethodU3Ed__9_MoveNext_m575F58B2568865FC9699CCBF4B5BA4F73A3416DE (void);
// 0x000002CD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoAsyncMethod/<AsyncMethod>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncMethodU3Ed__9_SetStateMachine_m0424443862F055A35A9F8D61AAD3A0CD2E1D4270 (void);
// 0x000002CE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoSceneLoad::StartProgressBehavior()
extern void ProgressIndicatorDemoSceneLoad_StartProgressBehavior_m47BF66F004A24A7A77451856F494A855F6C3CEBC (void);
// 0x000002CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoSceneLoad::.ctor()
extern void ProgressIndicatorDemoSceneLoad__ctor_m47A8BA173E362F1D674F7DB2F88330F812FD416A (void);
// 0x000002D0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoSceneLoad/<StartProgressBehavior>d__4::MoveNext()
extern void U3CStartProgressBehaviorU3Ed__4_MoveNext_m9A41723CF8D8E4172E0C693C410C7CA572BA0F70 (void);
// 0x000002D1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemoSceneLoad/<StartProgressBehavior>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartProgressBehaviorU3Ed__4_SetStateMachine_m741306E6BDBB3CB825E7EDD5CD8966E7E1C2E19B (void);
// 0x000002D2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SliderLunarLander::OnSliderUpdated(Microsoft.MixedReality.Toolkit.UI.SliderEventData)
extern void SliderLunarLander_OnSliderUpdated_m7CE53650B5473FADC95CA219242762930222F023 (void);
// 0x000002D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SliderLunarLander::.ctor()
extern void SliderLunarLander__ctor_mC189F20CF4ACA20A6B80B2B0C52AE4668238D23E (void);
// 0x000002D4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::Update()
extern void ReadingModeSceneBehavior_Update_m8C51B3FCD16F0BC0F8B6D6550D4D44CEB0915537 (void);
// 0x000002D5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::EnableReadingMode()
extern void ReadingModeSceneBehavior_EnableReadingMode_m4A6DFB4C1ED3A851C196B7E4DBC6637C56CDED23 (void);
// 0x000002D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::DisableReadingMode()
extern void ReadingModeSceneBehavior_DisableReadingMode_mA4701614B9E60F72691B3DC46A1FF640A1CF4E40 (void);
// 0x000002D7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::.ctor()
extern void ReadingModeSceneBehavior__ctor_mACCAF8EB527251E4ADC7242AE5A3E132C6C8FBFF (void);
// 0x000002D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Awake()
extern void ColorTap_Awake_m84412C8FBF5AFD3119FD285E70B5AEA62E607756 (void);
// 0x000002D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusEnter(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m3DA2781147C846F573869FAAAE1AA006AE1A7307 (void);
// 0x000002DA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusExit(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_mAC3B9A3842F6747B6E47C72C183A2905AABAE1A2 (void);
// 0x000002DB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m90239C3F08F0D548AEE61E1484A0C9973BCA87B1 (void);
// 0x000002DC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mC567C24B2A722B648C7E7DA8AB3CF6CB56399B45 (void);
// 0x000002DD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m27A9C3FC647EDAB1B94BCE967CB7367E289F0EB8 (void);
// 0x000002DE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m1192E6701B81AE0D2F93312DD8C1AB074B1CF0E4 (void);
// 0x000002DF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::.ctor()
extern void ColorTap__ctor_mBDC9ACBB0F08D97576451039B760776F1554424A (void);
// 0x000002E0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGazeGazeProvider::Update()
extern void FollowEyeGazeGazeProvider_Update_m98D68F320BF5BB163F91EFEBD99B3BACE990FB00 (void);
// 0x000002E1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGazeGazeProvider::.ctor()
extern void FollowEyeGazeGazeProvider__ctor_m661224A5B8E28A46EC1DDB3853E48312FE650298 (void);
// 0x000002E2 Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeSaccadeProvider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::get_EyeSaccadeProvider()
extern void PanZoomBase_get_EyeSaccadeProvider_m3248858ECEBA0E4B0CE0A904111BE03ECAF6D5E2 (void);
// 0x000002E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Initialize()
// 0x000002E4 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ComputePanSpeed(System.Single,System.Single,System.Single)
// 0x000002E5 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomDir(System.Boolean)
// 0x000002E6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomIn()
// 0x000002E7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomOut()
// 0x000002E8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdatePanZoom()
// 0x000002E9 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdateCursorPosInHitBox()
// 0x000002EA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Start()
extern void PanZoomBase_Start_m8A04E039C1CE199E7A16C6ABA900908FCD5A35C1 (void);
// 0x000002EB UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::get_CustomColliderSizeOnLookAt()
extern void PanZoomBase_get_CustomColliderSizeOnLookAt_m72D050A963DFFB77A3BA3A8CFA5D601D3AF22EB5 (void);
// 0x000002EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::AutoPan()
extern void PanZoomBase_AutoPan_m164E573B0A35591F0407FF2EED6465113523C7C5 (void);
// 0x000002ED System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanHorizontally(System.Single)
extern void PanZoomBase_PanHorizontally_mBD47406466E6D91DE52246502349272BB2383C94 (void);
// 0x000002EE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanVertically(System.Single)
extern void PanZoomBase_PanVertically_m134E1CA9477495F67D6A92DF07567C5451A3130A (void);
// 0x000002EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::EnableHandZoom()
extern void PanZoomBase_EnableHandZoom_m660373CC07370139C9B024362A9DA4502E75BBBD (void);
// 0x000002F0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::DisableHandZoom()
extern void PanZoomBase_DisableHandZoom_m33D24D81C7FB05C8C6FBEA8F68BB0173BCEC5D7C (void);
// 0x000002F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomStart(System.Boolean)
extern void PanZoomBase_ZoomStart_m5B5D13DB90F747DE0C8475894938D95D28621FE5 (void);
// 0x000002F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomInStart()
extern void PanZoomBase_ZoomInStart_mA7A44A250CAC114F416C2A69F3C28E7DCB725053 (void);
// 0x000002F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomOutStart()
extern void PanZoomBase_ZoomOutStart_m41D8D9B0FBF580B75027AD5EFB9C23E135737598 (void);
// 0x000002F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomStop()
extern void PanZoomBase_ZoomStop_mFA8EA9B7C598D50DCF75E99F981F50545DD64768 (void);
// 0x000002F5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::NavigationStart()
extern void PanZoomBase_NavigationStart_mEA9B866904719D251391E07A4D33C47CD69E8255 (void);
// 0x000002F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::NavigationStop()
extern void PanZoomBase_NavigationStop_mEC075B2C1784D78D7EAA89575DB11C268BAAF0C2 (void);
// 0x000002F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::NavigationUpdate(UnityEngine.Vector3)
extern void PanZoomBase_NavigationUpdate_mDC40421228553D6B11B3DCF60669ADEAE9D23561 (void);
// 0x000002F8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Update()
extern void PanZoomBase_Update_m58E69D529DC18B0D9488C92E32BB4F2B2DF9EC19 (void);
// 0x000002F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::SetSkimProofUpdateSpeed(System.Single)
extern void PanZoomBase_SetSkimProofUpdateSpeed_m007168E7E69932022E434B4AD2A463281DF7C0AE (void);
// 0x000002FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ResetNormFixator()
extern void PanZoomBase_ResetNormFixator_mD2A4BF297EC011A4F0D908F5A0CE865D3844CC30 (void);
// 0x000002FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::IncrementNormFixator()
extern void PanZoomBase_IncrementNormFixator_m045A103A640C1739F1F90B2BECD25EB802DAF4EE (void);
// 0x000002FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ResetScroll_OnSaccade()
extern void PanZoomBase_ResetScroll_OnSaccade_mD4BA794815B32ADB255281886775240741280379 (void);
// 0x000002FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::LateUpdate()
extern void PanZoomBase_LateUpdate_m3EBF526CB2E72EAADAFA281F89CD97E33F61EDC3 (void);
// 0x000002FE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::AutomaticGazePanning()
extern void PanZoomBase_AutomaticGazePanning_m502BA9E0F27A4BE4161D3A31F934B46A02672938 (void);
// 0x000002FF UnityEngine.BoxCollider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::get_MyCollider()
extern void PanZoomBase_get_MyCollider_m194C5A33AEA0119F02AA7D7A4C2ADCCE91E65205 (void);
// 0x00000300 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::set_MyCollider(UnityEngine.BoxCollider)
extern void PanZoomBase_set_MyCollider_m3601EA1650AC30987C3886D978221D11D1C26521 (void);
// 0x00000301 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanUpDown(System.Single)
extern void PanZoomBase_PanUpDown_mF42BE0F2FC509917C4D3D102122DEE0F52DBF636 (void);
// 0x00000302 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanLeftRight(System.Single)
extern void PanZoomBase_PanLeftRight_m3820E1407101D5481B1A953D05E02082256732D6 (void);
// 0x00000303 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdateZoom()
extern void PanZoomBase_UpdateZoom_mD1E991986CC47502A40D7ED13412EDFE3FC080E3 (void);
// 0x00000304 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::LimitScaling(UnityEngine.Vector2)
extern void PanZoomBase_LimitScaling_mFF90B882CCD8D695E5C7C1091BA9E2EC03A5FEF8 (void);
// 0x00000305 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomIn_Timed()
extern void PanZoomBase_ZoomIn_Timed_mAF4A3A7C094AFC9D1B093761743513125D5816A3 (void);
// 0x00000306 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomOut_Timed()
extern void PanZoomBase_ZoomOut_Timed_mC51CB3EBB9CB0EAC6FBE35048EE6FE5F43CF1AD2 (void);
// 0x00000307 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomAndStop(System.Boolean)
extern void PanZoomBase_ZoomAndStop_m64A048F504DE97CB1F6A8BF71DF2A82DE3C5812C (void);
// 0x00000308 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdateValues(T&,T)
// 0x00000309 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::StartFocusing()
extern void PanZoomBase_StartFocusing_m5981DDA2897223BCD93D08C936F17DAF9BA1EBB6 (void);
// 0x0000030A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::StopFocusing()
extern void PanZoomBase_StopFocusing_m19F65737E6882E737A04AD78A88882B0138411DF (void);
// 0x0000030B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m59AA6B638526735A774D1F1FC018DAF6926EE522 (void);
// 0x0000030C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m0679F164BE197BA19E978FE7D9ADA9D7C3753E58 (void);
// 0x0000030D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m0C0FC4301B0443872BD11D19E4AE76CF8FE04162 (void);
// 0x0000030E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m24672ADBA0A69BB023F6305334BDC8175BF25E59 (void);
// 0x0000030F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusEnter(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m285A644A29521942D7F353D2D52E2424947A69F3 (void);
// 0x00000310 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusExit(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_m334DCC053C7479237231FE85A70B640999062E08 (void);
// 0x00000311 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_mABCFE1E5856B66CFE3BD4F084E19145EF0AFED82 (void);
// 0x00000312 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m907D3655F053E13FC314372D75207004F8773A10 (void);
// 0x00000313 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointHandler.OnHandJointsUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<System.Collections.Generic.IDictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>>)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_m2BBA97C2E0E331868E9F4ABF83F7B28C9E5EE2E9 (void);
// 0x00000314 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::.ctor()
extern void PanZoomBase__ctor_m6A39E0307104CECE258D5A744D668DD9E81B33B8 (void);
// 0x00000315 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::.ctor(System.Int32)
extern void U3CZoomAndStopU3Ed__78__ctor_m638B15B7B300137A28BD3DB389EA4E661A250DC9 (void);
// 0x00000316 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.IDisposable.Dispose()
extern void U3CZoomAndStopU3Ed__78_System_IDisposable_Dispose_mC642298070681920CB57E9D3A6A3D79173510C9E (void);
// 0x00000317 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::MoveNext()
extern void U3CZoomAndStopU3Ed__78_MoveNext_mD8F80AA1077677243A4DBE00A8AADE0EEE002E10 (void);
// 0x00000318 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CZoomAndStopU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58F8EEAFC96F67745BD50AA736A98523ECE0A535 (void);
// 0x00000319 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.Collections.IEnumerator.Reset()
extern void U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_Reset_mAF68F9A8D6BF0ECD2B6162431A766D1937FD9EF5 (void);
// 0x0000031A System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.Collections.IEnumerator.get_Current()
extern void U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_get_Current_mFCDB5FCAB8ADE755CBB4761264D1D56CCCB4FBF3 (void);
// 0x0000031B System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::get_IsValid()
extern void PanZoomBaseRectTransf_get_IsValid_mF8F280F23EC8ADD0B492E4A1A6F00F80D237BC77 (void);
// 0x0000031C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::Initialize()
extern void PanZoomBaseRectTransf_Initialize_m11A1FC45621BCE93106ED707278B1B89930E9588 (void);
// 0x0000031D System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ComputePanSpeed(System.Single,System.Single,System.Single)
extern void PanZoomBaseRectTransf_ComputePanSpeed_m7D51253DC6497D441171CEB43C1DA109220B8919 (void);
// 0x0000031E System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomDir(System.Boolean)
extern void PanZoomBaseRectTransf_ZoomDir_m75F5A5BE5D97693BFCEDEB3FF279CE796DF54346 (void);
// 0x0000031F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomIn()
extern void PanZoomBaseRectTransf_ZoomIn_m87C54B437F84B3690EDC72DE1CA7E13E43D972E2 (void);
// 0x00000320 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomOut()
extern void PanZoomBaseRectTransf_ZoomOut_mF874267338C487B1A6F92E58676000A6EDE916BB (void);
// 0x00000321 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::UpdatePanZoom()
extern void PanZoomBaseRectTransf_UpdatePanZoom_m3D5B5008FD1429089ED9913186D9CC57B8962814 (void);
// 0x00000322 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::LimitPanning()
extern void PanZoomBaseRectTransf_LimitPanning_m7667E59FEA84E5B603FC4F4260BFA806D17C7F4E (void);
// 0x00000323 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomInOut_RectTransform(System.Single,UnityEngine.Vector2)
extern void PanZoomBaseRectTransf_ZoomInOut_RectTransform_m671161DF1C1718D8BB5FCE719336C6ED42B566F9 (void);
// 0x00000324 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::UpdateCursorPosInHitBox()
extern void PanZoomBaseRectTransf_UpdateCursorPosInHitBox_m67DDAA6E01BBC4CDB1AB284FEE3082110E5AF835 (void);
// 0x00000325 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::.ctor()
extern void PanZoomBaseRectTransf__ctor_mCFC3693AC6F1EBD355C8C0CD6474008F814A36D7 (void);
// 0x00000326 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::get_TextureShaderProperty()
extern void PanZoomBaseTexture_get_TextureShaderProperty_m6E5B74418BC03CC7271A6431FD58282B44CE6956 (void);
// 0x00000327 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::set_TextureShaderProperty(System.String)
extern void PanZoomBaseTexture_set_TextureShaderProperty_mD84BCFC3CFCCC0319803FF2AB248063C7853C601 (void);
// 0x00000328 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::get_IsValid()
extern void PanZoomBaseTexture_get_IsValid_m56A27D26611381EE3E67A10758CDEADFE1CE72B6 (void);
// 0x00000329 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::Initialize()
extern void PanZoomBaseTexture_Initialize_m030ED873A5DDA37C756FECA2A2D80233B3AA5910 (void);
// 0x0000032A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::Initialize(System.Single)
extern void PanZoomBaseTexture_Initialize_mFC59DB01692EC496F6B17574F3C722A70D42090F (void);
// 0x0000032B System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ComputePanSpeed(System.Single,System.Single,System.Single)
extern void PanZoomBaseTexture_ComputePanSpeed_m462C0EBB76E42B45DC99E58DE2889DCED1A847E1 (void);
// 0x0000032C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::UpdatePanZoom()
extern void PanZoomBaseTexture_UpdatePanZoom_m104FE221E39C655CAA4F0293526EA5FB6B5A1A7A (void);
// 0x0000032D System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomDir(System.Boolean)
extern void PanZoomBaseTexture_ZoomDir_mB40612879DC4F642BDDEB1202210CA0006F4413D (void);
// 0x0000032E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomIn()
extern void PanZoomBaseTexture_ZoomIn_mA02349F004FD1145B0223B67FF865297BFF23E83 (void);
// 0x0000032F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomOut()
extern void PanZoomBaseTexture_ZoomOut_m343D4B1072594247E257FDFED039286CF69B0ED9 (void);
// 0x00000330 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomInOut(System.Single,UnityEngine.Vector2)
extern void PanZoomBaseTexture_ZoomInOut_m5945C81949933193484A76A6B377FD3682A008CC (void);
// 0x00000331 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::UpdateCursorPosInHitBox()
extern void PanZoomBaseTexture_UpdateCursorPosInHitBox_mBBB877DE821EA1F1E964ACD00D8A65D2BEEAB454 (void);
// 0x00000332 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::.ctor()
extern void PanZoomBaseTexture__ctor_m50806B717B65CC5442881F6D0CEF4978D749F948 (void);
// 0x00000333 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomRectTransf::Start()
extern void PanZoomRectTransf_Start_mAFBB206E442ACD8B39AA3A55E98C305AB74A5C9A (void);
// 0x00000334 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomRectTransf::Update()
extern void PanZoomRectTransf_Update_m773ECEE72AB88B979F79DD6591F4F04623C303CE (void);
// 0x00000335 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomRectTransf::.ctor()
extern void PanZoomRectTransf__ctor_mAC045A21DFBAA5059924D51C1435C65E02A04541 (void);
// 0x00000336 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomTexture::Start()
extern void PanZoomTexture_Start_mBEBD156134214866BEF430A6EC0098A2547E9EBE (void);
// 0x00000337 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomTexture::Update()
extern void PanZoomTexture_Update_m24A0206949E90D6093F247DC891C13A2388B7273 (void);
// 0x00000338 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomTexture::.ctor()
extern void PanZoomTexture__ctor_m683619F4430ED9453E37800A7AD323DBC5ED9E64 (void);
// 0x00000339 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::Start()
extern void ScrollRectTransf_Start_m3131601857480EF1C51D93B0167C8B0EE62FE7A6 (void);
// 0x0000033A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::UpdatePivot()
extern void ScrollRectTransf_UpdatePivot_m294A9187F156FDB87D76E331517EE5C95ADA5F02 (void);
// 0x0000033B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::Update()
extern void ScrollRectTransf_Update_m3AF072ACA3D721A9ED5BF1C736B34605E621C791 (void);
// 0x0000033C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::.ctor()
extern void ScrollRectTransf__ctor_m0697E1957609F787592AF7A4CE6FCA227AF65C4B (void);
// 0x0000033D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollTexture::Start()
extern void ScrollTexture_Start_mE65D03492F1320B7692EFF5A1E9C0B6054F73C37 (void);
// 0x0000033E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollTexture::Update()
extern void ScrollTexture_Update_m0B495C5D035159D88BEC0AD28898443788A5F08B (void);
// 0x0000033F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollTexture::.ctor()
extern void ScrollTexture__ctor_m597F8BB593059F31C25F817006642208CA04A488 (void);
// 0x00000340 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::Start()
extern void TargetMoveToCamera_Start_m09F48A502C0C17D003C31300E23C63EB57B54406 (void);
// 0x00000341 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::Update()
extern void TargetMoveToCamera_Update_m724772FDE9B7F3820AD117BAF3B86A002AF0766E (void);
// 0x00000342 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::OnEyeFocusStop()
extern void TargetMoveToCamera_OnEyeFocusStop_mD8398AE4AB348A962D1D6522AD3B37613EEA1585 (void);
// 0x00000343 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::OnSelect()
extern void TargetMoveToCamera_OnSelect_m613F9F0E2BD42F84EEC902C381D73EF3295B6418 (void);
// 0x00000344 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::TransitionToUser()
extern void TargetMoveToCamera_TransitionToUser_m49F812DE104D2407D3FA239A14A3898EAC6E5134 (void);
// 0x00000345 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::ReturnHome()
extern void TargetMoveToCamera_ReturnHome_m48058DD46D16F5AEEC3817FB2110710466760FEE (void);
// 0x00000346 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::TransitionToCamera()
extern void TargetMoveToCamera_TransitionToCamera_m9EA4B4791F039882903950FBA21C510BD934636D (void);
// 0x00000347 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::.ctor()
extern void TargetMoveToCamera__ctor_m931AFEED9468462FA0D148FE896772C061A2A2C8 (void);
// 0x00000348 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Awake()
extern void GrabReleaseDetector_Awake_m6C822802F211FB028EBE301323EF935ED067FFA0 (void);
// 0x00000349 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mCF7979FD72036AF5A73ED6D283C9EE9816A035C7 (void);
// 0x0000034A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m378F6F6544589A9A2F7F6A1E3B69F14D3619E6E5 (void);
// 0x0000034B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m241503EE2DFBA41EE1222C71D2DEAF2A9D659C8E (void);
// 0x0000034C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mA50F54588E42F89A1DB70686D9753DF1C5E0D205 (void);
// 0x0000034D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::.ctor()
extern void GrabReleaseDetector__ctor_m30242BEF7351AF2B2D6D3A329378A15081DEC51B (void);
// 0x0000034E Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_EyeTrackingProvider()
extern void MoveObjByEyeGaze_get_EyeTrackingProvider_mF37265972A7CAE9B0BEC457B536398347647E49D (void);
// 0x0000034F System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_ConstrX()
extern void MoveObjByEyeGaze_get_ConstrX_mB214E0F5621B4046A14627DC8986E7E591DE4488 (void);
// 0x00000350 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_ConstrY()
extern void MoveObjByEyeGaze_get_ConstrY_mDB5F49C0CE06D0C351C904522803BBB0E2684B87 (void);
// 0x00000351 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_ConstrZ()
extern void MoveObjByEyeGaze_get_ConstrZ_m1916F5DF7056488707FF7393883A501CEB37109F (void);
// 0x00000352 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Start()
extern void MoveObjByEyeGaze_Start_mE82BCB194B8840814A71EF9ACFD6C382182ED6BB (void);
// 0x00000353 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Update()
extern void MoveObjByEyeGaze_Update_mD8A3259DB66C90E343C6467CE8011A0A63F61B4A (void);
// 0x00000354 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechHandler.OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_m77DA24AC091BF23B0DD095E2EA7C1FEB2294271F (void);
// 0x00000355 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointHandler.OnHandJointsUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<System.Collections.Generic.IDictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>>)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_mAADECD6EF417FBF98FEAD7401317BD182246F55F (void);
// 0x00000356 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_mB0B0D2CEA50ABDEAB7B63587E1649B96B4899D95 (void);
// 0x00000357 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m8DA6EF45FBD47F06123B35C75D3806AB8E84E694 (void);
// 0x00000358 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m0549A08BA45C3414F9F332ED57FC530836EA0267 (void);
// 0x00000359 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m663290EF79257B68BD27D7BA15B9B7516BDD01C9 (void);
// 0x0000035A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m9FCDB90E1B8BA321A2D5ED87692EC2BAC6779903 (void);
// 0x0000035B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::HandDrag_Start()
extern void MoveObjByEyeGaze_HandDrag_Start_m05E4C1835EFFB52FAB2C6C913821645BC1EA811F (void);
// 0x0000035C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::HandDrag_Stop()
extern void MoveObjByEyeGaze_HandDrag_Stop_mF69C155C38E3157AC026F69FD1A28E68F346E167 (void);
// 0x0000035D System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::IsLookingAwayFromTarget()
extern void MoveObjByEyeGaze_IsLookingAwayFromTarget_mF8546DEF5DDF287E6C6593CDEFCAE487E3DCD36F (void);
// 0x0000035E System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::IsLookingAwayFromPreview()
extern void MoveObjByEyeGaze_IsLookingAwayFromPreview_m42B4CC3161813E5452C8FF5D9D4A3FBF87F8F3E6 (void);
// 0x0000035F System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::IsDestinationPlausible()
extern void MoveObjByEyeGaze_IsDestinationPlausible_mAE5798A7ED7F75ABD2374C4DADDE0A2D1242F1D1 (void);
// 0x00000360 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::GetValidPlacemLocation(UnityEngine.GameObject)
extern void MoveObjByEyeGaze_GetValidPlacemLocation_m18F9AD11FC6F549FCF57285398A10AA5C12BE34A (void);
// 0x00000361 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::ActivatePreview()
extern void MoveObjByEyeGaze_ActivatePreview_m4B074C24B26862987B77CEC601DDAACA92E412CA (void);
// 0x00000362 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::DeactivatePreview()
extern void MoveObjByEyeGaze_DeactivatePreview_mBE16F4F1F0201560E6D11ED70C04B93305CCF572 (void);
// 0x00000363 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::DragAndDrop_Start()
extern void MoveObjByEyeGaze_DragAndDrop_Start_m795A08E37CD00B8AD2CBAAC467544437510EE7D9 (void);
// 0x00000364 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::DragAndDrop_Finish()
extern void MoveObjByEyeGaze_DragAndDrop_Finish_mF4ABABFEE38277274757CD6DADD6DE3B5157399D (void);
// 0x00000365 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::RelativeMoveUpdate(UnityEngine.Vector3)
extern void MoveObjByEyeGaze_RelativeMoveUpdate_mFD93941427E095EA404E32C06639724804B03305 (void);
// 0x00000366 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Angle_InitialGazeToCurrGazeDir()
extern void MoveObjByEyeGaze_Angle_InitialGazeToCurrGazeDir_m43C8CB7A6D8713258D084737D98AE2F77BEBCB92 (void);
// 0x00000367 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Angle_ToCurrHitTarget(UnityEngine.GameObject)
extern void MoveObjByEyeGaze_Angle_ToCurrHitTarget_m73D866E7B2DE2FC43713298FEFCAB6A6D71215FF (void);
// 0x00000368 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::HeadIsInMotion()
extern void MoveObjByEyeGaze_HeadIsInMotion_m5504CC98282A8D6D35F64EBE96EE1A38D1FEF6D5 (void);
// 0x00000369 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::MoveTargetBy(UnityEngine.Vector3)
extern void MoveObjByEyeGaze_MoveTargetBy_m5D24F30549C9D06AF6635E485D0D7C8C9C626078 (void);
// 0x0000036A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::UpdateSliderTextOutput()
extern void MoveObjByEyeGaze_UpdateSliderTextOutput_m68D38DCDABB561D6A52F36617A6F3FA4DE0B8B2B (void);
// 0x0000036B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::ConstrainMovement()
extern void MoveObjByEyeGaze_ConstrainMovement_m5D8922AE0963E977BC749B74A8291353BD9A9C6B (void);
// 0x0000036C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::OnDrop_SnapToClosestDecimal()
extern void MoveObjByEyeGaze_OnDrop_SnapToClosestDecimal_m02874884CD9F731A2E65D9F85F709698CFB9ACA4 (void);
// 0x0000036D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::MoveTargetTo(UnityEngine.Vector3)
extern void MoveObjByEyeGaze_MoveTargetTo_mB933B60F26C278576BCAA082C6B919E76AB323A5 (void);
// 0x0000036E System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::ShouldObjBeWarped(System.Single,System.Single,System.Boolean)
extern void MoveObjByEyeGaze_ShouldObjBeWarped_mE908CCB6AAC776C9FBB6110A063CBB809E5A7198 (void);
// 0x0000036F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mDEEA992415BDDE07DD1EE5DC8CEC6CFD3EA8D622 (void);
// 0x00000370 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::.ctor()
extern void MoveObjByEyeGaze__ctor_m450B8E6D323C466B02DAA9CA23BF342E2A859978 (void);
// 0x00000371 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SnapTo::.ctor()
extern void SnapTo__ctor_m53C3DE3026638C6D83D68102558BB3DDB0F883C6 (void);
// 0x00000372 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TransportToRespawnLocation::OnTriggerEnter(UnityEngine.Collider)
extern void TransportToRespawnLocation_OnTriggerEnter_mB9103014B6FAC89FFC5C885DBC8E13ED9F1C61CB (void);
// 0x00000373 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TransportToRespawnLocation::.ctor()
extern void TransportToRespawnLocation__ctor_m7540E5F653E2579F5106110992B11C0CA169E7F4 (void);
// 0x00000374 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::Start()
extern void TriggerZonePlaceObjsWithin_Start_m06CCD6F04A5EEBB0B744BC16257E1E9D424EDE7B (void);
// 0x00000375 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::OnTriggerEnter(UnityEngine.Collider)
extern void TriggerZonePlaceObjsWithin_OnTriggerEnter_m8C757721883DDA4D778E6DC6031A61BFEDE8B745 (void);
// 0x00000376 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::OnTriggerExit(UnityEngine.Collider)
extern void TriggerZonePlaceObjsWithin_OnTriggerExit_mAEF8DDC8CD19B3DB6B3413D89C473EC5E6E33D19 (void);
// 0x00000377 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::CheckForCompletion()
extern void TriggerZonePlaceObjsWithin_CheckForCompletion_m5BE3D58EA13D9079DA397F016A508C81258E2FF8 (void);
// 0x00000378 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::TargetDropped()
extern void TriggerZonePlaceObjsWithin_TargetDropped_mFC7940378EBE2D3FE42699973270B9B5E14DB518 (void);
// 0x00000379 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::AreWeThereYet()
extern void TriggerZonePlaceObjsWithin_AreWeThereYet_m3090ACB31FBB2DDA19AEDB816D2EB29B858078C3 (void);
// 0x0000037A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::.ctor()
extern void TriggerZonePlaceObjsWithin__ctor_m1792410F3DC29504FFAE6FE23AA63B9CD378FC2A (void);
// 0x0000037B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::Start()
extern void HitBehaviorDestroyOnSelect_Start_m03982393F6BBF9DE650646E234DCAF7C61AAD6D7 (void);
// 0x0000037C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::SetUpAudio()
extern void HitBehaviorDestroyOnSelect_SetUpAudio_mD93DC64D8328AAAA98845906141AA51385E480E5 (void);
// 0x0000037D System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::PlayAudioOnHit(UnityEngine.AudioClip)
extern void HitBehaviorDestroyOnSelect_PlayAudioOnHit_m1B43FB92F67C81B2C08821A6CD3635BC4D08D036 (void);
// 0x0000037E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::PlayAnimationOnHit()
extern void HitBehaviorDestroyOnSelect_PlayAnimationOnHit_mE7A67BA0DDCE69A60793D246085785847B2548C4 (void);
// 0x0000037F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::TargetSelected()
extern void HitBehaviorDestroyOnSelect_TargetSelected_m055F5740FAF1A2982F3472382E607703E85E97DE (void);
// 0x00000380 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::HandleTargetGridIterator()
extern void HitBehaviorDestroyOnSelect_HandleTargetGridIterator_mE33344F205174B8858B89911037480A1FC705740 (void);
// 0x00000381 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::.ctor()
extern void HitBehaviorDestroyOnSelect__ctor_m97FE187C544C7E19027FD842FDD5ED83CDA5B2B2 (void);
// 0x00000382 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.RotateWithConstSpeedDir::RotateTarget()
extern void RotateWithConstSpeedDir_RotateTarget_m6F7F96D35A2EA3DA69D84BC1061E6253A57866F3 (void);
// 0x00000383 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.RotateWithConstSpeedDir::.ctor()
extern void RotateWithConstSpeedDir__ctor_m69E08EC7D91434F0D86B3FA794D43487711F6FF9 (void);
// 0x00000384 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::Start()
extern void TargetGroupCreatorRadial_Start_mA67A53E699ED03944FBE419254D6A513882692D5 (void);
// 0x00000385 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::Update()
extern void TargetGroupCreatorRadial_Update_m869304BD0C60D5C2293CFC5A1DFDCE70CFA1A040 (void);
// 0x00000386 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::HideTemplates()
extern void TargetGroupCreatorRadial_HideTemplates_m4EC341589D7F1EC021643EDEE8B0E15BB2714BDD (void);
// 0x00000387 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::CreateNewTargets_RadialLayout()
extern void TargetGroupCreatorRadial_CreateNewTargets_RadialLayout_mAAB8C7C599356B13F544ED0BAB0BB48A206F7617 (void);
// 0x00000388 UnityEngine.GameObject[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::get_InstantiatedObjects()
extern void TargetGroupCreatorRadial_get_InstantiatedObjects_m03DF17D7DF48E79E03B248E9A81A90AB35B7604B (void);
// 0x00000389 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::GetRandomTemplate()
extern void TargetGroupCreatorRadial_GetRandomTemplate_m5C1D3E9F11457F01F5903208BCCCBD1888A63878 (void);
// 0x0000038A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::InstantiateRadialLayoutedTarget(System.Single,System.Single,System.Int32)
extern void TargetGroupCreatorRadial_InstantiateRadialLayoutedTarget_mA815D00F954F6A2678FB4B931940A6B468FBDDE9 (void);
// 0x0000038B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::KeepConstantVisAngleTargetSize()
extern void TargetGroupCreatorRadial_KeepConstantVisAngleTargetSize_m7209CBEE33800B771E5EAC909BE995885C93E3A4 (void);
// 0x0000038C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::KeepFacingTheCamera()
extern void TargetGroupCreatorRadial_KeepFacingTheCamera_m865CC37571EEC8A83BDA1A5DE36EC1E2D5E765CC (void);
// 0x0000038D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::.ctor()
extern void TargetGroupCreatorRadial__ctor_mD266490E90E7C0D6A10A182C811B588EA74798EF (void);
// 0x0000038E UnityEngine.Color Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_HighlightColor()
extern void TargetGroupIterator_get_HighlightColor_mB3A870016F53B0E8FA04EC12763CD7F5687BFC75 (void);
// 0x0000038F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::add_OnAllTargetsSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_add_OnAllTargetsSelected_m875EFD163DC043D2C9ABDFA4521C213A466C2765 (void);
// 0x00000390 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::remove_OnAllTargetsSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_remove_OnAllTargetsSelected_m3D8A5A92ECBC361394691822D37CF083DFBD8C6A (void);
// 0x00000391 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::add_OnTargetSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_add_OnTargetSelected_mCC0763436B47AC194697BE8BA5A7B518964BCAB2 (void);
// 0x00000392 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::remove_OnTargetSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_remove_OnTargetSelected_m78422C1373EE8322D86D64059A3673BB63ACD3E7 (void);
// 0x00000393 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Start()
extern void TargetGroupIterator_Start_m6DEBCAFB61A8531080CD688616B4AF1A3AB49B0A (void);
// 0x00000394 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Update()
extern void TargetGroupIterator_Update_m2AF5AC176AC94C130FBC8A665431DEC41637E030 (void);
// 0x00000395 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ResetAmountOfTries()
extern void TargetGroupIterator_ResetAmountOfTries_m9A760A2EDBA46109B4CDF0DF524677EB5C5EF546 (void);
// 0x00000396 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ResetIterator()
extern void TargetGroupIterator_ResetIterator_mD05DDCB3D1E5065425E0DEFEB80E29F50102A176 (void);
// 0x00000397 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ProceedToNextTarget()
extern void TargetGroupIterator_ProceedToNextTarget_m050D44D81CD90018D4478E83B0F6D214231C4C97 (void);
// 0x00000398 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_CurrentTarget()
extern void TargetGroupIterator_get_CurrentTarget_m15C58166B6CF52DF4DD734D0810D151EEF1D9D3C (void);
// 0x00000399 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_PreviousTarget()
extern void TargetGroupIterator_get_PreviousTarget_m9529D1243E539C7FA1AA7ADC215AB472595D7574 (void);
// 0x0000039A System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_CurrentTargetIsValid()
extern void TargetGroupIterator_get_CurrentTargetIsValid_m85CD0B51400A3C6509A7A266DC1E43710006E96D (void);
// 0x0000039B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::SaveOriginalColors()
extern void TargetGroupIterator_SaveOriginalColors_mDE9F3C8541D8B3D7647189D2E7C9534C61155D3D (void);
// 0x0000039C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HighlightTarget()
extern void TargetGroupIterator_HighlightTarget_mEA6A5D5DF62C78E0F09E140113BCFFFAFEF8FA47 (void);
// 0x0000039D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HighlightTarget(UnityEngine.Color)
extern void TargetGroupIterator_HighlightTarget_m9A3C0B0A8E0AC293F18D4233EC0B88D671400236 (void);
// 0x0000039E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ShowVisualMarkerForCurrTarget()
extern void TargetGroupIterator_ShowVisualMarkerForCurrTarget_m3A3AF2369A7A2F918221D86A3F26278428F19E67 (void);
// 0x0000039F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HideVisualMarkerForCurrTarget()
extern void TargetGroupIterator_HideVisualMarkerForCurrTarget_mCBE553D7A61FE74A45437E4CB5498193319D8285 (void);
// 0x000003A0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::UnhighlightTarget()
extern void TargetGroupIterator_UnhighlightTarget_m940FC39C7331FA812533DDFCE345A1F1E8BC24E3 (void);
// 0x000003A1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ShowHighlights()
extern void TargetGroupIterator_ShowHighlights_m01C00ABE482565898FDA8F0E55DB7E29606EE24D (void);
// 0x000003A2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HideHighlights()
extern void TargetGroupIterator_HideHighlights_m0D319DC297B214046A2AB3CFA8549A1B7B77D763 (void);
// 0x000003A3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Fire_OnAllTargetsSelected()
extern void TargetGroupIterator_Fire_OnAllTargetsSelected_m4E1E33D9A5F4513DF2A668A56378E55DCE18057B (void);
// 0x000003A4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Fire_OnTargetSelected()
extern void TargetGroupIterator_Fire_OnTargetSelected_m36DDC03C8ACD570ADA90A0451ACF7B954F328E47 (void);
// 0x000003A5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m1A2624821E1018B620994F9846AA46C5E7D6F07F (void);
// 0x000003A6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mA0343A25FA90D835F8D146D31A7AF2C5AE167594 (void);
// 0x000003A7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mEC1EA4F3941AFD7940732825AFB80A8E0BDF63C0 (void);
// 0x000003A8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m0E00CF1AF4DA7C3E69338AFA50704C4E7A33B909 (void);
// 0x000003A9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::.ctor()
extern void TargetGroupIterator__ctor_m43437D832F241D3D259CE2152D734F81CB51123B (void);
// 0x000003AA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::.ctor(System.Object,System.IntPtr)
extern void TargetGroupEventHandler__ctor_m0A0C790BB2C20720731817045008A44808CEC62D (void);
// 0x000003AB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::Invoke()
extern void TargetGroupEventHandler_Invoke_m42552E287FAB5C0CA92BD1DEABFE57B378045640 (void);
// 0x000003AC System.IAsyncResult Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern void TargetGroupEventHandler_BeginInvoke_mAF8566533726FE201775742B0C0C7C7E59943ACC (void);
// 0x000003AD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::EndInvoke(System.IAsyncResult)
extern void TargetGroupEventHandler_EndInvoke_m7DCFDB6ECC7C4BE9B08B8FE154F5653FB9AB8562 (void);
// 0x000003AE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::ShowIt()
extern void ToggleGameObject_ShowIt_mB8DD93B97261826E515806E9AEEFACD2CCF6DE78 (void);
// 0x000003AF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::HideIt()
extern void ToggleGameObject_HideIt_m8F26805E5594C1EDF7BBC446472BFBC0220383A3 (void);
// 0x000003B0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::ShowIt(System.Boolean)
extern void ToggleGameObject_ShowIt_mBCA892FE92B3F44B55B5B01555AB20413B12ADB7 (void);
// 0x000003B1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::.ctor()
extern void ToggleGameObject__ctor_m4B6464F3D9B71B314A267E36E5F2AF4F79B14130 (void);
// 0x000003B2 Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::get_EyeTarget()
extern void DrawOnTexture_get_EyeTarget_m467BFE852712E26B472E0160E52F98FDCC07A847 (void);
// 0x000003B3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::Start()
extern void DrawOnTexture_Start_m873FC4C38D3DEA3E68D9DFFCB96BF56B560A2291 (void);
// 0x000003B4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::OnLookAt()
extern void DrawOnTexture_OnLookAt_m6612932FB5FFF6AF10F7810189EC90AE5EFEA4D4 (void);
// 0x000003B5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAtThisHitPos(UnityEngine.Vector3)
extern void DrawOnTexture_DrawAtThisHitPos_m5AC10A3A5EBA1778C830210A4D5481B43708E8D7 (void);
// 0x000003B6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAt(UnityEngine.Vector2,UnityEngine.Color)
extern void DrawOnTexture_DrawAt_m4618A4966C3EC88DF8AB105F64F5C8573719E8DE (void);
// 0x000003B7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAt2(UnityEngine.Vector2,System.Int32,System.Single)
extern void DrawOnTexture_DrawAt2_m00A340F1DEB67185C5A6DFE98C71B81D5791B922 (void);
// 0x000003B8 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAt(UnityEngine.Vector2)
extern void DrawOnTexture_DrawAt_m54DB30FA4835EE628574AFB21AE537B468C0924E (void);
// 0x000003B9 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::ComputeHeatmapAt(UnityEngine.Vector2,System.Boolean,System.Boolean)
extern void DrawOnTexture_ComputeHeatmapAt_m1E92BF158DB280BAC448ADB11CC2CC46BE372626 (void);
// 0x000003BA System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::ComputeHeatmapColorAt(UnityEngine.Vector2,UnityEngine.Vector2,System.Nullable`1<UnityEngine.Color>&)
extern void DrawOnTexture_ComputeHeatmapColorAt_mFC7718FC9B1C2353A3039B4AD236297B45AAF36F (void);
// 0x000003BB UnityEngine.Renderer Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::get_MyRenderer()
extern void DrawOnTexture_get_MyRenderer_mD4DE79E1D7D62B94819CEF5F47AE258557886B2D (void);
// 0x000003BC UnityEngine.Texture2D Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::get_MyDrawTexture()
extern void DrawOnTexture_get_MyDrawTexture_m056DFBD24D6A71936784C166D4F7F321E08A27B6 (void);
// 0x000003BD System.Nullable`1<UnityEngine.Vector2> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::GetCursorPosInTexture(UnityEngine.Vector3)
extern void DrawOnTexture_GetCursorPosInTexture_mB6B0A497C35C0482A282BDE8FAE1439D25D70CF1 (void);
// 0x000003BE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::.ctor()
extern void DrawOnTexture__ctor_m80DCD7A503D81E2866DF388EDEF85BB3CE905504 (void);
// 0x000003BF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::.ctor(System.Int32)
extern void U3CDrawAtU3Ed__19__ctor_m3452D4AAE063679249F23B34F44678B64B0B90C2 (void);
// 0x000003C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.IDisposable.Dispose()
extern void U3CDrawAtU3Ed__19_System_IDisposable_Dispose_m2CAA259EE47BF4A75D6A4A0A790291FCEB7C542F (void);
// 0x000003C1 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::MoveNext()
extern void U3CDrawAtU3Ed__19_MoveNext_m5B38BC9A1076E1AB2CA656A2B4B470D04F6C20E6 (void);
// 0x000003C2 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDrawAtU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF4193E3A4C112AE16A64A0B944FA96085386D39D (void);
// 0x000003C3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.Collections.IEnumerator.Reset()
extern void U3CDrawAtU3Ed__19_System_Collections_IEnumerator_Reset_m3E856B0B07E6976D43E084468665197D8CF29673 (void);
// 0x000003C4 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CDrawAtU3Ed__19_System_Collections_IEnumerator_get_Current_m52270E6C535DCF743F6BC47C43A853B9246B87CB (void);
// 0x000003C5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::.ctor(System.Int32)
extern void U3CComputeHeatmapAtU3Ed__20__ctor_mCCF9506B4A8D95E5EEB1A1062CF5BFE1F8C6BE54 (void);
// 0x000003C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.IDisposable.Dispose()
extern void U3CComputeHeatmapAtU3Ed__20_System_IDisposable_Dispose_m3C8698642C4E061224C9CB51F43EC1B20A099AC4 (void);
// 0x000003C7 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::MoveNext()
extern void U3CComputeHeatmapAtU3Ed__20_MoveNext_m090D08FEB14BBD387DF1834E7992773C0E86259E (void);
// 0x000003C8 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CComputeHeatmapAtU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1B4C2DA82FE197A2080AC016EB60EEBE0C88F92 (void);
// 0x000003C9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.Collections.IEnumerator.Reset()
extern void U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_Reset_m69A13920489FCF0EA4532CCEA3DD91066B404841 (void);
// 0x000003CA System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_get_Current_m19328DB9FD53B9F0A6A43FCAE64A688C2614ED44 (void);
// 0x000003CB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Awake()
extern void OnSelectVisualizerInputController_Awake_m5CA45CB519EFED6A175DDE3520CB1E7E3B9BF85E (void);
// 0x000003CC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::OnTargetSelected()
extern void OnSelectVisualizerInputController_OnTargetSelected_m573D51F994EE2AB4ED01C4C50D8AEB1926CDA1CA (void);
// 0x000003CD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m168C4195481F6AB89052B13372BA31F6385763DD (void);
// 0x000003CE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m14E473EDE2C4347D1650E0CC92334C85603ACA69 (void);
// 0x000003CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mBB8D22DF9BC94B032E73E5342C747A4E0557781E (void);
// 0x000003D0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mC6C01241EA498991ABA8BC7C9023089343CA3B66 (void);
// 0x000003D1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::.ctor()
extern void OnSelectVisualizerInputController__ctor_m8665D68A78227437DF5A95CE9326D6BEB42C1F32 (void);
// 0x000003D2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::Start()
extern void ParticleHeatmap_Start_m88EE3C760F408E9D5851D46401D567076AB3497E (void);
// 0x000003D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::SetParticle(UnityEngine.Vector3)
extern void ParticleHeatmap_SetParticle_m98EED2F6DFAF3D6639C3EC47A09E27ABEC4049FA (void);
// 0x000003D4 System.Nullable`1<UnityEngine.Vector3> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::GetPositionOfParticle(System.Int32)
extern void ParticleHeatmap_GetPositionOfParticle_m0B1D752555BDB67643E55F9BD3371D41EF9F33AF (void);
// 0x000003D5 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::DetermineNormalizedIntensity(UnityEngine.Vector3,System.Single)
extern void ParticleHeatmap_DetermineNormalizedIntensity_mC71962E67254AD295016481FCAA86AE86AB12775 (void);
// 0x000003D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::UpdateColorForAllParticles()
extern void ParticleHeatmap_UpdateColorForAllParticles_mB6BD448702FEDF94C54D44C9BA241DE6CBB9EC9D (void);
// 0x000003D7 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::saturate(System.Single)
extern void ParticleHeatmap_saturate_mD123C4FF18C094DA696535D58164D7C91CB5AFB3 (void);
// 0x000003D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::DisplayParticles()
extern void ParticleHeatmap_DisplayParticles_mADC9C26BF7547F0EECBEFE35AD02232AB0B87F56 (void);
// 0x000003D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::ShowHeatmap()
extern void ParticleHeatmap_ShowHeatmap_mE1DBA3F299E2B2D25598BAA925536997C4A63F10 (void);
// 0x000003DA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::HideHeatmap()
extern void ParticleHeatmap_HideHeatmap_m118FDFF8D3C92F1CC17CE9C6C86034E17F2A0057 (void);
// 0x000003DB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::.ctor()
extern void ParticleHeatmap__ctor_mA87FAEF3D38908724374FB60FC576838D5071DF1 (void);
// 0x000003DC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmapParticleData::.ctor()
extern void ParticleHeatmapParticleData__ctor_m7C5BF207D19B830B82718265C8FB8E838F5BDD41 (void);
// 0x000003DD Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::get_Instance()
extern void AudioFeedbackPlayer_get_Instance_mCBA53A3C17CF2CD06CA0343C8A1CE725F5779B6C (void);
// 0x000003DE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::set_Instance(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer)
extern void AudioFeedbackPlayer_set_Instance_mDA0E8C26EDD76B2A5E40D371F30AC064A31EBA6C (void);
// 0x000003DF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::Start()
extern void AudioFeedbackPlayer_Start_m3D194EF14E1921665959BBF582E89A566F7C185C (void);
// 0x000003E0 UnityEngine.AudioSource Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::SetupAudioSource(UnityEngine.GameObject)
extern void AudioFeedbackPlayer_SetupAudioSource_m86C9DFBDDBDF010BE6D71184CDF80230B99998C3 (void);
// 0x000003E1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::PlaySound(UnityEngine.AudioClip)
extern void AudioFeedbackPlayer_PlaySound_m1039CF245B7D320EDA3AF6A3B4808788A3C71BC2 (void);
// 0x000003E2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::.ctor()
extern void AudioFeedbackPlayer__ctor_mB0BCA6380697B49EEC8B4CF09254C55B2A5B36FD (void);
// 0x000003E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGaze::Update()
extern void FollowEyeGaze_Update_m3137E86CF43C4DABC470B943169184B34E4F931E (void);
// 0x000003E4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGaze::.ctor()
extern void FollowEyeGaze__ctor_m5A9F005AA53DE29F950F3AA271249D50437928EF (void);
// 0x000003E5 UnityEngine.TextMesh Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::get_MyTextMesh()
extern void SpeechVisualFeedback_get_MyTextMesh_mFBE0088647B5DCF41E11609E886E1889D8BCBB72 (void);
// 0x000003E6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::UpdateTextMesh(System.String)
extern void SpeechVisualFeedback_UpdateTextMesh_m1EF7E792946EA53258473177A1B6B743CFA3A170 (void);
// 0x000003E7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::ShowVisualFeedback(System.String)
extern void SpeechVisualFeedback_ShowVisualFeedback_m93C60B8D5C9284C857D1107BEB0A3A1C8EE2489D (void);
// 0x000003E8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::Update()
extern void SpeechVisualFeedback_Update_mC8FAF719DFA65EBB8519922FF6EEF82A805D2B17 (void);
// 0x000003E9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechHandler.OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void SpeechVisualFeedback_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_mF3FA085FF5F59D69723246BE5E5329FD81F2AD65 (void);
// 0x000003EA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::.ctor()
extern void SpeechVisualFeedback__ctor_m6D25C50E7C642AD0CCBBE0AB78D8B353D335B6CF (void);
// 0x000003EB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Start()
extern void BlendOut_Start_m1744429E7BC8A534AB2677101DFD6F1E87AE6F0E (void);
// 0x000003EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::InitialSetup()
extern void BlendOut_InitialSetup_m91413838FA74302F088DB5FFCC5E30662D472F95 (void);
// 0x000003ED System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Engage()
extern void BlendOut_Engage_m964CDCA7BFE2A2EC5046C1E585DF994529F00F9D (void);
// 0x000003EE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Disengage()
extern void BlendOut_Disengage_mD6FE95FB9EDDF8B4430F9A532E490B2C07DD54FB (void);
// 0x000003EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::DwellSucceeded()
extern void BlendOut_DwellSucceeded_mF5E63829A3A80643D849EE85EC43DA68F3898AE4 (void);
// 0x000003F0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Update()
extern void BlendOut_Update_m15966AF5E569739ADE10B249F74778D20495A9F8 (void);
// 0x000003F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::SlowlyBlendOut()
extern void BlendOut_SlowlyBlendOut_m7601026AA0817505A52B0C68B11F81558B734580 (void);
// 0x000003F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::ChangeTransparency(System.Single)
extern void BlendOut_ChangeTransparency_m59F8B52CF21A2E2119D3BB6D88DF844C89BB1E3D (void);
// 0x000003F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::ChangeTransparency(System.Single,System.String)
extern void BlendOut_ChangeTransparency_m662078A4265014EF9C294AB8C08C87F9C877AAA7 (void);
// 0x000003F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Materials_BlendOut(UnityEngine.Material[],System.Single,System.String)
extern void BlendOut_Materials_BlendOut_m8278EC0CDA86DC03BA67B7F0E276351AC5011E61 (void);
// 0x000003F5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::.ctor()
extern void BlendOut__ctor_m5FD487435FDF3B1BDD97EC751B9CACCA5CF9FC62 (void);
// 0x000003F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Start()
extern void ChangeSize_Start_m668E97DD9A42AB46CDB6F0B40E90E1A012F83166 (void);
// 0x000003F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Engage()
extern void ChangeSize_Engage_m401A40CC0C22E6FFAD03E881562E93DB6928AE90 (void);
// 0x000003F8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Disengage()
extern void ChangeSize_Disengage_m573685FF359DA0F5ECB5E7FA0C01F24CD80A9C91 (void);
// 0x000003F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::InitialSetup()
extern void ChangeSize_InitialSetup_m897F44338833B714CC0F3138CF60B62368C4C924 (void);
// 0x000003FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Update()
extern void ChangeSize_Update_m7165D93EAFE0C52CBC65CBAA16DE8C2F76AE12B2 (void);
// 0x000003FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::OnLookAt_IncreaseTargetSize()
extern void ChangeSize_OnLookAt_IncreaseTargetSize_mE9DDD81B8E1892B7B57F1FC0A4A10E379D8FED2A (void);
// 0x000003FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::OnLookAway_ReturnToOriginalTargetSize()
extern void ChangeSize_OnLookAway_ReturnToOriginalTargetSize_mA0F97D2DA5563AAB8F78BF2EAC2CA7DE755B31A3 (void);
// 0x000003FD System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::AreWeThereYet(UnityEngine.Vector3)
extern void ChangeSize_AreWeThereYet_m34F26A45E2DCE4BFFD75225EA6AB54791ED44EC7 (void);
// 0x000003FE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::.ctor()
extern void ChangeSize__ctor_m8DBFDBA094FC9E6163CFD0184BD7E7CDB152B447 (void);
// 0x000003FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Start()
extern void FaceUser_Start_mE441F5BA68CE27B4D9E66755308E698184A8BE1B (void);
// 0x00000400 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::InitialSetup()
extern void FaceUser_InitialSetup_m142B0A453F916BDC1B198E95009A8B64836E859C (void);
// 0x00000401 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Update()
extern void FaceUser_Update_m38910082605F6A82C19B1FEF49959B6BA0314A4F (void);
// 0x00000402 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::TurnToUser(UnityEngine.Vector3,UnityEngine.Vector3)
extern void FaceUser_TurnToUser_mFFFEC3D92007051D1FEFC2778E59EFC3595305BB (void);
// 0x00000403 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::ReturnToOriginalRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern void FaceUser_ReturnToOriginalRotation_m634BF42CD7FDDA12B36F3E9D79A1C2E3CC8EED04 (void);
// 0x00000404 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Engage()
extern void FaceUser_Engage_mE4ADC460DC6D03C6178974959810334EEAF27B08 (void);
// 0x00000405 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Disengage()
extern void FaceUser_Disengage_mFEB625D405929962CBDA7EF092B4D5C6B5F0B05D (void);
// 0x00000406 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::.ctor()
extern void FaceUser__ctor_m0C4446030572CB7661CD06AC3234BDD379814BE0 (void);
// 0x00000407 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::Awake()
extern void KeepFacingCamera_Awake_m17B46C03C0D0108DC0A01B8D3F01526C2CE5C160 (void);
// 0x00000408 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::Start()
extern void KeepFacingCamera_Start_m9846E6CA36F19BD864FC02E86706EBBDD155C273 (void);
// 0x00000409 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::Update()
extern void KeepFacingCamera_Update_mCD119CE5F0F2DA4872713CA4B08DC564840E3064 (void);
// 0x0000040A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::.ctor()
extern void KeepFacingCamera__ctor_mDCE9A0BE451D9A6F2139A0AC65E3AC93AF52741A (void);
// 0x0000040B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::LoadScene()
extern void LoadAdditiveScene_LoadScene_m0CF29706D86EECB6D045F3677C822846F7BEF9D0 (void);
// 0x0000040C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::LoadScene(System.String)
extern void LoadAdditiveScene_LoadScene_m719AE7192FB113C8ACDE3897322C95F072FA4606 (void);
// 0x0000040D System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::LoadNewScene(System.String)
extern void LoadAdditiveScene_LoadNewScene_mA9B0F703148A35AE42F526F6B1D0D074BEF83C6B (void);
// 0x0000040E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::.ctor()
extern void LoadAdditiveScene__ctor_m56DA10D254B57FE7B93616BFBAE48793195E357B (void);
// 0x0000040F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::.cctor()
extern void LoadAdditiveScene__cctor_m25D6C56543EC29322963954D6E0C4199106653EC (void);
// 0x00000410 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::.ctor(System.Int32)
extern void U3CLoadNewSceneU3Ed__6__ctor_m7AB7244F6CA4375DA1E463300EDC1170CD8CFA56 (void);
// 0x00000411 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.IDisposable.Dispose()
extern void U3CLoadNewSceneU3Ed__6_System_IDisposable_Dispose_m5FF2C084528B0704B251DBEA0D6BECA6D8B71BEB (void);
// 0x00000412 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::MoveNext()
extern void U3CLoadNewSceneU3Ed__6_MoveNext_m9BE12E8D747D3DE27D3B75DFA491122A543901FF (void);
// 0x00000413 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadNewSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9A59C746603E6F919F7E65B33B003181E837304 (void);
// 0x00000414 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.Collections.IEnumerator.Reset()
extern void U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_Reset_mC7480A3BFA0D90A78B4D4A312001384BA163EA10 (void);
// 0x00000415 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_get_Current_mFBB82D699D897C1EB00FE080178AA94A5B6C11A6 (void);
// 0x00000416 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveWithCamera::Update()
extern void MoveWithCamera_Update_mD7074A5166DAE0942BBE3ADC3E815C531F4BC532 (void);
// 0x00000417 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveWithCamera::.ctor()
extern void MoveWithCamera__ctor_m70A8DDBDE3028CB82A743E15F4B5A345AACEB71D (void);
// 0x00000418 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::Start()
extern void OnLoadStartScene_Start_m77030DC5D61416EE56A0F23D5597AA0A6ECE3CA1 (void);
// 0x00000419 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::LoadOnDevice()
extern void OnLoadStartScene_LoadOnDevice_m499DCBB4C8EFE46A43AD30E9F2DBD641E7A58359 (void);
// 0x0000041A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::LoadNewScene()
extern void OnLoadStartScene_LoadNewScene_m86088B37DD8C9A64E39F10AD8B0A9011B61350F5 (void);
// 0x0000041B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::.ctor()
extern void OnLoadStartScene__ctor_mA99E13D54949FA2F2660AC7F63F8F959D0C2BA6F (void);
// 0x0000041C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::OnEyeFocusStay()
extern void OnLookAtRotateByEyeGaze_OnEyeFocusStay_mFE2399F4CE91BECACDBE337489FDB93E279B368E (void);
// 0x0000041D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::RotateHitTarget()
extern void OnLookAtRotateByEyeGaze_RotateHitTarget_m96454A9E3FEB0983A9F332CFF3830BFB33AC10BD (void);
// 0x0000041E System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::ClampAngleInDegree(System.Single,System.Single,System.Single)
extern void OnLookAtRotateByEyeGaze_ClampAngleInDegree_m4A771E7693FF0EE775FF9005B9BA1AC589443018 (void);
// 0x0000041F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::.ctor()
extern void OnLookAtRotateByEyeGaze__ctor_m9E94BE4D49F3240EC8DB09E6E41267871F06D87D (void);
// 0x00000420 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Start()
extern void DwellSelection_Start_m9A630CFF908970997B2A13FF42F51376CA7DBEC9 (void);
// 0x00000421 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::EtTarget_OnTargetSelected(System.Object,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs)
extern void DwellSelection_EtTarget_OnTargetSelected_mA0A700CA9D9CC6C333C8EFC74A393CCA3278635E (void);
// 0x00000422 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::EnableDwell()
extern void DwellSelection_EnableDwell_mAD42E96313AAF18EB38E69B870DBF98FA5D4DF53 (void);
// 0x00000423 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::DisableDwell()
extern void DwellSelection_DisableDwell_m355C17B84690347BEB02B47402A274759D7EF745 (void);
// 0x00000424 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::get_UseDwell()
extern void DwellSelection_get_UseDwell_m1D058F5ABEC4C17FB86F938253D130236C93DA9D (void);
// 0x00000425 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::OnEyeFocusStart()
extern void DwellSelection_OnEyeFocusStart_m9303B20BF6E1051A7AD6D817CF00EB142518E680 (void);
// 0x00000426 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::OnEyeFocusStay()
extern void DwellSelection_OnEyeFocusStay_mB88259BECF276CF96A2FA77180F9A7378407D9D0 (void);
// 0x00000427 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::OnEyeFocusStop()
extern void DwellSelection_OnEyeFocusStop_mC94C67B81A99DC79B5E6B151F81FBC1341F4D298 (void);
// 0x00000428 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::StartDwellFeedback()
extern void DwellSelection_StartDwellFeedback_mDDA6055A6F241C77221A41068380231AB97E5349 (void);
// 0x00000429 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::ResetDwellFeedback()
extern void DwellSelection_ResetDwellFeedback_mCB095C812A9C40B7A8F0201F485EE6444CD7F748 (void);
// 0x0000042A UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::get_SpeedSizeChangePerSecond()
extern void DwellSelection_get_SpeedSizeChangePerSecond_m4077FEDBAE4647E4118D2034216CE0085640D111 (void);
// 0x0000042B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Update()
extern void DwellSelection_Update_mF4AA63723902ED768C74EAD9C9740ABED44B691E (void);
// 0x0000042C UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::ClampVector3(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void DwellSelection_ClampVector3_mA73A583F01C46F806B90CF6507AA039DF5F026F8 (void);
// 0x0000042D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::UpdateTransparency(System.Single)
extern void DwellSelection_UpdateTransparency_mDD03FEED8CB2EAC221E69EF3C6325A09704BE3B2 (void);
// 0x0000042E System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::LerpTransparency(System.Single,System.Single,System.Single)
extern void DwellSelection_LerpTransparency_m13149FA74B8B556445CA3CA77331C29471EDCCA8 (void);
// 0x0000042F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m44FF5711B3307F11C17FA7BB8F09760042BBD277 (void);
// 0x00000430 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m9F94E95B5A7FFA97A38307A9EFC7FDFA1DF8F5F8 (void);
// 0x00000431 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m1B178B56EFB131E5E6175804361DF52ECFB900C8 (void);
// 0x00000432 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m8DBEBE2A807C9B14344517C73658B05468AF8D23 (void);
// 0x00000433 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::get_TextureShaderProperty()
extern void DwellSelection_get_TextureShaderProperty_mC1C11397DAFF17C1A82AB093EEB017BD2852E2F0 (void);
// 0x00000434 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::set_TextureShaderProperty(System.String)
extern void DwellSelection_set_TextureShaderProperty_mBE7E90ECF8808AC33DF45E77307611284AFEFF7F (void);
// 0x00000435 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::.ctor()
extern void DwellSelection__ctor_mCFD10568F30920D008800DB87685647A166E5D8A (void);
// 0x00000436 Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs::get_HitTarget()
extern void TargetEventArgs_get_HitTarget_m79731B1B64F57006419156123258483169AA9DD1 (void);
// 0x00000437 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs::set_HitTarget(Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void TargetEventArgs_set_HitTarget_mBDCCC4B4B8E6F554650DB774DB9EFE2B9F1B084F (void);
// 0x00000438 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs::.ctor(Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void TargetEventArgs__ctor_m9C9EEFD59B447E36A991D757AF2919D4D2B82594 (void);
// 0x00000439 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeRenderMode::ChangeRenderModes(UnityEngine.Material,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeRenderMode/BlendMode)
extern void ChangeRenderMode_ChangeRenderModes_mC125A77B940A37697754553384FD8D7C12D1E150 (void);
// 0x0000043A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DoNotRender::Start()
extern void DoNotRender_Start_m3CB963F8058C4ABB4F263BB3156C13D9BC372EC5 (void);
// 0x0000043B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DoNotRender::.ctor()
extern void DoNotRender__ctor_m92A4C218A60CFD9E8D40636C57F6533DC432C73D (void);
// 0x0000043C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeCalibrationChecker::Update()
extern void EyeCalibrationChecker_Update_mC9BC31FFE361F9B2C6FD6C873BFFE430E2FD6B82 (void);
// 0x0000043D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeCalibrationChecker::.ctor()
extern void EyeCalibrationChecker__ctor_m70521FDDF27910A6B62DBA67CEDCD86B03A9F319 (void);
// 0x0000043E Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::get_Instance()
extern void KeepThisAlive_get_Instance_m12A9C26E66BCAC799293E0EF8CC51E6834D8C7F5 (void);
// 0x0000043F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::set_Instance(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive)
extern void KeepThisAlive_set_Instance_m2F579A8EFCC355FA206AD3919F875E46A8B3827F (void);
// 0x00000440 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::Awake()
extern void KeepThisAlive_Awake_m94F659CDD8BECCDE0581B04A6ACDB26B978577E2 (void);
// 0x00000441 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::Start()
extern void KeepThisAlive_Start_m76E57B1A8F849DC271763AB76B6893F1C5E56F0A (void);
// 0x00000442 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::.ctor()
extern void KeepThisAlive__ctor_m79E70C4D62F79AA2E1D68BFCD2C836A67C5C459C (void);
// 0x00000443 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::get_Instance()
extern void StatusText_get_Instance_mCBB3E8CCF8874AB75906D5078D47ACAF6930D1F9 (void);
// 0x00000444 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::Awake()
extern void StatusText_Awake_mAE706EAC0908CAD3B0F10262CC28E27459E41445 (void);
// 0x00000445 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::Start()
extern void StatusText_Start_mAA9B137FA243051B1164C368602A02FC405E97A3 (void);
// 0x00000446 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::Log(System.String,System.Boolean)
extern void StatusText_Log_mF3B438327AEE53C0BA8003D1D56662105A580A0F (void);
// 0x00000447 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::.ctor()
extern void StatusText__ctor_m8AFF8179D5E3B76A3938A58ACAD1ECF09442AD7D (void);
// 0x00000448 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GetValidFilename(System.String)
extern void EyeTrackingDemoUtils_GetValidFilename_m7D3381E8288741E7C1D6229E74BAEEC1AF4BF696 (void);
// 0x00000449 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GetFullName(UnityEngine.GameObject)
extern void EyeTrackingDemoUtils_GetFullName_m0726B6420CB4AAB80CEF716965669572A5D1ED8C (void);
// 0x0000044A System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GetFullName(UnityEngine.GameObject,System.Boolean&)
extern void EyeTrackingDemoUtils_GetFullName_m760F975A56FAF7E56716A9C7330B51F368D76ADF (void);
// 0x0000044B System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::Normalize(System.Single,System.Single,System.Single)
extern void EyeTrackingDemoUtils_Normalize_m941014ACB293B86F3460D81F06085E7C7DBE55E9 (void);
// 0x0000044C T[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::RandomizeListOrder(T[])
// 0x0000044D UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::VisAngleInDegreesToMeters(UnityEngine.Vector3,System.Single)
extern void EyeTrackingDemoUtils_VisAngleInDegreesToMeters_m383345A2CC28FDDA40964B190C43E9BE035A2597 (void);
// 0x0000044E System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::VisAngleInDegreesToMeters(System.Single,System.Single)
extern void EyeTrackingDemoUtils_VisAngleInDegreesToMeters_m51014BC01BA4B552D137F455292C3E4F13418275 (void);
// 0x0000044F System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::LoadNewScene(System.String)
extern void EyeTrackingDemoUtils_LoadNewScene_mA7052E4FDC288015737F723924477A29E9FD5CD2 (void);
// 0x00000450 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::LoadNewScene(System.String,System.Single)
extern void EyeTrackingDemoUtils_LoadNewScene_m0AE63AECE54363C8902C0CCCF90D5170FC2EFEDC (void);
// 0x00000451 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GameObject_ChangeColor(UnityEngine.GameObject,UnityEngine.Color,System.Nullable`1<UnityEngine.Color>&,System.Boolean)
extern void EyeTrackingDemoUtils_GameObject_ChangeColor_m54D18DCFAC811DFDEC0FCDD817A9142ECA014862 (void);
// 0x00000452 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GameObject_ChangeTransparency(UnityEngine.GameObject,System.Single)
extern void EyeTrackingDemoUtils_GameObject_ChangeTransparency_mB51F7B9210D71C29F6DC7D5C938BA5D4BD37C8E6 (void);
// 0x00000453 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GameObject_ChangeTransparency(UnityEngine.GameObject,System.Single,System.Single&)
extern void EyeTrackingDemoUtils_GameObject_ChangeTransparency_m31E98A089CD6185CEA9F2AAF97B5C7EA93A550B2 (void);
// 0x00000454 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::Renderers_ChangeTransparency(UnityEngine.Renderer[],System.Single,System.Single&)
extern void EyeTrackingDemoUtils_Renderers_ChangeTransparency_m23EA9627CD35F3DF3F29910A249762628EE67B57 (void);
// 0x00000455 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::.ctor(System.Int32)
extern void U3CLoadNewSceneU3Ed__8__ctor_mB392F101E0418B2AD32E95DCBC57083DDD7D9EB7 (void);
// 0x00000456 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.IDisposable.Dispose()
extern void U3CLoadNewSceneU3Ed__8_System_IDisposable_Dispose_m1CE9B5CA891ECC02A581C59D07BB981D6EF7928B (void);
// 0x00000457 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::MoveNext()
extern void U3CLoadNewSceneU3Ed__8_MoveNext_m22E33674E9BCC058A57BA951579425BD1B2C1E4F (void);
// 0x00000458 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadNewSceneU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF5C96A2407CC0BC55FEAB004DEE4E564744403C7 (void);
// 0x00000459 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.Collections.IEnumerator.Reset()
extern void U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_Reset_m8D4D220DF4E3AB63A857D813DACB0A852C5828E5 (void);
// 0x0000045A System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_get_Current_mAA4DB3CC0E90E566E1B391E592CDA3CDF1A681ED (void);
// 0x0000045B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::Start()
extern void OnLookAtShowHoverFeedback_Start_m64DE29022B8BAD03B7D0578ACD15E2D2C4245DD3 (void);
// 0x0000045C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::Update()
extern void OnLookAtShowHoverFeedback_Update_m82A433551AFB41BB00E65F0C3DEB1CF3C46682F0 (void);
// 0x0000045D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::OnDestroy()
extern void OnLookAtShowHoverFeedback_OnDestroy_m082E52D60012F44E2B94BEDCA6283652D6859EDD (void);
// 0x0000045E System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::NormalizedInterest_Dwell()
extern void OnLookAtShowHoverFeedback_NormalizedInterest_Dwell_m7242681E062C6E0B0FF848D5A8A76F5C84FAE6CA (void);
// 0x0000045F System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::NormalizedDisinterest_LookAway()
extern void OnLookAtShowHoverFeedback_NormalizedDisinterest_LookAway_m906C941DB572DEDC6522176B94994875DECC1299 (void);
// 0x00000460 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::DestroyLocalFeedback()
extern void OnLookAtShowHoverFeedback_DestroyLocalFeedback_m62F895D85F845F02040ADEADED339140E940E4C1 (void);
// 0x00000461 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::ShowFeedback(System.Single)
extern void OnLookAtShowHoverFeedback_ShowFeedback_mB6738E6F8EF0657E36B317A684CF5173A9CC174C (void);
// 0x00000462 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::ShowFeedback_Overlay(System.Single)
extern void OnLookAtShowHoverFeedback_ShowFeedback_Overlay_mAFA93AE6832FE97F78A75AA093D403874CE0962D (void);
// 0x00000463 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::ShowFeedback_Highlight(System.Single)
extern void OnLookAtShowHoverFeedback_ShowFeedback_Highlight_m828016C6892FACB4F46EEC132909C9EDC39C10D7 (void);
// 0x00000464 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::TransitionAdjustedInterest(System.Single)
extern void OnLookAtShowHoverFeedback_TransitionAdjustedInterest_mBB31EB549232F23DE71E807F2FFF4DB13C9635EB (void);
// 0x00000465 System.Double Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::get_LookAwayTimeInMs()
extern void OnLookAtShowHoverFeedback_get_LookAwayTimeInMs_m3EFFD60140F144CD1B6C84BABBA079E9B06BE61A (void);
// 0x00000466 System.Double Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::get_DwellTimeInMs()
extern void OnLookAtShowHoverFeedback_get_DwellTimeInMs_m903B205BE77CFDB77DD2AF8D402DBABA46316811 (void);
// 0x00000467 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::OnLookAtStop()
extern void OnLookAtShowHoverFeedback_OnLookAtStop_m0CB31B582D1F7DB41AAC9DBA16323936786142B4 (void);
// 0x00000468 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::OnLookAtStart()
extern void OnLookAtShowHoverFeedback_OnLookAtStart_m625A7BBD02720DA79DADE81E27E58681887BF697 (void);
// 0x00000469 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::SaveOriginalColor()
extern void OnLookAtShowHoverFeedback_SaveOriginalColor_m6FE289F5EAAD733BDF667256BCE6B216B81ABDE2 (void);
// 0x0000046A UnityEngine.Color[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::GetColorsByProperty(System.String,UnityEngine.Renderer[])
extern void OnLookAtShowHoverFeedback_GetColorsByProperty_m838F980316977B834901C11FE2B0943DA54B7A9D (void);
// 0x0000046B UnityEngine.Color Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::BlendColors(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void OnLookAtShowHoverFeedback_BlendColors_m29808D7157CDD76EACE4E0E17D6F04761FC658E6 (void);
// 0x0000046C System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::divBlendColor(System.Single,System.Single,System.Single)
extern void OnLookAtShowHoverFeedback_divBlendColor_m6E4403B34857DB217F58E216A9DF613ABE186511 (void);
// 0x0000046D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::.ctor()
extern void OnLookAtShowHoverFeedback__ctor_m0F54940A8D11265C5DCE2D51303BF1170D236933 (void);
// 0x0000046E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers::RunSync(System.Func`1<System.Threading.Tasks.Task>)
extern void AsyncHelpers_RunSync_mD89BCA60E72B417FF35C83C8E9EC0E6C0E5588E7 (void);
// 0x0000046F T Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers::RunSync(System.Func`1<System.Threading.Tasks.Task`1<T>>)
// 0x00000470 System.Exception Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::get_InnerException()
extern void ExclusiveSynchronizationContext_get_InnerException_mCFC704A020866CE27AD451ACB0AA5729448E06FE (void);
// 0x00000471 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::set_InnerException(System.Exception)
extern void ExclusiveSynchronizationContext_set_InnerException_m4AB464A291F79D5848A5A9EB296FD2B7DF450B0C (void);
// 0x00000472 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void ExclusiveSynchronizationContext_Send_mD78553FD25A6B3DD25C24AD662376D792ED2C0F4 (void);
// 0x00000473 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void ExclusiveSynchronizationContext_Post_m4ECFF1199D4B2603EAC872EDABD9ED956873AC9F (void);
// 0x00000474 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::EndMessageLoop()
extern void ExclusiveSynchronizationContext_EndMessageLoop_m6C548C3CC4F111C370B79536860614767CEDFE8E (void);
// 0x00000475 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::BeginMessageLoop()
extern void ExclusiveSynchronizationContext_BeginMessageLoop_m0E9BC45A55EBEB589021D4305B87E78B25EAB6C4 (void);
// 0x00000476 System.Threading.SynchronizationContext Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::CreateCopy()
extern void ExclusiveSynchronizationContext_CreateCopy_m6A0875CD991BC1DF040E632AA5DCC8336A54BA48 (void);
// 0x00000477 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::.ctor()
extern void ExclusiveSynchronizationContext__ctor_m8C089CA17D4B9E33E726626ABCA65AF3C7C73F70 (void);
// 0x00000478 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::<EndMessageLoop>b__9_0(System.Object)
extern void ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m5B8F1B4820BF0D41FFC62F7A98B8595B67F8F2BA (void);
// 0x00000479 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m02B40EFB70D4BD0D542411C7ADF77EC6C48229B2 (void);
// 0x0000047A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0::<RunSync>b__0(System.Object)
extern void U3CU3Ec__DisplayClass0_0_U3CRunSyncU3Eb__0_mD7F01011FD2978D9E88CA54DC0110ACBC5E569C5 (void);
// 0x0000047B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0/<<RunSync>b__0>d::MoveNext()
extern void U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m62460AC0CF68DFACF7EC1172E850097F9DEE5645 (void);
// 0x0000047C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0/<<RunSync>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m2DAE57EA7D64EB02ACB7EDF19035F855583D4439 (void);
// 0x0000047D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1::.ctor()
// 0x0000047E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1::<RunSync>b__0(System.Object)
// 0x0000047F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1/<<RunSync>b__0>d::MoveNext()
// 0x00000480 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1/<<RunSync>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x00000481 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::SetUserName(System.String)
extern void BasicInputLogger_SetUserName_mD16A00E62E434C7338E979D3702B1D068FB15DAD (void);
// 0x00000482 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::SetSessionDescr(System.String)
extern void BasicInputLogger_SetSessionDescr_m6E4D34BC34D7C92DE4CF89CEFF42E53D187352E9 (void);
// 0x00000483 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_LogDirectory()
extern void BasicInputLogger_get_LogDirectory_mDA6F95CA28011BCD49F1E56DBC7E94DFC1452EF6 (void);
// 0x00000484 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::GetHeader()
// 0x00000485 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::GetFileName()
// 0x00000486 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::CreateNewLogFile()
extern void BasicInputLogger_CreateNewLogFile_mD6C79CFC4D09BD9D6093FA1B7761328F852500ED (void);
// 0x00000487 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::CheckIfInitialized()
extern void BasicInputLogger_CheckIfInitialized_mD37DDCA2A7480C7744A0088781DE0BC6A167565C (void);
// 0x00000488 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::ResetLog()
extern void BasicInputLogger_ResetLog_m55555949F93B9911F414D3EF0C5C3510A896E6FD (void);
// 0x00000489 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_FormattedTimeStamp()
extern void BasicInputLogger_get_FormattedTimeStamp_mE980AA36630D3585592A3044695EE7C74F339034 (void);
// 0x0000048A System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::AddLeadingZeroToSingleDigitIntegers(System.Int32)
extern void BasicInputLogger_AddLeadingZeroToSingleDigitIntegers_m448C915ECE46D804079925AB74B7B70F9CBC20AA (void);
// 0x0000048B System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::Append(System.String)
extern void BasicInputLogger_Append_mCCE2850E50A08D290CD10CEC31BDD95633D8556B (void);
// 0x0000048C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::LoadLogs()
extern void BasicInputLogger_LoadLogs_mDB20EE52F95C11817164AE3D01C20F570BE54D14 (void);
// 0x0000048D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::SaveLogs()
extern void BasicInputLogger_SaveLogs_mBA991B292BFAEC0BE338B222AADDBE4FDA3C59A6 (void);
// 0x0000048E System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_Filename()
extern void BasicInputLogger_get_Filename_mA90D7E48E689C63C9FE5053B7E4DD36411FD5EAD (void);
// 0x0000048F System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_FilenameWithTimestamp()
extern void BasicInputLogger_get_FilenameWithTimestamp_m2FA61565A7E50DF43077B5A883EAAA7E2D4CA5F9 (void);
// 0x00000490 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_FilenameNoTimestamp()
extern void BasicInputLogger_get_FilenameNoTimestamp_mFE4AD530DAB269FDFACDB243C6E504AE226C7837 (void);
// 0x00000491 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::OnDestroy()
extern void BasicInputLogger_OnDestroy_m265A66E30EE4370B42657361ADCEF5203B50F336 (void);
// 0x00000492 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::.ctor()
extern void BasicInputLogger__ctor_mF6D8393446C41AB9337D3E25DF6481B4ADB1DC66 (void);
// 0x00000493 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<CreateNewLogFile>d__14::MoveNext()
extern void U3CCreateNewLogFileU3Ed__14_MoveNext_mBE68DC2547DAEAB02399255517DD3DABB152AFCF (void);
// 0x00000494 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<CreateNewLogFile>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateNewLogFileU3Ed__14_SetStateMachine_mBB528CB8977F8797016496EBEFF3139CC717A28C (void);
// 0x00000495 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<LoadLogs>d__21::MoveNext()
extern void U3CLoadLogsU3Ed__21_MoveNext_m465394392A1CC40B1F76EF054E462327CB481AB9 (void);
// 0x00000496 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<LoadLogs>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadLogsU3Ed__21_SetStateMachine_m1FEF9DE9FB018C66BF156F8411185C92ACC6CF57 (void);
// 0x00000497 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<SaveLogs>d__22::MoveNext()
extern void U3CSaveLogsU3Ed__22_MoveNext_m3FB430EA63A9310C00C2B2A80A0200677EB671B5 (void);
// 0x00000498 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<SaveLogs>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSaveLogsU3Ed__22_SetStateMachine_m50C0515F078C93C7AA07C7F4B639D1816AE84C3F (void);
// 0x00000499 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::CustomAppend(System.String)
extern void CustomInputLogger_CustomAppend_mC3082A5143200FA5777BFE3596F67985350FE0BD (void);
// 0x0000049A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::CreateNewLog()
extern void CustomInputLogger_CreateNewLog_m30A5E5B3FD7249E6848DC27F3464F26C13BB311B (void);
// 0x0000049B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::StartLogging()
extern void CustomInputLogger_StartLogging_mED2A25A2CE4A16379E7FC9837EA0EF8FACFA219E (void);
// 0x0000049C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::StopLoggingAndSave()
extern void CustomInputLogger_StopLoggingAndSave_m28EBF1DDEAA1A281261E3001A3C0EB9E22BCD467 (void);
// 0x0000049D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::CancelLogging()
extern void CustomInputLogger_CancelLogging_m6D2EB89F7D294D86A783BDB0F30F8913C541E8A7 (void);
// 0x0000049E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::.ctor()
extern void CustomInputLogger__ctor_m68341B2FEAAC17DC4EE414FA8D6165F19AC4C497 (void);
// 0x0000049F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::Start()
extern void InputPointerVisualizer_Start_mE03BAAAF070032DDFD1707E179C164FF58FFC273 (void);
// 0x000004A0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetVisualizations()
extern void InputPointerVisualizer_ResetVisualizations_m0F5A625385650DB837D0B0888781C998C0358F80 (void);
// 0x000004A1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::InitPointClouds(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap,System.Int32)
extern void InputPointerVisualizer_InitPointClouds_mBB6DCD21230132A22D1930086C8EBDCFBD9D919F (void);
// 0x000004A2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::InitVisArrayObj(UnityEngine.GameObject[]&,UnityEngine.GameObject,System.Int32)
extern void InputPointerVisualizer_InitVisArrayObj_m7E25C4F30B12FAF52919C7A8B3D8C33522749DC1 (void);
// 0x000004A3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetVis()
extern void InputPointerVisualizer_ResetVis_mDBC1E18CAFE00E7C383109AD13C9C230A1521C42 (void);
// 0x000004A4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetVis(UnityEngine.GameObject[]&)
extern void InputPointerVisualizer_ResetVis_m7AD448B6FCC5C78DB664AD1C4BCB52C6C0B7E52C (void);
// 0x000004A5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetPointCloudVis(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&)
extern void InputPointerVisualizer_ResetPointCloudVis_mC2998A43D80C42D2838C6CBDA55980626ACD46D9 (void);
// 0x000004A6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_DataVis(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer/VisModes)
extern void InputPointerVisualizer_SetActive_DataVis_m320C46DED5F2C429A2C72AD8801E36E8B8AB57DD (void);
// 0x000004A7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_DataVis(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void InputPointerVisualizer_SetActive_DataVis_m92C617218D9974454C1D65EE3E0EB379580F761F (void);
// 0x000004A8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_DataVis(UnityEngine.GameObject[]&,System.Boolean)
extern void InputPointerVisualizer_SetActive_DataVis_mCDC1FB154A8565FD30E4592B940AD93504E7423C (void);
// 0x000004A9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_PointCloudVis(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&,System.Boolean)
extern void InputPointerVisualizer_SetActive_PointCloudVis_m0BAFB659E2579B3D22EA4046541EEAD127217EBD (void);
// 0x000004AA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateDataVisPos(UnityEngine.GameObject[]&,System.Int32,UnityEngine.Vector3,System.Boolean)
extern void InputPointerVisualizer_UpdateDataVisPos_mF7364829B2CA249BB88DB54312D91A77FDC027BA (void);
// 0x000004AB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateVis_PointCloud(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&,System.Int32,UnityEngine.Vector3,System.Boolean)
extern void InputPointerVisualizer_UpdateVis_PointCloud_m813FB2BCDBAF3F04C8E9DEDA73BC464712E91172 (void);
// 0x000004AC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateConnectorLines(UnityEngine.GameObject[]&,System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean)
extern void InputPointerVisualizer_UpdateConnectorLines_m550407448267FE7D90D2AB8D52CBC69B751AA4CC (void);
// 0x000004AD System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::GetPrevLineIndex(System.Int32,System.Int32)
extern void InputPointerVisualizer_GetPrevLineIndex_m16699A946300D62ECF52753B73039A0B9E06F187 (void);
// 0x000004AE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateDataVis(UnityEngine.Ray)
extern void InputPointerVisualizer_UpdateDataVis_mC57E3CF77932051D9E3958339917F53FC2AEB7FC (void);
// 0x000004AF System.Nullable`1<UnityEngine.Vector3> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::PerformHitTest(UnityEngine.Ray)
extern void InputPointerVisualizer_PerformHitTest_m5338763DE47B3D9AB2DE8848F58622BE0E05EDE0 (void);
// 0x000004B0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::Update()
extern void InputPointerVisualizer_Update_mF2F2993E82780A901562DC5ABD876294F4A1436B (void);
// 0x000004B1 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::IsDwelling()
extern void InputPointerVisualizer_IsDwelling_m4FC2A6406E403CA7F71F5527C66E03E7EDA97E06 (void);
// 0x000004B2 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::get_AmountOfSamples()
extern void InputPointerVisualizer_get_AmountOfSamples_m45ED071FED4465F869AB67DAB1590C1AB8C3287B (void);
// 0x000004B3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::set_AmountOfSamples(System.Int32)
extern void InputPointerVisualizer_set_AmountOfSamples_m5994068C554E26F9432C9BAFB268AAE39B8A2905 (void);
// 0x000004B4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ToggleAppState()
extern void InputPointerVisualizer_ToggleAppState_m0FA7B318E61EFA84A5E75C566D7BA2CA4EEBB63C (void);
// 0x000004B5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::PauseApp()
extern void InputPointerVisualizer_PauseApp_mA3827A91ED15A89FF4AB2696EB329BCB04572A2A (void);
// 0x000004B6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UnpauseApp()
extern void InputPointerVisualizer_UnpauseApp_mDE63EC556A99F52378A35A7708F639D9F9CBB007 (void);
// 0x000004B7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetAppState(System.Boolean)
extern void InputPointerVisualizer_SetAppState_m73E5B39D8BA915D6D52E858B0FAF1818BB324585 (void);
// 0x000004B8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::.ctor()
extern void InputPointerVisualizer__ctor_m936E1933F44E64FBC07DEADFEA4D81602E06F5B7 (void);
// 0x000004B9 System.String[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructure::GetHeaderColumns()
extern void LogStructure_GetHeaderColumns_m303412053AC45A43FB7688CAA6F2D61C0D472453 (void);
// 0x000004BA System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructure::GetData(System.String,System.String,Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void LogStructure_GetData_m01E628A5AABEE7FD5409C76975AB6E25F858BDB5 (void);
// 0x000004BB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructure::.ctor()
extern void LogStructure__ctor_mBF7F2525B379518460C787DCEA666AE9D30684EB (void);
// 0x000004BC Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::get_EyeTrackingProvider()
extern void LogStructureEyeGaze_get_EyeTrackingProvider_m61F2FD20116F0AA4585441077AB99A4C452C148F (void);
// 0x000004BD System.String[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::GetHeaderColumns()
extern void LogStructureEyeGaze_GetHeaderColumns_m58D256A8BCFEF7AAAAF96548822F34CDF5FBEEDB (void);
// 0x000004BE System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::GetData(System.String,System.String,Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void LogStructureEyeGaze_GetData_m250ED06546EED5314EBC38F6DB0AE7C4FAD289AC (void);
// 0x000004BF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::.ctor()
extern void LogStructureEyeGaze__ctor_mCEE67B1F064E42F371B7F68F48E9332935AB8A8F (void);
// 0x000004C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Start()
extern void UserInputPlayback_Start_mC667090401F44C52662F03B3C5D9FB0AB5F2297D (void);
// 0x000004C1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ResetCurrentStream()
extern void UserInputPlayback_ResetCurrentStream_m1348C34AECDD19020DD384D09EAFA4B2B5275175 (void);
// 0x000004C2 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_Load()
extern void UserInputPlayback_UWP_Load_m90DD8792E60ACDBC77A34E7D4A0ACA52C28FDFBC (void);
// 0x000004C3 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_LoadNewFile(System.String)
extern void UserInputPlayback_UWP_LoadNewFile_m950B4D6A913676FDC2EC1C916B0576C7B5DE80A5 (void);
// 0x000004C4 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_FileExists(System.String,System.String)
extern void UserInputPlayback_UWP_FileExists_mA6D81019DBEB76446532FDCE758A615EF612E561 (void);
// 0x000004C5 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_ReadData(Windows.Storage.StorageFile)
extern void UserInputPlayback_UWP_ReadData_mF915D6532E33AC4033624029D2BB53C28EB84CD6 (void);
// 0x000004C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadNewFile(System.String)
extern void UserInputPlayback_LoadNewFile_m9B5593EAA5CE8861955B49F0A3D748D0348638E9 (void);
// 0x000004C7 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::TryParseStringToVector3(System.String,System.String,System.String,System.Boolean&)
extern void UserInputPlayback_TryParseStringToVector3_m2405B9E525C2D5E1CF5B7859D4B8C15A7825B0C4 (void);
// 0x000004C8 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ParseStringToFloat(System.String)
extern void UserInputPlayback_ParseStringToFloat_mCB2D59245B32555F11D09E11A1F255ED9EE8FA46 (void);
// 0x000004C9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Load()
extern void UserInputPlayback_Load_mAECA47AF116374D7726712F08F4E2CFCE6CA1E33 (void);
// 0x000004CA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadInEditor()
extern void UserInputPlayback_LoadInEditor_m8AF8F85982EA8941CD914C182A50DC96C4E678FE (void);
// 0x000004CB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadInUWP()
extern void UserInputPlayback_LoadInUWP_mFA032E94B5A1FA935658B64D1ECDA5FEAF6C25B8 (void);
// 0x000004CC System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::get_FileName()
extern void UserInputPlayback_get_FileName_mBB9BF1CD012F107962E877CB7331AB87ACA72AD0 (void);
// 0x000004CD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::set_IsPlaying(System.Boolean)
extern void UserInputPlayback_set_IsPlaying_m023BD2B8382793CA011B97B6BD0D8B9021EB7C03 (void);
// 0x000004CE System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::get_IsPlaying()
extern void UserInputPlayback_get_IsPlaying_m24956047C048537124AA17206FFA4011B89949F9 (void);
// 0x000004CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Play()
extern void UserInputPlayback_Play_m3F7CCE9AB584905B7631C06944AA745C1CE1FDC6 (void);
// 0x000004D0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Pause()
extern void UserInputPlayback_Pause_m556F1156829B50B626CAD54653434781A63815F7 (void);
// 0x000004D1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Clear()
extern void UserInputPlayback_Clear_m919F93574AC5F4190E1CECDA7D33CBD96EB919BA (void);
// 0x000004D2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::SpeedUp()
extern void UserInputPlayback_SpeedUp_mBAE45189BA7CEAC5FFA289060A643CF39BAFDBC1 (void);
// 0x000004D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::SlowDown()
extern void UserInputPlayback_SlowDown_m4A626E35D8A4921E6677DE6F993A9196FEC391A2 (void);
// 0x000004D4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ShowAllAndFreeze()
extern void UserInputPlayback_ShowAllAndFreeze_m8B42EE1823AB6B8FCC4F74C31CEAF0C6639646ED (void);
// 0x000004D5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ShowAllAndFreeze(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void UserInputPlayback_ShowAllAndFreeze_m19B7616E1F24B8E4C4A6FAFEB78FA891ED0894D6 (void);
// 0x000004D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ShowHeatmap()
extern void UserInputPlayback_ShowHeatmap_m7A816D88BA6A89AB5415A7595B795F3079E9C020 (void);
// 0x000004D7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadingStatus_Hide()
extern void UserInputPlayback_LoadingStatus_Hide_m3B145FFA3112A7428F7C0F839A75F83E528E4D5C (void);
// 0x000004D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadingStatus_Show()
extern void UserInputPlayback_LoadingStatus_Show_m02ED23C7BFD68559AFA9D1F99F8D8A709C7427FB (void);
// 0x000004D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateLoadingStatus(System.Int32,System.Int32)
extern void UserInputPlayback_UpdateLoadingStatus_m2C336BA7F96CB4D9379AF9CB6E2C5EC526988DDC (void);
// 0x000004DA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Log(System.String)
extern void UserInputPlayback_Log_m3876C3A165ECE5E80C1590D6B408794C65BF7E7B (void);
// 0x000004DB System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::PopulateHeatmap()
extern void UserInputPlayback_PopulateHeatmap_m7297D7FB753B5C44B8AD7B3FBC9407D11878181E (void);
// 0x000004DC System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateStatus(System.Single)
extern void UserInputPlayback_UpdateStatus_m707E890ECC0327E36F59E30D065212C6BF509FBD (void);
// 0x000004DD System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::AddToCounter(System.Single)
extern void UserInputPlayback_AddToCounter_m60C9688D18B4BCD6DBF4E639F56C408FC70F408C (void);
// 0x000004DE System.Nullable`1<UnityEngine.Ray> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::GetEyeRay(System.String[])
extern void UserInputPlayback_GetEyeRay_m9A80D2B933D77A9BC254F21BF19D9823892B2A34 (void);
// 0x000004DF System.Nullable`1<UnityEngine.Ray> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::GetRay(System.String,System.String,System.String,System.String,System.String,System.String)
extern void UserInputPlayback_GetRay_mAC062B30B2E9A6BDC80B3665743F8EEEEE955C6E (void);
// 0x000004E0 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::get_DataIsLoaded()
extern void UserInputPlayback_get_DataIsLoaded_m12D79CCECBFCD7ADA760A8865CE3685F05B6C8BD (void);
// 0x000004E1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateTimestampForNextReplay(System.String[])
extern void UserInputPlayback_UpdateTimestampForNextReplay_m4705CE9D6611EDE732A85933107B757F1AF9682E (void);
// 0x000004E2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateEyeGazeSignal(System.String[],Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer)
extern void UserInputPlayback_UpdateEyeGazeSignal_m79DEFD0803D85B1210326D2D3EFCA82234F776C7 (void);
// 0x000004E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateHeadGazeSignal(System.String[],Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer)
extern void UserInputPlayback_UpdateHeadGazeSignal_m374514A7134E8B3056B4ED655364FDC625B953E1 (void);
// 0x000004E4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateTargetingSignal(System.String,System.String,System.String,System.String,System.String,System.String,UnityEngine.Ray,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer)
extern void UserInputPlayback_UpdateTargetingSignal_mCFE75108091E3B39263B86E3D9ED4C2CBC791688 (void);
// 0x000004E5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Update()
extern void UserInputPlayback_Update_m616F13984403A96DBB8D4A18BB4BECE965DDA387 (void);
// 0x000004E6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::PlayNext()
extern void UserInputPlayback_PlayNext_mEADD913728F4628DF229B10FD331EE3F576C804C (void);
// 0x000004E7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::.ctor()
extern void UserInputPlayback__ctor_m8EADDAE7D039D4BA559DFA8658739EED777AAC18 (void);
// 0x000004E8 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::<ShowAllAndFreeze>b__34_0()
extern void UserInputPlayback_U3CShowAllAndFreezeU3Eb__34_0_mADBC70CE9E9E8DB02676A4F6C446A8E614D8ACC3 (void);
// 0x000004E9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_Load>d__12::MoveNext()
extern void U3CUWP_LoadU3Ed__12_MoveNext_m9D85D0A3795968BEC8AA55D56467300F3DD1F7C9 (void);
// 0x000004EA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_Load>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_LoadU3Ed__12_SetStateMachine_m2C4B3C455A8E03E867BE61D5B1BA0E4A63FC7DB8 (void);
// 0x000004EB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_LoadNewFile>d__13::MoveNext()
extern void U3CUWP_LoadNewFileU3Ed__13_MoveNext_m3113DF5B967063CD4CA6542ED9AAF57F9BB6E615 (void);
// 0x000004EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_LoadNewFile>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m08D7E1B935DF8D5DDE59EE125E5457EDF6EC3426 (void);
// 0x000004ED System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_FileExists>d__14::MoveNext()
extern void U3CUWP_FileExistsU3Ed__14_MoveNext_m9C9E5129BAEAF23D1AE693F6D4C27BDA11AF5359 (void);
// 0x000004EE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_FileExists>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_FileExistsU3Ed__14_SetStateMachine_m40DA4924B55A8B15F9E4BAB780B72948A8FD0C1D (void);
// 0x000004EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_ReadData>d__15::MoveNext()
extern void U3CUWP_ReadDataU3Ed__15_MoveNext_mC6415C909253B839D77B2FA26344FBAB31987CBD (void);
// 0x000004F0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_ReadData>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_ReadDataU3Ed__15_SetStateMachine_m7FB0514A5F50E95AEE67876D43CB68734CB6F2D4 (void);
// 0x000004F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<LoadInUWP>d__21::MoveNext()
extern void U3CLoadInUWPU3Ed__21_MoveNext_m2CD40348FA310488ABBB418C2B433AB9C27D61C0 (void);
// 0x000004F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<LoadInUWP>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadInUWPU3Ed__21_SetStateMachine_mA2CB12B355F31796EF31B4B349AD43BEC4AA16E4 (void);
// 0x000004F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::.ctor(System.Int32)
extern void U3CPopulateHeatmapU3Ed__42__ctor_mFEB8BC1B8438EDC42891740833DD8E8B6899D765 (void);
// 0x000004F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.IDisposable.Dispose()
extern void U3CPopulateHeatmapU3Ed__42_System_IDisposable_Dispose_m25C0C9052C7C7EA3EED60A7B232EB12B8DD6DB6B (void);
// 0x000004F5 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::MoveNext()
extern void U3CPopulateHeatmapU3Ed__42_MoveNext_m8FB1222539AC58D5758B7CA54584D9ABF06A80FC (void);
// 0x000004F6 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPopulateHeatmapU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DEBA026CE679FB88BD7FE72E67CCEE67E2E182E (void);
// 0x000004F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.Collections.IEnumerator.Reset()
extern void U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_Reset_m1DAD006870E46A0DD9B39FE51E9A2CF30DA21617 (void);
// 0x000004F8 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_get_Current_mCDEE30E2F0FE5048403561399AE76FDB449366D0 (void);
// 0x000004F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::.ctor(System.Int32)
extern void U3CUpdateStatusU3Ed__43__ctor_mD576E82ABBED4B360863524A4F69B9BA7D8D09B3 (void);
// 0x000004FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.IDisposable.Dispose()
extern void U3CUpdateStatusU3Ed__43_System_IDisposable_Dispose_mD7FBADBEB5474CDA9F8FC54DE1AB4E60C1E4D4A3 (void);
// 0x000004FB System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::MoveNext()
extern void U3CUpdateStatusU3Ed__43_MoveNext_mA417CB03A73D772820F828B4797733007A904340 (void);
// 0x000004FC System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateStatusU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD92EF9DB25465F34E86245F82272ADCEAE87D159 (void);
// 0x000004FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.Collections.IEnumerator.Reset()
extern void U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_Reset_m57A40C13F3D1AD1ACA7CCE9243F449C651E752A3 (void);
// 0x000004FE System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_get_Current_m216A1667EA3DEE80308E3AA09DCC9CC9AC6199CB (void);
// 0x000004FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::.ctor(System.Int32)
extern void U3CAddToCounterU3Ed__44__ctor_m9472F6FFDFEF3B98CEB09D32B5F8C44B17C6D003 (void);
// 0x00000500 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.IDisposable.Dispose()
extern void U3CAddToCounterU3Ed__44_System_IDisposable_Dispose_mC040F55582579BC4B84C95F6FF7BA79B6093F88E (void);
// 0x00000501 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::MoveNext()
extern void U3CAddToCounterU3Ed__44_MoveNext_mA4E3D0F1235B6CD1307C2FBD1B2C4128739D089F (void);
// 0x00000502 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAddToCounterU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFAB8863E56E1688FAC9D1A040A8A5B3362492A78 (void);
// 0x00000503 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.Collections.IEnumerator.Reset()
extern void U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_Reset_m6CEDB25F9FB1FB8A1E990B25E617DB4225F05E29 (void);
// 0x00000504 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_get_Current_m8DA98EE14D3229C805831FAE82F0590DF9186BC9 (void);
// 0x00000505 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::get_Instance()
extern void UserInputRecorder_get_Instance_mA42897FA3A7809865142C5CDFACEC81B96115736 (void);
// 0x00000506 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetHeader()
extern void UserInputRecorder_GetHeader_m4E1EF17483C22B42D63094C63255699ED2F176AC (void);
// 0x00000507 System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetData_Part1()
extern void UserInputRecorder_GetData_Part1_m1CD4A8771239C2B72B75271A33AF69CB5281AD38 (void);
// 0x00000508 System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::MergeObjArrays(System.Object[],System.Object[])
extern void UserInputRecorder_MergeObjArrays_m0F2A8FAF0C0414A2775A5EC49E509AEFD0E6210E (void);
// 0x00000509 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetFileName()
extern void UserInputRecorder_GetFileName_m80A4298C1350FAFAA3000EB71EFAD41EA345529F (void);
// 0x0000050A System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::LimitStringLength(System.String,System.Int32)
extern void UserInputRecorder_LimitStringLength_mDAD5453704B180BAAEA5F8B47EFDEE9035FC5765 (void);
// 0x0000050B System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetStringFormat(System.Object[])
extern void UserInputRecorder_GetStringFormat_m007CDF936438A9CB84EA612FDABA4BC396FE93F3 (void);
// 0x0000050C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::UpdateLog(System.String,System.String,Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void UserInputRecorder_UpdateLog_m05C644149530FB3511BB26934713EE775D90C575 (void);
// 0x0000050D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::CustomAppend(System.String)
extern void UserInputRecorder_CustomAppend_mBAC7C3131EC4FAA890AE59F64821917BE339793D (void);
// 0x0000050E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::UpdateLog()
extern void UserInputRecorder_UpdateLog_m5B2E28D08848E13F92FED146ED3E5302C47E8E43 (void);
// 0x0000050F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::Update()
extern void UserInputRecorder_Update_mEC43DBA63B0290ED1E5F68BD4E952E3E6E0A1261 (void);
// 0x00000510 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::OnDestroy()
extern void UserInputRecorder_OnDestroy_m24C1FC84FCC3F5CD2AC648395FEA96AB1B3CB422 (void);
// 0x00000511 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::.ctor()
extern void UserInputRecorder__ctor_m6D09773EE4B28256885E670CF176F2C6FA90C827 (void);
// 0x00000512 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::PlayAudio(UnityEngine.AudioClip)
extern void UserInputRecorderFeedback_PlayAudio_mB74774A018E1F222708E7F7182E7D30EF79B7A7A (void);
// 0x00000513 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::UpdateStatusText(System.String)
extern void UserInputRecorderFeedback_UpdateStatusText_m1EEAA1B0E8C6E49018D8B31B800AFCF6E8FC2772 (void);
// 0x00000514 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::ResetStatusText()
extern void UserInputRecorderFeedback_ResetStatusText_mEA67A8CDE68FE4BC92A7691CA6E1241C693FE768 (void);
// 0x00000515 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::Update()
extern void UserInputRecorderFeedback_Update_m5B5CFE50E23741A3F9F071BAEB5FB7BAE6CC48E9 (void);
// 0x00000516 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::StartRecording()
extern void UserInputRecorderFeedback_StartRecording_m08B966EB4ECA894A3649829EA144D7F9A4F675E5 (void);
// 0x00000517 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::StopRecording()
extern void UserInputRecorderFeedback_StopRecording_m726CCCE3DAD9B568ED0CFDFA8612433C8FEC25BE (void);
// 0x00000518 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::LoadData()
extern void UserInputRecorderFeedback_LoadData_m033FF5F070D62978136ED080928D0088F32D114C (void);
// 0x00000519 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::StartReplay()
extern void UserInputRecorderFeedback_StartReplay_mB2242EB195576153BEA6A8E6FD7DA48642BE9416 (void);
// 0x0000051A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::PauseReplay()
extern void UserInputRecorderFeedback_PauseReplay_m780CAB3B11222EEE5935ED3BA0AFBF289CDAA6F8 (void);
// 0x0000051B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::.ctor()
extern void UserInputRecorderFeedback__ctor_m52A8C8EFC72559AD4847AA794484C1272F728EBA (void);
// 0x0000051C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::Start()
extern void UserInputRecorderUIController_Start_m8F233705BC48CE2DC032E948BCFA613CA2B2A4B6 (void);
// 0x0000051D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::StartRecording()
extern void UserInputRecorderUIController_StartRecording_mFAC6974D4BF41107B75BA45259433014A3ED5DE8 (void);
// 0x0000051E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::StopRecording()
extern void UserInputRecorderUIController_StopRecording_mB2D99A729598F6B20F5244DBDD7767BEE5AD9C87 (void);
// 0x0000051F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::RecordingUI_Reset(System.Boolean)
extern void UserInputRecorderUIController_RecordingUI_Reset_m3C9EE2D4DCAFAE8729541FCCB28794CCB2EFFD04 (void);
// 0x00000520 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::LoadData()
extern void UserInputRecorderUIController_LoadData_mA6F77791E1A8E34FCACF97C7DB5ACD3CEC35FA32 (void);
// 0x00000521 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::ReplayUI_SetActive(System.Boolean)
extern void UserInputRecorderUIController_ReplayUI_SetActive_m9E796554BEE3F24703AB2601B6C831852014F107 (void);
// 0x00000522 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::StartReplay()
extern void UserInputRecorderUIController_StartReplay_m31728EA29978ACCD9814ED03915EB61C5C6F7BB7 (void);
// 0x00000523 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::PauseReplay()
extern void UserInputRecorderUIController_PauseReplay_mD5C6F4C72418D514A62DC8F069D05666D03F5FBD (void);
// 0x00000524 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::ResetPlayback(System.Boolean,System.Boolean)
extern void UserInputRecorderUIController_ResetPlayback_m3A5DAD96B6B195932FE40557E203A89D634A132A (void);
// 0x00000525 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::.ctor()
extern void UserInputRecorderUIController__ctor_m12D57C4EC7ABA1263A66F606BE36AFC02DE2AE6D (void);
// 0x00000526 System.Void MRTK.Tutorials.GettingStarted.DirectionalIndicatorController::OnBecameInvisible()
extern void DirectionalIndicatorController_OnBecameInvisible_m81BAC96F24A8B8FD38A5A1B399194F4E2E9CF353 (void);
// 0x00000527 System.Void MRTK.Tutorials.GettingStarted.DirectionalIndicatorController::.ctor()
extern void DirectionalIndicatorController__ctor_mAF93D393485CD7937E4F0A049CAE0D80D17AA0C2 (void);
// 0x00000528 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::set_IsPunEnabled(System.Boolean)
extern void ExplodeViewController_set_IsPunEnabled_m9B9F54E404B03478F23B116EBDA6D22729947B1C (void);
// 0x00000529 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::Start()
extern void ExplodeViewController_Start_mEBCA23758FC99609617CEE99E5CC6AD1A8E8DD28 (void);
// 0x0000052A System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::Update()
extern void ExplodeViewController_Update_m68272F7CABAE057B7402902EACC742A5FD11B266 (void);
// 0x0000052B System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::ToggleExplodedView()
extern void ExplodeViewController_ToggleExplodedView_m59FB613ADA832339B088AF41B27DF25C59EB6E68 (void);
// 0x0000052C System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::Toggle()
extern void ExplodeViewController_Toggle_mC19D29CA70BFBE320678DC27565EED8AE3775011 (void);
// 0x0000052D System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::add_OnToggleExplodedView(MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate)
extern void ExplodeViewController_add_OnToggleExplodedView_m7B7C12027D19E16A6E136FB760FB843493F87FB5 (void);
// 0x0000052E System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::remove_OnToggleExplodedView(MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate)
extern void ExplodeViewController_remove_OnToggleExplodedView_m6D5BED15516C033624058FA746BD3E7A630B738B (void);
// 0x0000052F System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController::.ctor()
extern void ExplodeViewController__ctor_m6B9698AB8508AB45BA9EFE8C003A368F956A39C9 (void);
// 0x00000530 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate::.ctor(System.Object,System.IntPtr)
extern void ExplodeViewControllerDelegate__ctor_m00CF199628C294EC4C78DB6BCFE9CA99F31E1187 (void);
// 0x00000531 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate::Invoke()
extern void ExplodeViewControllerDelegate_Invoke_m9475EC90F8EABC05D3124071E6AC55FA93C56D16 (void);
// 0x00000532 System.IAsyncResult MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void ExplodeViewControllerDelegate_BeginInvoke_m4CE6E375D997DAAA0E3FAFF36F4700838C51D958 (void);
// 0x00000533 System.Void MRTK.Tutorials.GettingStarted.ExplodeViewController/ExplodeViewControllerDelegate::EndInvoke(System.IAsyncResult)
extern void ExplodeViewControllerDelegate_EndInvoke_m073ACDB10BEA116E55E112E082F114D15ADECD76 (void);
// 0x00000534 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::set_IsPunEnabled(System.Boolean)
extern void PartAssemblyController_set_IsPunEnabled_m3B2755FC9F412A9A80F00E881A43C9E3D71259BD (void);
// 0x00000535 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::Start()
extern void PartAssemblyController_Start_m7662EBE3717584F93527DFF47EBFF7B17E3CAE74 (void);
// 0x00000536 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::SetPlacement()
extern void PartAssemblyController_SetPlacement_m3753B2A0342CD8A54914FFAD1B38D887F1C504C9 (void);
// 0x00000537 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::Set()
extern void PartAssemblyController_Set_m78C7CE4286BD0961DA1318A24C6EA8692CAF18CF (void);
// 0x00000538 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::ResetPlacement()
extern void PartAssemblyController_ResetPlacement_m87A7AB3C4C5D3C539EDAB43011E775BC5C6D1646 (void);
// 0x00000539 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::Reset()
extern void PartAssemblyController_Reset_m3F1C2A8DEC7F886D75DB1B37F406AA7932399AB5 (void);
// 0x0000053A System.Collections.IEnumerator MRTK.Tutorials.GettingStarted.PartAssemblyController::CheckPlacement()
extern void PartAssemblyController_CheckPlacement_mCD2226DB9D0976415A580C7B4D161A5B0C087D96 (void);
// 0x0000053B System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::add_OnResetPlacement(MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate)
extern void PartAssemblyController_add_OnResetPlacement_m05594F373CAAF81B52DCE6B53F7080535AF36A6C (void);
// 0x0000053C System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::remove_OnResetPlacement(MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate)
extern void PartAssemblyController_remove_OnResetPlacement_mED257FFBB650024D91B8441D3BD45A8173FCFED2 (void);
// 0x0000053D System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::add_OnSetPlacement(MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate)
extern void PartAssemblyController_add_OnSetPlacement_mB02D746A05D3A8CF8260832887C55D53D74649DC (void);
// 0x0000053E System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::remove_OnSetPlacement(MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate)
extern void PartAssemblyController_remove_OnSetPlacement_m6EDB130CABBA27EF6539D4A22EF2B451462458BA (void);
// 0x0000053F System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController::.ctor()
extern void PartAssemblyController__ctor_m4C1B83FE2E3CF9F06A2AF8F5D366BBCDECA4B5AC (void);
// 0x00000540 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate::.ctor(System.Object,System.IntPtr)
extern void PartAssemblyControllerDelegate__ctor_m7F2B8D7A6D04834696B8F6F71E84D644D99B68EF (void);
// 0x00000541 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate::Invoke()
extern void PartAssemblyControllerDelegate_Invoke_mC5F3437F064E32947FFAB510C33A36A1AD8A059C (void);
// 0x00000542 System.IAsyncResult MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void PartAssemblyControllerDelegate_BeginInvoke_m7671440389FDAA059E71ADFF712A82E1AD7B8CEB (void);
// 0x00000543 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/PartAssemblyControllerDelegate::EndInvoke(System.IAsyncResult)
extern void PartAssemblyControllerDelegate_EndInvoke_m09C63B12E14C3681A69C6DC62AFFE9C77685B03C (void);
// 0x00000544 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::.ctor(System.Int32)
extern void U3CCheckPlacementU3Ed__25__ctor_mD99D1F68A1B42D45D5E0303134EB8CDEF442F9B0 (void);
// 0x00000545 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::System.IDisposable.Dispose()
extern void U3CCheckPlacementU3Ed__25_System_IDisposable_Dispose_m0A3FD3FF6019150BD5AA2392C53BC217DB95477F (void);
// 0x00000546 System.Boolean MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::MoveNext()
extern void U3CCheckPlacementU3Ed__25_MoveNext_mBEFDAA4E1B5E6C090C1C516910FE692C516319AE (void);
// 0x00000547 System.Object MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckPlacementU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE0F540979B60424322B424EC85CC6A7F660C014C (void);
// 0x00000548 System.Void MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::System.Collections.IEnumerator.Reset()
extern void U3CCheckPlacementU3Ed__25_System_Collections_IEnumerator_Reset_m9CFCADBFA8E2719A5D2EAED052816115B65083D4 (void);
// 0x00000549 System.Object MRTK.Tutorials.GettingStarted.PartAssemblyController/<CheckPlacement>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CCheckPlacementU3Ed__25_System_Collections_IEnumerator_get_Current_mC1774CBAA3E10DA18E8343A526ACBA70553C607B (void);
// 0x0000054A System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::set_IsPunEnabled(System.Boolean)
extern void PlacementHintsController_set_IsPunEnabled_m1364F13B088427C7258B0AED579757B594B74A0E (void);
// 0x0000054B System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::Start()
extern void PlacementHintsController_Start_m306723AF3759666445826208B8BD76902741002F (void);
// 0x0000054C System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::TogglePlacementHints()
extern void PlacementHintsController_TogglePlacementHints_mF5A22F14A17D362A2FAE71EFD314A4BB14D7A28E (void);
// 0x0000054D System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::Toggle()
extern void PlacementHintsController_Toggle_m5285332F7C240D90986D60F15827EC5718A9FE7D (void);
// 0x0000054E System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::add_OnTogglePlacementHints(MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate)
extern void PlacementHintsController_add_OnTogglePlacementHints_m4BE517DA85E4BC9F742A80F7863D428A2F4DC931 (void);
// 0x0000054F System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::remove_OnTogglePlacementHints(MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate)
extern void PlacementHintsController_remove_OnTogglePlacementHints_mED93E45F3C670BA58DDF275DE9C576B48D51024C (void);
// 0x00000550 System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController::.ctor()
extern void PlacementHintsController__ctor_m789398D4FEC0B3548A616A086A9CD5121B51DED0 (void);
// 0x00000551 System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate::.ctor(System.Object,System.IntPtr)
extern void PlacementHintsControllerDelegate__ctor_m6483805F57E4800C0D83C9D905A5C2CE71432B84 (void);
// 0x00000552 System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate::Invoke()
extern void PlacementHintsControllerDelegate_Invoke_mFB26FE30BE1AE37EF71E6B55861A49F1125F9554 (void);
// 0x00000553 System.IAsyncResult MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void PlacementHintsControllerDelegate_BeginInvoke_m29B9BE46FFF16EF20BC950660E42F1E0FE71F841 (void);
// 0x00000554 System.Void MRTK.Tutorials.GettingStarted.PlacementHintsController/PlacementHintsControllerDelegate::EndInvoke(System.IAsyncResult)
extern void PlacementHintsControllerDelegate_EndInvoke_m158B6288E2B765CD86F64CB51EBE247426631117 (void);
// 0x00000555 System.Void MRTK.Tutorials.AzureSpatialAnchors.DebugWindow::Start()
extern void DebugWindow_Start_mE4DB4982DB85A70C2B2E0DB3528249AC0F326FA2 (void);
// 0x00000556 System.Void MRTK.Tutorials.AzureSpatialAnchors.DebugWindow::OnDestroy()
extern void DebugWindow_OnDestroy_mCF6E877DFC0EFBA82A32169CAA288EC49FDA5775 (void);
// 0x00000557 System.Void MRTK.Tutorials.AzureSpatialAnchors.DebugWindow::HandleLog(System.String,System.String,UnityEngine.LogType)
extern void DebugWindow_HandleLog_m979E071474C595A8B1848C110C45F0B946EE5BBC (void);
// 0x00000558 System.Void MRTK.Tutorials.AzureSpatialAnchors.DebugWindow::.ctor()
extern void DebugWindow__ctor_m283AC06C16E0FB054BF57249D8CB64F24E82FA2D (void);
// 0x00000559 System.Void MRTK.Tutorials.AzureSpatialAnchors.DisableDiagnosticsSystem::.ctor()
extern void DisableDiagnosticsSystem__ctor_m586116B337F12DF5FE5CB804EECAD6D504D33DB6 (void);
static Il2CppMethodPointer s_methodPointers[1369] = 
{
	AnchorFeedbackScript_Awake_m8C7C5106D96097F43537AA4ED7E17629808B8BE4,
	AnchorFeedbackScript_Start_m8633B37353754E7EA06489C1B19A7451706EE40D,
	AnchorFeedbackScript_AnchorModuleScript_OnStartASASession_m1C0208D946F37B6FABDC1D8D392F40EF6162325C,
	AnchorFeedbackScript_AnchorModuleScript_OnEndASASession_m44EB1F2F07C6617DDECFB86FF7806198D4DE5997,
	AnchorFeedbackScript_AnchorModuleScript_OnCreateAnchorStarted_m3804F2E06F1481EBDD885C0EC8DDEEF974DC4025,
	AnchorFeedbackScript_AnchorModuleScript_OnCreateAnchorSucceeded_m42E9F331602E2F6941EBCBB24D64A502AA4BE698,
	AnchorFeedbackScript_AnchorModuleScript_OnCreateAnchorFailed_m6870E07E4F937ECE2AB3B4B52D4B81E01FD6CFFC,
	AnchorFeedbackScript_AnchorModuleScript_OnCreateLocalAnchor_m126A81B3DD3604B1C7AE5FFC233928DB8E1192F4,
	AnchorFeedbackScript_AnchorModuleScript_OnRemoveLocalAnchor_m2C10AABEB867CA1CD4EF43ACD5E3162A7034FF8D,
	AnchorFeedbackScript_AnchorModuleScript_OnFindASAAnchor_mDAC84A6D053DF688EACCE64D562BB052D0972765,
	AnchorFeedbackScript_AnchorModuleScript_OnASAAnchorLocated_m62B31964A1E7E3A95B76D1E6D4FBCDBC6934EB01,
	AnchorFeedbackScript__ctor_m9DB66C384CAA23363AA1B58971A7D9EEB243439F,
	AnchorModuleScript_Start_mFBE9F27E302E395093F56B907EE29613BEDB5A4F,
	AnchorModuleScript_Update_m73B821FCF903C4D4996A1CCD8980859B16CECD89,
	AnchorModuleScript_OnDestroy_m5E5C1EF97883D304B71AF7DA2DC44722CD541F0D,
	AnchorModuleScript_StartAzureSession_mE0DE4CD1D9D1AC9A39CB3639AD349FE34A0CCDB2,
	AnchorModuleScript_StopAzureSession_mC6B5843F849B5644C1284327C4A5FDE3476F6A4A,
	AnchorModuleScript_CreateAzureAnchor_mC1B80FAF523304BC6F8CF3569EA530D35285A53B,
	AnchorModuleScript_RemoveLocalAnchor_m778A9ACBF51C57013126B2D77DC7D66EE7029919,
	AnchorModuleScript_FindAzureAnchor_m196572032AD1ABFDC99646EB5EC5CADE8B2E33F8,
	AnchorModuleScript_DeleteAzureAnchor_mED2B07FEDB0CA7B49E2F58B838C699F2051689BE,
	AnchorModuleScript_SaveAzureAnchorIdToDisk_mDC61452065EB769DD04D8917764C278E470E7A37,
	AnchorModuleScript_GetAzureAnchorIdFromDisk_mFDCEE50EC71AC86EFA284416C6A82C4427911582,
	AnchorModuleScript_ShareAzureAnchorIdToNetwork_m4FA0D227017FDF14ABC7FE515FAFCA704ACC7C87,
	AnchorModuleScript_GetAzureAnchorIdFromNetwork_mD30FB0E386DC4A73B668C1147935F208B99D0479,
	AnchorModuleScript_CloudManager_AnchorLocated_m4901EA6011DEB0AB2D36E65CA39854E9F9D4AF26,
	AnchorModuleScript_QueueOnUpdate_m6CCB426D379767DBB1876F81F7510C0F16465F8D,
	AnchorModuleScript_ShareAzureAnchorIdToNetworkCoroutine_m8043DC685DD62288B7A367E5F3A0905D489B8BCD,
	AnchorModuleScript_GetSharedAzureAnchorIDCoroutine_mD8F93F5CFE0735475DFC9812335C117414208457,
	AnchorModuleScript_add_OnStartASASession_m9E1F126DFF9DE26602B62250B2525EF3FD8873EA,
	AnchorModuleScript_remove_OnStartASASession_m7A99EB51CB4AA53E306BD7161A054466CC14CB0F,
	AnchorModuleScript_add_OnEndASASession_mC42AB0B84878682D0610DBBD8DE563ED101BAB8D,
	AnchorModuleScript_remove_OnEndASASession_m48A1D28B60921A565048325984084CF05899550D,
	AnchorModuleScript_add_OnCreateAnchorStarted_m290AEAF30C103B0FE2F2954261B161122AF460CC,
	AnchorModuleScript_remove_OnCreateAnchorStarted_mF4734B1784E83F0A6DE263BF832ADFA1D6AA8891,
	AnchorModuleScript_add_OnCreateAnchorSucceeded_m2B97E028C8CB1F40CA8C470F6A705D406FBAAA8F,
	AnchorModuleScript_remove_OnCreateAnchorSucceeded_m597DCF9B72826687D8AB6B56577C6626460949C1,
	AnchorModuleScript_add_OnCreateAnchorFailed_m0D63C171B5CDE942BD811EEB4BAE607BF2B706A0,
	AnchorModuleScript_remove_OnCreateAnchorFailed_mBEF0CF8B5AF568E0547729AB5D42036BBD557C40,
	AnchorModuleScript_add_OnCreateLocalAnchor_m90DC5E7F24EBADD06F73A7DA8DD24820AB9428CB,
	AnchorModuleScript_remove_OnCreateLocalAnchor_mF30CF49094AB87B4EDD5B83DC1D8A1F2423575DF,
	AnchorModuleScript_add_OnRemoveLocalAnchor_m8C730185093BB514F7EBFFEC742F4EEE222C4E6F,
	AnchorModuleScript_remove_OnRemoveLocalAnchor_m72122C75147589814951FAD4ACF15EF5BE5CD41A,
	AnchorModuleScript_add_OnFindASAAnchor_m1FDC5B6B678C0C1C033466257B43B08C087590EB,
	AnchorModuleScript_remove_OnFindASAAnchor_mA25D92B8EFCA5557F3575F446747741EAAC888C5,
	AnchorModuleScript_add_OnASAAnchorLocated_m9912B081D7CF6F7BFB8A9D82C7FDD3A893C38FAC,
	AnchorModuleScript_remove_OnASAAnchorLocated_m36541F05B4C456E131368E31E6439301F74928EE,
	AnchorModuleScript_add_OnDeleteASAAnchor_m1884E831956AB59EC774AB213EF4EEA8985DEF32,
	AnchorModuleScript_remove_OnDeleteASAAnchor_m65E2D188E27BDCE82ABAD2430C113E957C7AAAE0,
	AnchorModuleScript__ctor_m758023C408A657AD13AF0FCD075D9720CDDC4211,
	StartASASessionDelegate__ctor_m9D427688C3705D2DBC2B1F1B512AD63DC630174E,
	StartASASessionDelegate_Invoke_m45223B8A76264C5AD2C10E0068BB1E6115C74898,
	StartASASessionDelegate_BeginInvoke_mD2D1E587CACD9D0F043027E0D2B72DD4FEFB68C3,
	StartASASessionDelegate_EndInvoke_mB711351D9CEA770550C66456BBA17E1A7066E113,
	EndASASessionDelegate__ctor_mCEB47632974ECB3802740517303F1784F27FC33E,
	EndASASessionDelegate_Invoke_m6C360E6B6D453CB6909E259993C990802BA98613,
	EndASASessionDelegate_BeginInvoke_m052A51473B60FE826B9A7BA832A1C678752833D9,
	EndASASessionDelegate_EndInvoke_m1F7184B0119F0C0E09404D1DD0C84039BD51468E,
	CreateAnchorDelegate__ctor_m5FB74DEA4AE88293C06705290538C53F870404BA,
	CreateAnchorDelegate_Invoke_m04854CF72C1189DF92AC3721645745CAC0A5A46D,
	CreateAnchorDelegate_BeginInvoke_m7223440DD7A6138AD1A2006F66CBBF828CFB1EE0,
	CreateAnchorDelegate_EndInvoke_mD6CB12A5C01C1969420EAD6114AF1A9A972FCE04,
	CreateLocalAnchorDelegate__ctor_mAAD201999767CC679BB6938B1DB042DE0E3E65BD,
	CreateLocalAnchorDelegate_Invoke_m567AD8D9AA96A42F930DC5CE80FEFBB6C3B8288F,
	CreateLocalAnchorDelegate_BeginInvoke_m312DDDD72D5C559323A044C7497C4C81A184E9AF,
	CreateLocalAnchorDelegate_EndInvoke_m8927B83EF93B4B9F3FBCEA1E825A350094713A0D,
	RemoveLocalAnchorDelegate__ctor_m3583EDF10B64499FECF07E3906CB8E8CDAF7084C,
	RemoveLocalAnchorDelegate_Invoke_mDE3FC06E7812F6374D403B629D800D6FD2C2F1BD,
	RemoveLocalAnchorDelegate_BeginInvoke_m1445301D7DF64AA6D908750360E94870BFE0A148,
	RemoveLocalAnchorDelegate_EndInvoke_mCA219DBEE9E2D466B6BF2DAB7B7BCF7A0AF6EC4D,
	FindAnchorDelegate__ctor_m4E4AE8E63A5932F12B26B246C37EFBC5CDD170E8,
	FindAnchorDelegate_Invoke_mFDF2FE513B8150280949A5B20B456DEFB11D70C3,
	FindAnchorDelegate_BeginInvoke_mE0AF6C5526E5D65CEC381888AEB28F658EAFAC48,
	FindAnchorDelegate_EndInvoke_m560106CE5454CD670233A809653CF03125D135E6,
	AnchorLocatedDelegate__ctor_m65D7C380783E0F5B49F4D59E5D2AD88969A65AF1,
	AnchorLocatedDelegate_Invoke_mFAB4D550DD52EC5CB81467911F20F3AE0645346B,
	AnchorLocatedDelegate_BeginInvoke_mC1E0AF54A704F3C0642AA23FD947633A02364E53,
	AnchorLocatedDelegate_EndInvoke_m44D4D7E6BB3358A3CAB46BDFECC4F34309FAF4CA,
	DeleteASAAnchorDelegate__ctor_m207B762CC3BF99A4B0DCB3C33B23096C4CD7ECC5,
	DeleteASAAnchorDelegate_Invoke_mCB11AF4379DB50FC161DF2DCD5F09B43CB951F6E,
	DeleteASAAnchorDelegate_BeginInvoke_mF6907845BDFCEEFEF74ADFAAF08CBF2CEE43ADE4,
	DeleteASAAnchorDelegate_EndInvoke_m5A19B52E0D9C8C049A75944826FA38610435DE9D,
	U3CStartAzureSessionU3Ed__11_MoveNext_m096E8B5FFFFF21934958199F5E704B0A35030F86,
	U3CStartAzureSessionU3Ed__11_SetStateMachine_mC14F49D42F4196CE29AC0BB8D170C8822B39EDBA,
	U3CStopAzureSessionU3Ed__12_MoveNext_mB56F54C704AA91C36474482A1D72AD4ADCCECDBF,
	U3CStopAzureSessionU3Ed__12_SetStateMachine_m7026DDC7EDB620D4ADDD1EC4AB67208C3067FEC1,
	U3CU3Ec__DisplayClass13_0__ctor_m999AF18E0C633FF39F108F9997E3C306D9EC4111,
	U3CU3Ec__DisplayClass13_0_U3CCreateAzureAnchorU3Eb__0_m1A18F3105143BA74FD4DAD42106E0401F7689A8C,
	U3CCreateAzureAnchorU3Ed__13_MoveNext_mC928D721B690A52C63E28A6F87DCA0B85C4016EB,
	U3CCreateAzureAnchorU3Ed__13_SetStateMachine_m1CB417E804D0F9776B38FE512446F426683AEC70,
	U3CDeleteAzureAnchorU3Ed__16_MoveNext_m57179936926C706B648D169308C85A22C5F69FD9,
	U3CDeleteAzureAnchorU3Ed__16_SetStateMachine_m81A308F54E85613A92D46E6623D04E5ADFD383C5,
	U3CU3Ec__DisplayClass21_0__ctor_mCC87BAED3EC57EBA53AA64FB246DABBFD1DD71AD,
	U3CU3Ec__DisplayClass21_0_U3CCloudManager_AnchorLocatedU3Eb__1_m37751B6CA50FFFC0FB18973E0AB2C08E70FE6930,
	U3CU3Ec__DisplayClass21_0_U3CCloudManager_AnchorLocatedU3Eb__2_mD6A7F8D836409E1FA099392BA6B63B1AE1537B2B,
	U3CU3Ec__cctor_m5AC2E6FC709218D26C6DD25B9129C8530A7E2D1D,
	U3CU3Ec__ctor_m536152BE87B22D0313FBECB2D9062B85BB2B09FB,
	U3CU3Ec_U3CCloudManager_AnchorLocatedU3Eb__21_0_m356E27F42FF9C0FFEBDD6649EF07643130A54FDD,
	U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23__ctor_m6C041396A7D4FC8EBABA2129E8C77E1CF2E758EF,
	U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_System_IDisposable_Dispose_m2157092318FCCFF36DE8345E71BD60E0772E2BED,
	U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_MoveNext_m8B29D15F7EBA0023CB6FD0E1FC9FA2A301A4B617,
	U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49B75272B861E6173A999958AA0F6408D75E52D5,
	U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_System_Collections_IEnumerator_Reset_mA948E2B23CA6BA09C43E944E99B1F3576A06437E,
	U3CShareAzureAnchorIdToNetworkCoroutineU3Ed__23_System_Collections_IEnumerator_get_Current_m1AA3E2E5F17A260616C403C9553784B6F9F146B7,
	U3CGetSharedAzureAnchorIDCoroutineU3Ed__24__ctor_m167833FAEDD66EB43B9E52337A1388757894F307,
	U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_System_IDisposable_Dispose_m31FA364EBDCD64EA44E2CC9022A11BAAC7DCC9D2,
	U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_MoveNext_mBEE753F6A9B63B03B7C08CAD4824382C8A0E3518,
	U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_U3CU3Em__Finally1_m1B3A37A8E00196BD5CE63FCFBC4C6AC6CAB7D96C,
	U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C570B293C649BAF8E80AF9F799BE61DA72F2304,
	U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_System_Collections_IEnumerator_Reset_m96D368E30405AEA9B584C9013FFF43BB02BFAFE1,
	U3CGetSharedAzureAnchorIDCoroutineU3Ed__24_System_Collections_IEnumerator_get_Current_mFB137B6F28CC285B8DABB29395499389EB924D0C,
	ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7,
	ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46,
	ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722,
	ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719,
	DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F,
	DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B,
	EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435,
	EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9,
	EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138,
	U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C,
	U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA,
	TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9,
	TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0,
	TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3,
	TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D,
	TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3,
	TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C,
	TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82,
	TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5,
	TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C,
	TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3,
	TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908,
	TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427,
	TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D,
	TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF,
	TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052,
	TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1,
	TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A,
	TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6,
	TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189,
	TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475,
	TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33,
	TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6,
	TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02,
	TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB,
	CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E,
	SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963,
	WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2,
	LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2,
	LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC,
	Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9,
	Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4,
	U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D,
	U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD,
	Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA,
	Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB,
	U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC,
	U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A,
	Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29,
	Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1,
	Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3,
	Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D,
	Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6,
	Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90,
	Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27,
	CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788,
	CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5,
	CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F,
	CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F,
	CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878,
	ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72,
	ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695,
	ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A,
	ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371,
	ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88,
	ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56,
	ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B,
	U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA,
	SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8,
	SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE,
	SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807,
	SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93,
	SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690,
	SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB,
	SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362,
	SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22,
	U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51,
	U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5,
	TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D,
	TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6,
	TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0,
	U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2,
	U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8,
	TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C,
	TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39,
	TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95,
	TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689,
	TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A,
	TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52,
	TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1,
	TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025,
	U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B,
	U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186,
	U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5,
	U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E,
	TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2,
	TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE,
	TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E,
	TextMeshProFloatingText__cctor_m272097816057A64A9FFE16F69C6844DCF88E9557,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55,
	TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D,
	TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66,
	TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233,
	TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C,
	TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839,
	TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063,
	TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1,
	TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A,
	TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C,
	TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A,
	TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723,
	TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43,
	TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E,
	TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6,
	TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE,
	TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215,
	TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79,
	TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559,
	TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779,
	TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF,
	TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E,
	TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797,
	TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B,
	TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A,
	TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503,
	TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE,
	TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87,
	TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B,
	TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE,
	TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF,
	TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98,
	TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65,
	TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97,
	TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398,
	TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301,
	TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491,
	TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F,
	TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36,
	TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848,
	VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1,
	VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B,
	VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC,
	VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C,
	VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C,
	VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354,
	VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2,
	VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76,
	VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8,
	VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88,
	VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D,
	U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9,
	VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867,
	VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304,
	VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23,
	VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E,
	VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77,
	VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38,
	VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB,
	U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B,
	VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E,
	VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21,
	VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5,
	VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1,
	VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D,
	VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F,
	VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198,
	U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0,
	VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0,
	VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47,
	VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47,
	VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB,
	VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7,
	VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0,
	VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422,
	U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A,
	U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C,
	WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F,
	WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809,
	WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF,
	WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98,
	WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795,
	U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53,
	U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19,
	BaseDwellSample_get_DwellHandler_m2098A2AD0AC23E583BAD69EFF25D63BAB03AD8D8,
	BaseDwellSample_set_DwellHandler_mA13C8281843B65F232805CE69962EAF37307A090,
	BaseDwellSample_get_IsDwelling_mAFB7EEEDBF99E3CDF7CF08BD6F9A14FE867C4EAD,
	BaseDwellSample_set_IsDwelling_mA8F2120E1D25CD3692EC4B8D594B6AA39DE71742,
	BaseDwellSample_Awake_m8DDDC7BFB655CB5B9A0A8370C66892BEC288BA62,
	BaseDwellSample_DwellStarted_m30E7E408EF442CEA9AFB7747260944CCBE37D5B9,
	BaseDwellSample_DwellIntended_mA5CC33B44F7BE0491EA3F88A504F2C1647596C40,
	BaseDwellSample_DwellCanceled_m7C036A8AC4593510B2A1B6383BC3535B4CF94C81,
	BaseDwellSample_DwellCompleted_m7962EC121A5E4561000D9316100224109E7402AD,
	BaseDwellSample_ButtonExecute_m23A7395A20886C88FF24EBF7232D8F766EB8F4B9,
	BaseDwellSample__ctor_m123AEDB828DB2A723807218F667DFE90B59883FA,
	InstantDwellSample_Update_m6AEF94A44BD0C995B3A7649B305C29A37770F43F,
	InstantDwellSample_DwellCompleted_m4B9A0A068D6E739C947F55DB4156D4CD1838DF79,
	InstantDwellSample_ButtonExecute_m6CCDCA462AA91ABB0DE2D8E280662F6B4D3E4885,
	InstantDwellSample__ctor_m12EFAE67E2B1D9767F06B4940B4EC86C7FC30711,
	ListItemDwell_Awake_m920339120548A256F675E5EBA460ED62BF033E2C,
	ListItemDwell_Update_m5663E17AFABCCDDC895EB45CFA19E134AAF2B53A,
	ListItemDwell_DwellCompleted_m5B5121D3A3CE81586640096AE6AECDAFC7AB5933,
	ListItemDwell_ButtonExecute_mCE5D5793F81B0FA3E5901086BD890C46C1E83214,
	ListItemDwell__ctor_m3B090DE69C79E75F6843F7084CC87EFDD002185D,
	ToggleDwellSample_Update_mA0AC5EE22276C7A732470373820AD7DC6794A722,
	ToggleDwellSample_DwellIntended_m9A5CE0A95AC3B91BF966C10BA925392A61647E6C,
	ToggleDwellSample_DwellStarted_mD724801088A9B49F13B3194EC965B7FDB21889E1,
	ToggleDwellSample_DwellCanceled_mE888D66A5061485F06E4BCF7BC32F7FF40450A8F,
	ToggleDwellSample_DwellCompleted_m11BB146A8D26534BE015E9657D879EA0FB7C568E,
	ToggleDwellSample_ButtonExecute_mF10F16BFE4C3006A9C9450AC3E5B5B1CEA2FEF09,
	ToggleDwellSample__ctor_m78C4026657BE2B8EF319437F219206BE04F4945A,
	DemoSceneUnderstandingController_Start_m4B17A37DE3F2D508CE71E7157EFB5858D8DE1A72,
	DemoSceneUnderstandingController_OnEnable_m62D4ECA9AC80E65CBDA49E9C4C87008D15E695F8,
	DemoSceneUnderstandingController_OnDisable_mE0875C4DEB026A2F8EBA297A467F359C01732377,
	DemoSceneUnderstandingController_OnDestroy_m7B4DE4D879BCB3E79C8D7852AD28842919B43CA2,
	DemoSceneUnderstandingController_OnObservationAdded_m84010971D940DFE6E95431836A62A992ACAA4F70,
	DemoSceneUnderstandingController_OnObservationUpdated_m7F818491FD7C98EC618339AE440F336AD0E7551D,
	DemoSceneUnderstandingController_OnObservationRemoved_m1E046C51010DECB2363B9F3801847EC083612DC2,
	DemoSceneUnderstandingController_GetSceneObjectsOfType_mDF036B1CDEEF82C9633BBDF392FC99997C746AF8,
	DemoSceneUnderstandingController_UpdateScene_mE17222C56EA2D82BF65572D01FA3FC7176C8D51E,
	DemoSceneUnderstandingController_SaveScene_mFB615C0B6A54E752340E6554334330FAE8780006,
	DemoSceneUnderstandingController_ClearScene_m2ED847E5EE175C01C1EC6A3C45ABD03E9A6A1A64,
	DemoSceneUnderstandingController_ToggleAutoUpdate_m3C611B4B65EED1376DCB3AA6B9787CF4E6A7E22C,
	DemoSceneUnderstandingController_ToggleOcclusionMask_m04728E2761F48FDAB3B59BE8523EED1B21E4E0C3,
	DemoSceneUnderstandingController_ToggleGeneratePlanes_m23789FAB660733BD5E19D9D5FAD10E1B052254D0,
	DemoSceneUnderstandingController_ToggleGenerateMeshes_mE6A79BD78FB9E33DF50B6E57255841EDA685B6DD,
	DemoSceneUnderstandingController_ToggleFloors_m35A7E2F082F6E89F4919192411A7E9201B57EF9C,
	DemoSceneUnderstandingController_ToggleWalls_m00F44B7BE89E42988789504618B8F5BB3EABEA53,
	DemoSceneUnderstandingController_ToggleCeilings_m4BD71B44E4B7BFA27B52025AA186054CADCA8A47,
	DemoSceneUnderstandingController_TogglePlatforms_m222359C5DC9FB50A5D0A1BA71C5ADEC639D007D4,
	DemoSceneUnderstandingController_ToggleInferRegions_m8830563A5AD5879F8532180837342EF4B5BA78E3,
	DemoSceneUnderstandingController_ToggleWorld_m3E9F262334FAAF1A791A12A31592ED7E8981BABF,
	DemoSceneUnderstandingController_ToggleBackground_m1FFA7D3A89471BDDB797FEE1123E005B351F61B0,
	DemoSceneUnderstandingController_ToggleCompletelyInferred_m43CBACE73783B42845163FA9DF489589F2DB6474,
	DemoSceneUnderstandingController_InitToggleButtonState_m7F626A4293E8BDEB74CA31A2EECF39F31B535D49,
	DemoSceneUnderstandingController_ColorForSurfaceType_m7F0126FD9BA9B2A92F6EFAEAD93C9C84F4A02463,
	DemoSceneUnderstandingController_ClearAndUpdateObserver_m5FEFE0F4BB79B9FD5A182483A9650FE26258C5B1,
	DemoSceneUnderstandingController_ToggleObservedSurfaceType_mF295356D81B81233E36A180B6D7431EB017A0E8D,
	DemoSceneUnderstandingController__ctor_m0A4DBA9966AB236B2D41CAAF71FE9E4ED6589EC4,
	KeyboardTest_OnPointerDown_m0839A6FB3F723C3C2F3DF7FBB485DBD8146F264A,
	KeyboardTest_UpdateText_m38415F169A69BC935F5237D3168DFFA74E084C27,
	KeyboardTest_DisableKeyboard_mAD37A7B9E6D0EF790B01F9E0006C1EEB46511D08,
	KeyboardTest__ctor_m0D784D016FBB427DEEEF6AA9F8F448863947247A,
	JoystickSliders_Start_m70D17A04A3219B9D023FEF30E7358FBE6C42E9C7,
	JoystickSliders_CalculateValues_m5EE7E7E2344C15F497110F2A9E487AAE76B0EF78,
	JoystickSliders_UpdateSliderValues_m9186287AB06D884A08BC8D820E6C64BA5FE27E3D,
	JoystickSliders__ctor_m31DD8D30473443BEE176D3A944E7092F8BEB9EBC,
	HideOnDevice_Start_m67317CBC5E1DADFBA583D4F417DFB10E9A339CEB,
	HideOnDevice__ctor_m9DAF3310FFE059D3CF8DDEA3FE739838CB35B1B1,
	GestureTester_OnEnable_m9AADD8392DD6B7F1426E64893B3F4BAC46A4BC07,
	GestureTester_OnGestureStarted_mCF1BE4F1FF22393623C32F1B0E6AC54F91BE297E,
	GestureTester_OnGestureUpdated_mF46446A8F728105F119FB700455D39E512163CD4,
	GestureTester_OnGestureUpdated_m87C7C00F3CDF1CD9EEB1ADE6FB76213503ACB3CB,
	GestureTester_OnGestureCompleted_m4666AFD1E4F059552A9D39D985F71F8E84F8E0CE,
	GestureTester_OnGestureCompleted_m7C0B5A3C601B167666063099914A8F2E918A68AC,
	GestureTester_OnGestureCanceled_mB0BAF7B49FCA4856600B4D0BABB7EDC6A04FEB3E,
	GestureTester_SetIndicator_mD5DDB1BCEE0E02E3B3F15978868F540017B13CE8,
	GestureTester_SetIndicator_m28F3812D5118DB72C0471412FFDA3AF75BE9BE01,
	GestureTester_ShowRails_mC8E3453E111E72C2E5D3A5154396B1B06C4837F8,
	GestureTester_HideRails_mF951FABE031588F25EEE7AFFAD0449CAA21650C1,
	GestureTester__ctor_m59B2BD41A6257BE9E46109314FDF03DBC13A5E22,
	GrabTouchExample_Awake_mF1A7CF657D2BF3845A6F5013CAAA2ED3B4871518,
	GrabTouchExample_OnInputDown_m4F2432C631F68039D2A4D58A9A191163F060FC1C,
	GrabTouchExample_OnInputUp_m3ABADEBABE435C956571436FA9DD6829B97206F3,
	GrabTouchExample_OnInputPressed_m22B8A5F55A79FB4B22740CE58C862D2207686EA6,
	GrabTouchExample_OnPositionInputChanged_mAFD365CD4F7BC572E1BB69E2BA93B7093B59902C,
	GrabTouchExample_OnTouchCompleted_m6A65545CD38255D5C8A0E8DD3958B1428FCF61EB,
	GrabTouchExample_OnTouchStarted_m8509F80B2EFE35C934A2BE96A4096EBAAA9AEFF9,
	GrabTouchExample_OnTouchUpdated_m602E0527A4C57151F74EA6CFA069228312CFAAF1,
	GrabTouchExample__ctor_m9EAD45E5FACBABD21087D76A284094AA74AF644B,
	LeapMotionOrientationDisplay__ctor_mB03351C717D53DF299B4F817B344562D94AC9315,
	RotateWithPan_OnEnable_m119D06CDEDB9960441C6A9EB52E0E3156526F6FC,
	RotateWithPan_OnDisable_mE66C37D891C63D16C57B9C42FB46DE6CCAE577D0,
	RotateWithPan_OnPanEnded_m5DCB7ADB80553546E0EB418824D1C193DB29F52E,
	RotateWithPan_OnPanning_mAAEE5025B70EA41A405EFE3E97AF94FAC9547827,
	RotateWithPan_OnPanStarted_m53911BBAB7CC74996FF05BB38F1CA5CF74CCCDF3,
	RotateWithPan__ctor_m75092EEC95C572006C07A392B333F4862347E21D,
	WidgetElasticDemo_Start_m6674F2EAD39D9E0D4AF6EC01D8923082C7E080CE,
	WidgetElasticDemo_Update_mA5A7CFCC2851CE05FE57128B69AAB20ECDE510C4,
	WidgetElasticDemo_ToggleInflate_m3943A9E8BE66A6D0D5CF6E1E91CF200466629947,
	WidgetElasticDemo_DeflateCoroutine_m7A6B7ABCEC5B8DA37508114CEE6AA833D01623BB,
	WidgetElasticDemo_InflateCoroutine_m531C85BAA6BF580ACB6A8739883D463E4663DAAD,
	WidgetElasticDemo__ctor_mB3E21BF8959B7ED3A272375BADEEBB793B0624CA,
	WidgetElasticDemo__cctor_m79261925C68C375709D216826C617477CFCE9BCE,
	U3CDeflateCoroutineU3Ed__17__ctor_m5DFBE929B124C6BF2853DE555D5EED0665E98DF7,
	U3CDeflateCoroutineU3Ed__17_System_IDisposable_Dispose_m9C136F49502B8941857512F103A71FC82CF1DC55,
	U3CDeflateCoroutineU3Ed__17_MoveNext_mBE4D780C9111C8E62B42F42031C7C0A8082CEB16,
	U3CDeflateCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54B77CAAA405C67DCDD618942DAB395F0E61E404,
	U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m9F18AABEB42C0AF9CB87D5AA40EFDBEB8DBEBE50,
	U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_m9602F0FA0E980A6B59E2E6AFFAC65C417D3F0438,
	U3CInflateCoroutineU3Ed__18__ctor_mDB85ED46DCB8408FD5640F914B6CFD808D4F5C6E,
	U3CInflateCoroutineU3Ed__18_System_IDisposable_Dispose_m53123F5C53E12D7B7E287E0A0E7121CD20637451,
	U3CInflateCoroutineU3Ed__18_MoveNext_m7DEF6D11C67B652DA43ECC61AA182EA3B60D2CCF,
	U3CInflateCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B98603EDEBD08B16E03B196AE45DA4F8C90FA18,
	U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_mB0E951E48F412D0E6B14ECBC6CF01C41457B40B5,
	U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_mB7A971F317E35D991918C1D40A27BA4B6B41E114,
	DialogExampleController_get_DialogPrefabLarge_m8FC664A339189AB78D7574D55DDE795ED3294F17,
	DialogExampleController_set_DialogPrefabLarge_m02063994FF1ED01FE783C99633D7A9222C787A99,
	DialogExampleController_get_DialogPrefabMedium_m9C5598B73A460A775FEA01085FC64D00426D782D,
	DialogExampleController_set_DialogPrefabMedium_mEF1C9F41E989D976F991A71942F3C9449E3AB5DA,
	DialogExampleController_get_DialogPrefabSmall_mB59482A7E606DAD8CF69B9A177304A2BDC55CAD8,
	DialogExampleController_set_DialogPrefabSmall_mEF0448A048EE6AD01E04B03FC1E517DDD1616FE7,
	DialogExampleController_OpenConfirmationDialogLarge_mC6356B5C7E14DC444417ECCDDB4A452EC7E793F2,
	DialogExampleController_OpenChoiceDialogLarge_m79ECC12F22BEA31A9EF5434478A56E27318F8D74,
	DialogExampleController_OpenConfirmationDialogMedium_m5FEA5A1E0E73AA8D1B4388BB83898D9FFAA085AF,
	DialogExampleController_OpenChoiceDialogMedium_mFC4D1F728B865873A9BFAB20B2FD7CC55714B9DD,
	DialogExampleController_OpenConfirmationDialogSmall_m78866AEC7AF530584A4E624583050970CAF44D69,
	DialogExampleController_OpenChoiceDialogSmall_mB6658E027EE8DC1C789A82C94FC6A0751617ED79,
	DialogExampleController_OnClosedDialogEvent_mFCA1C13D6EEF04C1204A52E0129CA6CFFBDBBB59,
	DialogExampleController__ctor_m3BC8BC7D42744DF5E24372839FBE629D789BD17B,
	BoundaryVisualizationDemo_Awake_m2A779C57EE3A389E1600C176FBA1CF8404A25C93,
	BoundaryVisualizationDemo_Start_m83206DA28C5AA3F03645C62BB40CD5E41FAC98CB,
	BoundaryVisualizationDemo_Update_m0C5779043B7681AA15F2188B330FE21A79CB2700,
	BoundaryVisualizationDemo_OnEnable_mCD089D3906774A57D8F7DD860DBA64A4FF55894D,
	BoundaryVisualizationDemo_OnDisable_m28EE46D607457BA30CDD784F7DD61216406CFA5A,
	BoundaryVisualizationDemo_OnBoundaryVisualizationChanged_mF0A72330C0785EA3C594B14ED6A62F08222E3A72,
	BoundaryVisualizationDemo_AddMarkers_m60BA0DB0C980993299BFAEDDE47C5BF6F1239DBB,
	BoundaryVisualizationDemo__ctor_m3EE3DA09C389EC0C132EDFB40248828031E4A525,
	DebugTextOutput_SetTextWithTimestamp_m62F3F7E77B050521F6378455ED8784C44FB82278,
	DebugTextOutput__ctor_mC1F2AC69FFEC0E69567A3D525D2FA49E9E28B3A9,
	DemoTouchButton_Awake_m83BAA3AAE898955C5D1A805ED49145A6B28BE6E0,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m022BD52F7B9375F565C9819C36F4E99FD7423588,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mB4D0A6210C60B243DAE610CEF15FC8D9B64F1A7A,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m12F427DE21A0E72EBFFDA28770B73E72373503C4,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m13D7ABD2DD39019470E32028659BBD8476190E20,
	DemoTouchButton__ctor_mF7D651379967BF470D6AE876C18D80D4594F6323,
	HandInteractionTouch_Start_mC4C9FE251E5C7099CFFF986F81A7DD8CB8DAD8B7,
	HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchCompleted_mB132E11E21B6CA0B22CF0BE1176C396D07275B06,
	HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchStarted_mCC8AF20A81FD38F193253887392F484612DDEB48,
	HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_mB2961B642338C4BAF9AFCBA197011B8B4DB2E116,
	HandInteractionTouch__ctor_m2BAF73805C7E05C4BA70E87DE0C43EC77D38E7BC,
	HandInteractionTouchRotate_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_mAC47B7F4FFEB871D99BAB41BD24F1070EC7C394A,
	HandInteractionTouchRotate__ctor_mD76C9936325A4491C17BA2317A07E0958ED9E6C8,
	LaunchUri_Launch_m3288F46EF18446AE7EFBCC5D012A4E7BB11BB43C,
	LaunchUri__ctor_m7FA1E45963920ECD98D27F0CCF13FE265224C265,
	LeapCoreAssetsDetector_Start_m23798D463B5CC0D836ED7BB4C22BD8A4149AD97A,
	LeapCoreAssetsDetector__ctor_m16AB1C5D1007014CC9E23A0F530275A969A1E15A,
	SolverTrackedTargetType_ChangeTrackedTargetTypeHead_m26CDA8F1F58F139EBB2950BF4FEC98CC4A2CDAFA,
	SolverTrackedTargetType_ChangeTrackedTargetTypeHandJoint_m5053FCEF017B9D73200DDB0FDF2886AB46A5A1A0,
	SolverTrackedTargetType__ctor_m53B326829C5A2A45B5E608A12A47908BF01415A4,
	SystemKeyboardExample_OpenSystemKeyboard_m9BD2817D17BAEB8FE0C5D35EDF4947240329A042,
	SystemKeyboardExample_Start_m6B4779381CC80DC830F3C76406E75A3150A0564B,
	SystemKeyboardExample_Update_mEBC6768FDE887113E9C86D9EA09A2C9D1E851F23,
	SystemKeyboardExample__ctor_mE3E324EF9F9159533B8AEDE89FA1F02216CC227D,
	SystemKeyboardExample_U3CStartU3Eb__5_0_mBBC953AE9A3048F737E4E9F5EA6DFBFB520CA5B0,
	SystemKeyboardExample_U3CStartU3Eb__5_1_m2FA208FBE3F7D0001E43C0384E3C6B4363B19B26,
	TetheredPlacement_get_DistanceThreshold_m69455E3940DBC306E5D7B92D3C3BC9FC6F7E3264,
	TetheredPlacement_set_DistanceThreshold_m5524A2BBB89E21EC7B39D181562C28C6BE897169,
	TetheredPlacement_Start_mEB6C4989B61651853D9AE6259765F5C67BD16092,
	TetheredPlacement_LateUpdate_m9BBC1F0262D9430F60EB2353CB33BE36CC82ECB2,
	TetheredPlacement_LockSpawnPoint_m0F437637778DE779C046B8450C184B98D80DB230,
	TetheredPlacement__ctor_mB4266A14A1AC6DB8282FC58FAA85C86EF5A45F25,
	ToggleBoundingBox_Awake_mCB0371F1DF5F44D3FAB2417C925A337275BA9D2B,
	ToggleBoundingBox_ToggleBoundingBoxActiveState_mA19D26CE4211EAFA2C03278F28F4AA21F09F69A4,
	ToggleBoundingBox__ctor_m3F71A30BF46D15049AAFE21F2547C66B1CDBE898,
	DisablePointersExample_Start_mE95C8E48999035201BCC2E286BFB8293BC184D27,
	DisablePointersExample_ResetExample_m6BB2007914FA4A1BCA89649385223BFCBD289BAA,
	DisablePointersExample_Update_mE7086EDDD3BB7043A1BF51E486D27C64D21545E7,
	NULL,
	DisablePointersExample__ctor_m38D914A24180CA1F2527CC9A1F15A4AEBF19F8B8,
	Rotator_Rotate_m9C30728AA6DDBB6414F95AD3C2F4EED9BB56F52F,
	Rotator__ctor_mDC4DCF4B852F99E46D355FF0F80732352E4593AB,
	InputDataExample_Update_m42D3EDD3EA92C407C6EAAA9C8C44A12EE5026A90,
	InputDataExample_Start_m2BE6957A24DB43264CF4BC5DF4AEA2B219D8CC35,
	InputDataExample__ctor_mE2D52779029ADD0152A1ECD4973935F9F8371B60,
	InputDataExampleGizmo_SetIsDataAvailable_m7A9DF9F25D6D16DD610E5FA495D325626C41CA76,
	InputDataExampleGizmo_Update_m5FEDB21DF2A5124A80A44A77E00DAA7920C57694,
	InputDataExampleGizmo__ctor_mB0E72B20C8C8833255B0E6330E8EE1B83DD2C9BA,
	SpawnOnPointerEvent_Spawn_m28A42E905B34B797BEDB1276B50EE00C790BD76F,
	SpawnOnPointerEvent__ctor_mF4501DBA5DCB61B7BDF97C252644013AAC1BB242,
	PrimaryPointerHandlerExample_OnEnable_m373469C4B43BEA03EC91C4F85F846A0CA2557D44,
	PrimaryPointerHandlerExample_OnPrimaryPointerChanged_m056DBFB929005ED8B7AB9276FE9D02B0048A87E6,
	PrimaryPointerHandlerExample_OnDisable_mE71162D75DF5352D9265B07651786DEE8E868017,
	PrimaryPointerHandlerExample__ctor_mF6CC650501FDA49A11D19F35C0B15E6E8B623E0B,
	ScrollableListPopulator_get_ScrollView_mE4359C6503DA8D9D908AE808BDBD39F6CF6EC165,
	ScrollableListPopulator_set_ScrollView_m111BE64C0713209CB73285869CC54F00F0529665,
	ScrollableListPopulator_get_DynamicItem_m0272AD2829DCE59728C68F4FC4FD16D75512235C,
	ScrollableListPopulator_set_DynamicItem_m47A499A149076520900F0FD0C9C266AE067C0446,
	ScrollableListPopulator_get_NumItems_mEFA4395AEC9A2C322031AB38E6DDBE5F5F2204C9,
	ScrollableListPopulator_set_NumItems_mCACE6E5E3AFF966F7E544AEB7EEE74248E50EEB6,
	ScrollableListPopulator_get_LazyLoad_m94D32263AA1ED4A7CD448885FCFE29ECDA7D7024,
	ScrollableListPopulator_set_LazyLoad_m706E6B3D480CEC9B14763AB17E3F974C1B8DD1BB,
	ScrollableListPopulator_get_ItemsPerFrame_m00817C2F73473CA49268B1DB02C1B632393DEDC2,
	ScrollableListPopulator_set_ItemsPerFrame_m28A9FD7D6B490D47906F9AEAA120E369F24156F2,
	ScrollableListPopulator_get_Loader_mB607C13954D90666E01D6CDF98E69B49ACD94B32,
	ScrollableListPopulator_set_Loader_mC3348F1248E6B894151EEF0D2A70A858E7960990,
	ScrollableListPopulator_OnEnable_m08DD662D16477AFA757FC7AFF3D5BFE977A34662,
	ScrollableListPopulator_MakeScrollingList_m4E8AF1CB34021233F1D733A596F785B1FA35AD01,
	ScrollableListPopulator_UpdateListOverTime_m32ED0D3125568A81E660F540892F08F316978C03,
	ScrollableListPopulator_MakeItem_mF02B39783E169D75E4C1128208568C694B0F90C9,
	ScrollableListPopulator__ctor_m8FDE50C9FD64C5CC70ADBC38DD02824965B7648D,
	U3CUpdateListOverTimeU3Ed__33__ctor_mACF3D61CE3F2A9BB63D04365DFEB3FBA33006A8E,
	U3CUpdateListOverTimeU3Ed__33_System_IDisposable_Dispose_m776D3F20FCEE5A4C9AB735AC719CDC73A3286A58,
	U3CUpdateListOverTimeU3Ed__33_MoveNext_mEA913295CA5C6568754B5A6D6AAD232F39AF9675,
	U3CUpdateListOverTimeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD4405FFF14D4D5888134AE62DB8E397895FFFDAC,
	U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_Reset_m7F8F79192E94B0404CF67B7821AAD3A2BFDA198A,
	U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_get_Current_mF8BFCF66510179224DDAD81021FCD10349DF4ABB,
	ScrollablePagination_get_ScrollView_m33BDA7C3EE18158EA0BB51C08CC9BE6ED40D552B,
	ScrollablePagination_set_ScrollView_mE6F7D99CF48610748F72EFC7239C3BDE005D4FB1,
	ScrollablePagination_ScrollByTier_mD2F6CAB9042505021A68A4F9341AE62A1E128D50,
	ScrollablePagination__ctor_m2BAE76DB581ED8BB855CACEB369E170421CABA67,
	HideTapToPlaceLabel_Start_mCC773ED90C0381AB7541551FB618A5E0E03293EA,
	HideTapToPlaceLabel_AddTapToPlaceListeners_mC16C31B8CE4C898FEE72D7A2B9AA153413FF31FF,
	HideTapToPlaceLabel__ctor_m01E75563A70E3C414DE5B5159FDBE5224361BF0E,
	HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_0_mA9B253B46801DD948048DE6952B66562AA3FD6F4,
	HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_1_m104568A79D4F1B5C5D6D483CEC339552D74AE3D0,
	SolverExampleManager_get_TrackedType_mE352E99180AD07F9BD7C10FAE1A96765517B60A3,
	SolverExampleManager_set_TrackedType_mA6B36A1455DF0BBABC4C5DD4FAA4736229FB3D71,
	SolverExampleManager_Awake_m45882BF5F2ED694554C4366071C5DE088C2DB73B,
	SolverExampleManager_SetTrackedHead_m2E7F32F58366D84BFB5D4E77E56452904B5340BC,
	SolverExampleManager_SetTrackedController_mAFAF0B064B39DBF178D595CFF5BFE3E9C9A99434,
	SolverExampleManager_SetTrackedHands_m0056AD29B22AA74401C34031CF2B5BBA60155971,
	SolverExampleManager_SetTrackedCustom_m415CCF839F500B28529B15321865B57ED98652D6,
	SolverExampleManager_SetRadialView_m3C66B3B33501F01ACD9F015A658977E39EFD2B90,
	SolverExampleManager_SetOrbital_m557A359F1A2CC5B55066062A3F6A217515A22477,
	SolverExampleManager_SetSurfaceMagnetism_mB3BF53B2219E1028997E2528FD3372333F62725F,
	NULL,
	SolverExampleManager_RefreshSolverHandler_mE22A621F894F2A36560C99AE3DEFA06E4457C632,
	SolverExampleManager_DestroySolver_mB3C862B5A8492B236D5AFCC0B59F42CCDE1E0BFC,
	SolverExampleManager__ctor_m323026103252B8ADEBAAD5D83CD179B7F8725225,
	ClearSpatialObservations_ToggleObservers_mCBF8F34B48824CECE007EAED66B739A21FCA3D97,
	ClearSpatialObservations__ctor_m7F145FA493156190DF93C07A9D7E3A77F238857B,
	ShowPlaneFindingInstructions_Start_mEC65BE9F96543B1D2242318482CFD33811D9608F,
	ShowPlaneFindingInstructions__ctor_m43706941098FB18156D44C75BFDC7FCF10412B99,
	MixedRealityCapabilityDemo_Start_m13FC8144EB56CD9B42115726843616A2D15596B7,
	MixedRealityCapabilityDemo__ctor_mEF59E783399C76E039623CAC4C96391B80B0EDFB,
	BoundingBoxExampleTest_OnEnable_m65D4644D563AB28D1C2C7B3DF566353A85276F42,
	BoundingBoxExampleTest_OnDisable_mE5B2A1090E831F707BF7FA2CB96FE5171D4C0770,
	BoundingBoxExampleTest_Start_mD1B2522A85F1B5148511B145F68525B00B9AFFE8,
	BoundingBoxExampleTest_SetStatus_mAB092B537F3279C2763D5CDEA8581C77AA6C9853,
	BoundingBoxExampleTest_Sequence_m52B3C449C585EB03563F40D49531BB49F96F31DB,
	BoundingBoxExampleTest_WaitForSpeechCommand_mEF5751F616E21567794FCB92397AA9BC67EBAFD9,
	BoundingBoxExampleTest_OnSpeechKeywordRecognized_m222463231C0659533D4B9EAF6431BB38292E4798,
	BoundingBoxExampleTest__ctor_m6C5C3EC8C9674650FD2FF831805C59C4871BEE93,
	BoundingBoxExampleTest_U3CSequenceU3Eb__12_0_mFF939D0D67AA1DF5F1F85FE83E45D4FCDF6278C1,
	BoundingBoxExampleTest_U3CSequenceU3Eb__12_1_m0575870681F4EA0D248901AB93FDD48BB8F56280,
	U3CSequenceU3Ed__12__ctor_m7D5B9C81C79BDFDA12B9C2630156D85048EC8CF3,
	U3CSequenceU3Ed__12_System_IDisposable_Dispose_m4E214E8D6DCF7575C01A264A47D3B88F9E77851C,
	U3CSequenceU3Ed__12_MoveNext_m255898E02B9A76DA875999DFBB658DB7B5834114,
	U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5FE29693EF5BB291C1BACB0F7950360E6F45AAC9,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_mA28B956659A382EA5DF3C7E8EC322CB548EB49FC,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_m2FF6E0ECCBA04C71AD0BB20D0FFFCC605D72CFAF,
	U3CWaitForSpeechCommandU3Ed__13__ctor_m6FB0854046E56A3E0B22E796BCD6EED6EF8CF782,
	U3CWaitForSpeechCommandU3Ed__13_System_IDisposable_Dispose_m283D88A6B0AD0FAE77E4453D509E33A61409117D,
	U3CWaitForSpeechCommandU3Ed__13_MoveNext_m4EC32F7EDA8014CBFBA9F3864E08E4588A143277,
	U3CWaitForSpeechCommandU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B3A3044C25185F3AEFF5BE7B1480F5BF0727C64,
	U3CWaitForSpeechCommandU3Ed__13_System_Collections_IEnumerator_Reset_m776239BEEA992B4EC07BF4820B48DF308E9BD601,
	U3CWaitForSpeechCommandU3Ed__13_System_Collections_IEnumerator_get_Current_m3C65E81450D102BAB16E193330897F48B3A9AE17,
	BoundsControlRuntimeExample_OnEnable_m5FEE3602036C62349B9537E4F7A3550ADA2FEF79,
	BoundsControlRuntimeExample_OnDisable_m2D717D12A295402AC6580E2787AFEFE606C78A61,
	BoundsControlRuntimeExample_Start_m4984E137FCE0C1F2433910514EB8A60BBB636481,
	BoundsControlRuntimeExample_SetStatus_m00973213807CC01E95D059DF76F050A3E0E0D646,
	BoundsControlRuntimeExample_Sequence_mA8BB9C4271B68A0FE6082F3C9398AE11ABE3AE0F,
	BoundsControlRuntimeExample_WaitForSpeechCommand_m9FF69ADCD703DF08B2BBAF0F5AEF44F2932B2A5E,
	BoundsControlRuntimeExample_OnSpeechKeywordRecognized_mD75A366084299899AD3FCF16A4A47F21174957BB,
	BoundsControlRuntimeExample__ctor_m7D81D7B40FB28D1F54B1678FE948BE6ED1D3C43D,
	BoundsControlRuntimeExample_U3CSequenceU3Eb__12_0_m56CBCF1CCB44BE9BF323DBE554BE7741A60EFA89,
	BoundsControlRuntimeExample_U3CSequenceU3Eb__12_1_mEEDE0BEB4314834E65950313E42394FE4FE55402,
	U3CSequenceU3Ed__12__ctor_mEE411CE274DF4BBEFA671D5E4915AA11AD47EA2E,
	U3CSequenceU3Ed__12_System_IDisposable_Dispose_m842C6733D840A95DDBE0C3946E5A79EB8A0DE880,
	U3CSequenceU3Ed__12_MoveNext_mEC2CF8A264CC326E6EEED4581E3935DC2879DE75,
	U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA082D74DD5FA1A0A6FABB4726EC3E5F2E22122E,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m12DEA3D25C8E2D2B8DE6CFDCE72FA72030F3DD15,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_m04CCB53596226B302B9129FCB774C6A9F2171C47,
	U3CWaitForSpeechCommandU3Ed__13__ctor_m7D65D5CDE2156C2AD5B4FB7E9B352CDD5A8EF794,
	U3CWaitForSpeechCommandU3Ed__13_System_IDisposable_Dispose_m00C33208D9802F3CAEB3B9C28D77A965D3057B81,
	U3CWaitForSpeechCommandU3Ed__13_MoveNext_m5775500DE49A84D509EC4A786B552B6C1EB630D5,
	U3CWaitForSpeechCommandU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19889DA87AF7EE3277D4E7AF0B6CA4A87FE82DFA,
	U3CWaitForSpeechCommandU3Ed__13_System_Collections_IEnumerator_Reset_mACA03EFCCE267FDE046277176D400C200D8E97C8,
	U3CWaitForSpeechCommandU3Ed__13_System_Collections_IEnumerator_get_Current_mBF8E18BCB26595D70BE4393A9F47903A7A2ABE92,
	GridObjectLayoutControl_NextLayout_m63F33E41D821EEAF99FE89416D133368DAFDA796,
	GridObjectLayoutControl_PreviousLayout_m8833FFB21CFC47001DFAF3CF1F5F2DCF1309D256,
	GridObjectLayoutControl_RunTest_mF5B8DC192FE845D4074630DC2185AF0E0860D663,
	GridObjectLayoutControl_Start_m1A140D0FCD14E5F76D2DCF9209D4311BFB2F42A3,
	GridObjectLayoutControl_UpdateUI_m92C6BA426052AC25EB90F414934D11D4CC289B37,
	GridObjectLayoutControl_TestAnchors_m460F0482E8D73A8E9749B93D35D1B8734D88825F,
	GridObjectLayoutControl_PrintGrid_mE315C6E513E6E21AB49DDA56E9CE753DB53D39BA,
	GridObjectLayoutControl__ctor_m34100C6FE9DFD6658FD3705FEDC8FB2A83F72D5F,
	U3CTestAnchorsU3Ed__7__ctor_mD8981BD02A905CEAC06D4E553E23F2BFE65F342F,
	U3CTestAnchorsU3Ed__7_System_IDisposable_Dispose_m9AE8C2217C68186641F8D54E874B247AD4745E5F,
	U3CTestAnchorsU3Ed__7_MoveNext_m5C729D7AD04897E69B72817DE63A876D32DC28D1,
	U3CTestAnchorsU3Ed__7_U3CU3Em__Finally1_mE68AF14C40C20BB79882F1BC1C1AEF1E272FFD8B,
	U3CTestAnchorsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51B67DA947B38655EA84BBB181DD7D3963D89AE5,
	U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_Reset_m2B037080877E9B04CEE6A653552909A5E84AD6F6,
	U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_get_Current_m6F94688A43879EBEBFFB8F5AFD784F5983F77EAA,
	ChangeManipulation_Start_mD91FF29A289DCAD9C6A28F127D682E146985F211,
	ChangeManipulation_Update_m777E8F752BDF582DE203BAB159385964389C66B2,
	ChangeManipulation_TryStopManipulation_m750D6248316C1A1223BDC8E27529437B2CA8F28C,
	ChangeManipulation__ctor_m6270F997C7154555604B8ED7F6D82969AD1E8B4A,
	ReturnToBounds_get_FrontBounds_mB457BE973F0D448D74242EE9755157B225668ED1,
	ReturnToBounds_set_FrontBounds_m555C5617E7CBECB7F45ED637388F0D50FFC7179F,
	ReturnToBounds_get_BackBounds_mB42CE86FF3653B03A925133CD8B217A2A8CE9507,
	ReturnToBounds_set_BackBounds_m7D68CDBB58F3858F9BD97FF0DFC5228DB11B1E19,
	ReturnToBounds_get_LeftBounds_m6E4FCE9513A5DE720BC582E8F4643978AF536530,
	ReturnToBounds_set_LeftBounds_mD3DB7047DC4D610B07D55B2442820818D1905BB1,
	ReturnToBounds_get_RightBounds_mA048A83709507E2ABD9CC8FFABB3BFF7C29BF727,
	ReturnToBounds_set_RightBounds_m60BFDC1C4E97FC752A7F2E6E4C2C5B7855672F0C,
	ReturnToBounds_get_BottomBounds_m070E85502D0CF39ACA58234F68FF4520A3A1CC22,
	ReturnToBounds_set_BottomBounds_mB81669774276DB28C307EE90B7CD3F059232B86F,
	ReturnToBounds_get_TopBounds_mC93B14634A492562AA098E6C89EF8C24241FF603,
	ReturnToBounds_set_TopBounds_m7C1A2A9216A2CCFB55A70C3F2A39FBD635B48FCA,
	ReturnToBounds_Start_mC403C2B3F83C8DD47D4A2B7B9FC9DF635AE7E69E,
	ReturnToBounds_Update_m601D4D3AF13BC46BE59524D62DFC990FDDE95A66,
	ReturnToBounds__ctor_mCE711B7D1384A0B7F146CC5B326940C0D3EE718E,
	NULL,
	ProgressIndicatorDemo_OnClickAsyncMethod_m4AFC89EC4E9860391D35149A26FBF2077B425CA2,
	ProgressIndicatorDemo_OnClickAnimation_m8F9A6B01C33501EB349FFB2D08F3E3F7E6B39497,
	ProgressIndicatorDemo_OnClickSceneLoad_mE17447FE149FA998570CE3F01D895AB8F1ED7AA2,
	ProgressIndicatorDemo_HandleButtonClick_m546F9C1E1AC95322A60BF294E495858C2D3113EE,
	ProgressIndicatorDemo_Update_m8405DC4D5740E287CFE059E8F16E0574955D1EAA,
	ProgressIndicatorDemo__ctor_mF2BC513AA5E94F646D8674BD176D2FA3BD2DB39E,
	ProgressIndicatorDemoAnimation_Awake_m4DD8C7D5A1F6184006B8962C4E4B4EEE242121EB,
	ProgressIndicatorDemoAnimation_StartProgressBehavior_m5F5E6F76E2291F2ECAD20EB4B22484B993A4BD1C,
	ProgressIndicatorDemoAnimation__ctor_m78C8F5F9EE7AAD21AC90746925CAE8DAA9C92AC8,
	U3CStartProgressBehaviorU3Ed__5_MoveNext_m843CD393E4AEA0499AC8C62E669E7FE3FE902BFB,
	U3CStartProgressBehaviorU3Ed__5_SetStateMachine_m96AD4A4F814E2FF54ADAC5BE1C8586F426D9754A,
	ProgressIndicatorDemoAsyncMethod_Awake_m894FECFBA9C55ED31EBDD33159891A33E6954072,
	ProgressIndicatorDemoAsyncMethod_OnDisable_m7A25E3EEA0BAD28C269F60FD27EC0D745819DCB8,
	ProgressIndicatorDemoAsyncMethod_StartProgressBehavior_m0CFCCE8B6DC603BD6CAEBC586D456EE5870DFCA5,
	ProgressIndicatorDemoAsyncMethod_AsyncMethod_m55E60A89B6C77FA28494E3316906DB72B7280012,
	ProgressIndicatorDemoAsyncMethod__ctor_m8F956C74FAF9AE455670C10DB108DC62AFE4C8BB,
	U3CStartProgressBehaviorU3Ed__8_MoveNext_m62CDA140A7821419665E435C3EA5D8FB59D572AF,
	U3CStartProgressBehaviorU3Ed__8_SetStateMachine_m19D69CC62B297B3B0B8BAD06E33CEDF96E276168,
	U3CAsyncMethodU3Ed__9_MoveNext_m575F58B2568865FC9699CCBF4B5BA4F73A3416DE,
	U3CAsyncMethodU3Ed__9_SetStateMachine_m0424443862F055A35A9F8D61AAD3A0CD2E1D4270,
	ProgressIndicatorDemoSceneLoad_StartProgressBehavior_m47BF66F004A24A7A77451856F494A855F6C3CEBC,
	ProgressIndicatorDemoSceneLoad__ctor_m47A8BA173E362F1D674F7DB2F88330F812FD416A,
	U3CStartProgressBehaviorU3Ed__4_MoveNext_m9A41723CF8D8E4172E0C693C410C7CA572BA0F70,
	U3CStartProgressBehaviorU3Ed__4_SetStateMachine_m741306E6BDBB3CB825E7EDD5CD8966E7E1C2E19B,
	SliderLunarLander_OnSliderUpdated_m7CE53650B5473FADC95CA219242762930222F023,
	SliderLunarLander__ctor_mC189F20CF4ACA20A6B80B2B0C52AE4668238D23E,
	ReadingModeSceneBehavior_Update_m8C51B3FCD16F0BC0F8B6D6550D4D44CEB0915537,
	ReadingModeSceneBehavior_EnableReadingMode_m4A6DFB4C1ED3A851C196B7E4DBC6637C56CDED23,
	ReadingModeSceneBehavior_DisableReadingMode_mA4701614B9E60F72691B3DC46A1FF640A1CF4E40,
	ReadingModeSceneBehavior__ctor_mACCAF8EB527251E4ADC7242AE5A3E132C6C8FBFF,
	ColorTap_Awake_m84412C8FBF5AFD3119FD285E70B5AEA62E607756,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m3DA2781147C846F573869FAAAE1AA006AE1A7307,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_mAC3B9A3842F6747B6E47C72C183A2905AABAE1A2,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m90239C3F08F0D548AEE61E1484A0C9973BCA87B1,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mC567C24B2A722B648C7E7DA8AB3CF6CB56399B45,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m27A9C3FC647EDAB1B94BCE967CB7367E289F0EB8,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m1192E6701B81AE0D2F93312DD8C1AB074B1CF0E4,
	ColorTap__ctor_mBDC9ACBB0F08D97576451039B760776F1554424A,
	FollowEyeGazeGazeProvider_Update_m98D68F320BF5BB163F91EFEBD99B3BACE990FB00,
	FollowEyeGazeGazeProvider__ctor_m661224A5B8E28A46EC1DDB3853E48312FE650298,
	PanZoomBase_get_EyeSaccadeProvider_m3248858ECEBA0E4B0CE0A904111BE03ECAF6D5E2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PanZoomBase_Start_m8A04E039C1CE199E7A16C6ABA900908FCD5A35C1,
	PanZoomBase_get_CustomColliderSizeOnLookAt_m72D050A963DFFB77A3BA3A8CFA5D601D3AF22EB5,
	PanZoomBase_AutoPan_m164E573B0A35591F0407FF2EED6465113523C7C5,
	PanZoomBase_PanHorizontally_mBD47406466E6D91DE52246502349272BB2383C94,
	PanZoomBase_PanVertically_m134E1CA9477495F67D6A92DF07567C5451A3130A,
	PanZoomBase_EnableHandZoom_m660373CC07370139C9B024362A9DA4502E75BBBD,
	PanZoomBase_DisableHandZoom_m33D24D81C7FB05C8C6FBEA8F68BB0173BCEC5D7C,
	PanZoomBase_ZoomStart_m5B5D13DB90F747DE0C8475894938D95D28621FE5,
	PanZoomBase_ZoomInStart_mA7A44A250CAC114F416C2A69F3C28E7DCB725053,
	PanZoomBase_ZoomOutStart_m41D8D9B0FBF580B75027AD5EFB9C23E135737598,
	PanZoomBase_ZoomStop_mFA8EA9B7C598D50DCF75E99F981F50545DD64768,
	PanZoomBase_NavigationStart_mEA9B866904719D251391E07A4D33C47CD69E8255,
	PanZoomBase_NavigationStop_mEC075B2C1784D78D7EAA89575DB11C268BAAF0C2,
	PanZoomBase_NavigationUpdate_mDC40421228553D6B11B3DCF60669ADEAE9D23561,
	PanZoomBase_Update_m58E69D529DC18B0D9488C92E32BB4F2B2DF9EC19,
	PanZoomBase_SetSkimProofUpdateSpeed_m007168E7E69932022E434B4AD2A463281DF7C0AE,
	PanZoomBase_ResetNormFixator_mD2A4BF297EC011A4F0D908F5A0CE865D3844CC30,
	PanZoomBase_IncrementNormFixator_m045A103A640C1739F1F90B2BECD25EB802DAF4EE,
	PanZoomBase_ResetScroll_OnSaccade_mD4BA794815B32ADB255281886775240741280379,
	PanZoomBase_LateUpdate_m3EBF526CB2E72EAADAFA281F89CD97E33F61EDC3,
	PanZoomBase_AutomaticGazePanning_m502BA9E0F27A4BE4161D3A31F934B46A02672938,
	PanZoomBase_get_MyCollider_m194C5A33AEA0119F02AA7D7A4C2ADCCE91E65205,
	PanZoomBase_set_MyCollider_m3601EA1650AC30987C3886D978221D11D1C26521,
	PanZoomBase_PanUpDown_mF42BE0F2FC509917C4D3D102122DEE0F52DBF636,
	PanZoomBase_PanLeftRight_m3820E1407101D5481B1A953D05E02082256732D6,
	PanZoomBase_UpdateZoom_mD1E991986CC47502A40D7ED13412EDFE3FC080E3,
	PanZoomBase_LimitScaling_mFF90B882CCD8D695E5C7C1091BA9E2EC03A5FEF8,
	PanZoomBase_ZoomIn_Timed_mAF4A3A7C094AFC9D1B093761743513125D5816A3,
	PanZoomBase_ZoomOut_Timed_mC51CB3EBB9CB0EAC6FBE35048EE6FE5F43CF1AD2,
	PanZoomBase_ZoomAndStop_m64A048F504DE97CB1F6A8BF71DF2A82DE3C5812C,
	NULL,
	PanZoomBase_StartFocusing_m5981DDA2897223BCD93D08C936F17DAF9BA1EBB6,
	PanZoomBase_StopFocusing_m19F65737E6882E737A04AD78A88882B0138411DF,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m59AA6B638526735A774D1F1FC018DAF6926EE522,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m0679F164BE197BA19E978FE7D9ADA9D7C3753E58,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m0C0FC4301B0443872BD11D19E4AE76CF8FE04162,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m24672ADBA0A69BB023F6305334BDC8175BF25E59,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m285A644A29521942D7F353D2D52E2424947A69F3,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_m334DCC053C7479237231FE85A70B640999062E08,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_mABCFE1E5856B66CFE3BD4F084E19145EF0AFED82,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m907D3655F053E13FC314372D75207004F8773A10,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_m2BBA97C2E0E331868E9F4ABF83F7B28C9E5EE2E9,
	PanZoomBase__ctor_m6A39E0307104CECE258D5A744D668DD9E81B33B8,
	U3CZoomAndStopU3Ed__78__ctor_m638B15B7B300137A28BD3DB389EA4E661A250DC9,
	U3CZoomAndStopU3Ed__78_System_IDisposable_Dispose_mC642298070681920CB57E9D3A6A3D79173510C9E,
	U3CZoomAndStopU3Ed__78_MoveNext_mD8F80AA1077677243A4DBE00A8AADE0EEE002E10,
	U3CZoomAndStopU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58F8EEAFC96F67745BD50AA736A98523ECE0A535,
	U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_Reset_mAF68F9A8D6BF0ECD2B6162431A766D1937FD9EF5,
	U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_get_Current_mFCDB5FCAB8ADE755CBB4761264D1D56CCCB4FBF3,
	PanZoomBaseRectTransf_get_IsValid_mF8F280F23EC8ADD0B492E4A1A6F00F80D237BC77,
	PanZoomBaseRectTransf_Initialize_m11A1FC45621BCE93106ED707278B1B89930E9588,
	PanZoomBaseRectTransf_ComputePanSpeed_m7D51253DC6497D441171CEB43C1DA109220B8919,
	PanZoomBaseRectTransf_ZoomDir_m75F5A5BE5D97693BFCEDEB3FF279CE796DF54346,
	PanZoomBaseRectTransf_ZoomIn_m87C54B437F84B3690EDC72DE1CA7E13E43D972E2,
	PanZoomBaseRectTransf_ZoomOut_mF874267338C487B1A6F92E58676000A6EDE916BB,
	PanZoomBaseRectTransf_UpdatePanZoom_m3D5B5008FD1429089ED9913186D9CC57B8962814,
	PanZoomBaseRectTransf_LimitPanning_m7667E59FEA84E5B603FC4F4260BFA806D17C7F4E,
	PanZoomBaseRectTransf_ZoomInOut_RectTransform_m671161DF1C1718D8BB5FCE719336C6ED42B566F9,
	PanZoomBaseRectTransf_UpdateCursorPosInHitBox_m67DDAA6E01BBC4CDB1AB284FEE3082110E5AF835,
	PanZoomBaseRectTransf__ctor_mCFC3693AC6F1EBD355C8C0CD6474008F814A36D7,
	PanZoomBaseTexture_get_TextureShaderProperty_m6E5B74418BC03CC7271A6431FD58282B44CE6956,
	PanZoomBaseTexture_set_TextureShaderProperty_mD84BCFC3CFCCC0319803FF2AB248063C7853C601,
	PanZoomBaseTexture_get_IsValid_m56A27D26611381EE3E67A10758CDEADFE1CE72B6,
	PanZoomBaseTexture_Initialize_m030ED873A5DDA37C756FECA2A2D80233B3AA5910,
	PanZoomBaseTexture_Initialize_mFC59DB01692EC496F6B17574F3C722A70D42090F,
	PanZoomBaseTexture_ComputePanSpeed_m462C0EBB76E42B45DC99E58DE2889DCED1A847E1,
	PanZoomBaseTexture_UpdatePanZoom_m104FE221E39C655CAA4F0293526EA5FB6B5A1A7A,
	PanZoomBaseTexture_ZoomDir_mB40612879DC4F642BDDEB1202210CA0006F4413D,
	PanZoomBaseTexture_ZoomIn_mA02349F004FD1145B0223B67FF865297BFF23E83,
	PanZoomBaseTexture_ZoomOut_m343D4B1072594247E257FDFED039286CF69B0ED9,
	PanZoomBaseTexture_ZoomInOut_m5945C81949933193484A76A6B377FD3682A008CC,
	PanZoomBaseTexture_UpdateCursorPosInHitBox_mBBB877DE821EA1F1E964ACD00D8A65D2BEEAB454,
	PanZoomBaseTexture__ctor_m50806B717B65CC5442881F6D0CEF4978D749F948,
	PanZoomRectTransf_Start_mAFBB206E442ACD8B39AA3A55E98C305AB74A5C9A,
	PanZoomRectTransf_Update_m773ECEE72AB88B979F79DD6591F4F04623C303CE,
	PanZoomRectTransf__ctor_mAC045A21DFBAA5059924D51C1435C65E02A04541,
	PanZoomTexture_Start_mBEBD156134214866BEF430A6EC0098A2547E9EBE,
	PanZoomTexture_Update_m24A0206949E90D6093F247DC891C13A2388B7273,
	PanZoomTexture__ctor_m683619F4430ED9453E37800A7AD323DBC5ED9E64,
	ScrollRectTransf_Start_m3131601857480EF1C51D93B0167C8B0EE62FE7A6,
	ScrollRectTransf_UpdatePivot_m294A9187F156FDB87D76E331517EE5C95ADA5F02,
	ScrollRectTransf_Update_m3AF072ACA3D721A9ED5BF1C736B34605E621C791,
	ScrollRectTransf__ctor_m0697E1957609F787592AF7A4CE6FCA227AF65C4B,
	ScrollTexture_Start_mE65D03492F1320B7692EFF5A1E9C0B6054F73C37,
	ScrollTexture_Update_m0B495C5D035159D88BEC0AD28898443788A5F08B,
	ScrollTexture__ctor_m597F8BB593059F31C25F817006642208CA04A488,
	TargetMoveToCamera_Start_m09F48A502C0C17D003C31300E23C63EB57B54406,
	TargetMoveToCamera_Update_m724772FDE9B7F3820AD117BAF3B86A002AF0766E,
	TargetMoveToCamera_OnEyeFocusStop_mD8398AE4AB348A962D1D6522AD3B37613EEA1585,
	TargetMoveToCamera_OnSelect_m613F9F0E2BD42F84EEC902C381D73EF3295B6418,
	TargetMoveToCamera_TransitionToUser_m49F812DE104D2407D3FA239A14A3898EAC6E5134,
	TargetMoveToCamera_ReturnHome_m48058DD46D16F5AEEC3817FB2110710466760FEE,
	TargetMoveToCamera_TransitionToCamera_m9EA4B4791F039882903950FBA21C510BD934636D,
	TargetMoveToCamera__ctor_m931AFEED9468462FA0D148FE896772C061A2A2C8,
	GrabReleaseDetector_Awake_m6C822802F211FB028EBE301323EF935ED067FFA0,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mCF7979FD72036AF5A73ED6D283C9EE9816A035C7,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m378F6F6544589A9A2F7F6A1E3B69F14D3619E6E5,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m241503EE2DFBA41EE1222C71D2DEAF2A9D659C8E,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mA50F54588E42F89A1DB70686D9753DF1C5E0D205,
	GrabReleaseDetector__ctor_m30242BEF7351AF2B2D6D3A329378A15081DEC51B,
	MoveObjByEyeGaze_get_EyeTrackingProvider_mF37265972A7CAE9B0BEC457B536398347647E49D,
	MoveObjByEyeGaze_get_ConstrX_mB214E0F5621B4046A14627DC8986E7E591DE4488,
	MoveObjByEyeGaze_get_ConstrY_mDB5F49C0CE06D0C351C904522803BBB0E2684B87,
	MoveObjByEyeGaze_get_ConstrZ_m1916F5DF7056488707FF7393883A501CEB37109F,
	MoveObjByEyeGaze_Start_mE82BCB194B8840814A71EF9ACFD6C382182ED6BB,
	MoveObjByEyeGaze_Update_mD8A3259DB66C90E343C6467CE8011A0A63F61B4A,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_m77DA24AC091BF23B0DD095E2EA7C1FEB2294271F,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_mAADECD6EF417FBF98FEAD7401317BD182246F55F,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_mB0B0D2CEA50ABDEAB7B63587E1649B96B4899D95,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m8DA6EF45FBD47F06123B35C75D3806AB8E84E694,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m0549A08BA45C3414F9F332ED57FC530836EA0267,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m663290EF79257B68BD27D7BA15B9B7516BDD01C9,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m9FCDB90E1B8BA321A2D5ED87692EC2BAC6779903,
	MoveObjByEyeGaze_HandDrag_Start_m05E4C1835EFFB52FAB2C6C913821645BC1EA811F,
	MoveObjByEyeGaze_HandDrag_Stop_mF69C155C38E3157AC026F69FD1A28E68F346E167,
	MoveObjByEyeGaze_IsLookingAwayFromTarget_mF8546DEF5DDF287E6C6593CDEFCAE487E3DCD36F,
	MoveObjByEyeGaze_IsLookingAwayFromPreview_m42B4CC3161813E5452C8FF5D9D4A3FBF87F8F3E6,
	MoveObjByEyeGaze_IsDestinationPlausible_mAE5798A7ED7F75ABD2374C4DADDE0A2D1242F1D1,
	MoveObjByEyeGaze_GetValidPlacemLocation_m18F9AD11FC6F549FCF57285398A10AA5C12BE34A,
	MoveObjByEyeGaze_ActivatePreview_m4B074C24B26862987B77CEC601DDAACA92E412CA,
	MoveObjByEyeGaze_DeactivatePreview_mBE16F4F1F0201560E6D11ED70C04B93305CCF572,
	MoveObjByEyeGaze_DragAndDrop_Start_m795A08E37CD00B8AD2CBAAC467544437510EE7D9,
	MoveObjByEyeGaze_DragAndDrop_Finish_mF4ABABFEE38277274757CD6DADD6DE3B5157399D,
	MoveObjByEyeGaze_RelativeMoveUpdate_mFD93941427E095EA404E32C06639724804B03305,
	MoveObjByEyeGaze_Angle_InitialGazeToCurrGazeDir_m43C8CB7A6D8713258D084737D98AE2F77BEBCB92,
	MoveObjByEyeGaze_Angle_ToCurrHitTarget_m73D866E7B2DE2FC43713298FEFCAB6A6D71215FF,
	MoveObjByEyeGaze_HeadIsInMotion_m5504CC98282A8D6D35F64EBE96EE1A38D1FEF6D5,
	MoveObjByEyeGaze_MoveTargetBy_m5D24F30549C9D06AF6635E485D0D7C8C9C626078,
	MoveObjByEyeGaze_UpdateSliderTextOutput_m68D38DCDABB561D6A52F36617A6F3FA4DE0B8B2B,
	MoveObjByEyeGaze_ConstrainMovement_m5D8922AE0963E977BC749B74A8291353BD9A9C6B,
	MoveObjByEyeGaze_OnDrop_SnapToClosestDecimal_m02874884CD9F731A2E65D9F85F709698CFB9ACA4,
	MoveObjByEyeGaze_MoveTargetTo_mB933B60F26C278576BCAA082C6B919E76AB323A5,
	MoveObjByEyeGaze_ShouldObjBeWarped_mE908CCB6AAC776C9FBB6110A063CBB809E5A7198,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mDEEA992415BDDE07DD1EE5DC8CEC6CFD3EA8D622,
	MoveObjByEyeGaze__ctor_m450B8E6D323C466B02DAA9CA23BF342E2A859978,
	SnapTo__ctor_m53C3DE3026638C6D83D68102558BB3DDB0F883C6,
	TransportToRespawnLocation_OnTriggerEnter_mB9103014B6FAC89FFC5C885DBC8E13ED9F1C61CB,
	TransportToRespawnLocation__ctor_m7540E5F653E2579F5106110992B11C0CA169E7F4,
	TriggerZonePlaceObjsWithin_Start_m06CCD6F04A5EEBB0B744BC16257E1E9D424EDE7B,
	TriggerZonePlaceObjsWithin_OnTriggerEnter_m8C757721883DDA4D778E6DC6031A61BFEDE8B745,
	TriggerZonePlaceObjsWithin_OnTriggerExit_mAEF8DDC8CD19B3DB6B3413D89C473EC5E6E33D19,
	TriggerZonePlaceObjsWithin_CheckForCompletion_m5BE3D58EA13D9079DA397F016A508C81258E2FF8,
	TriggerZonePlaceObjsWithin_TargetDropped_mFC7940378EBE2D3FE42699973270B9B5E14DB518,
	TriggerZonePlaceObjsWithin_AreWeThereYet_m3090ACB31FBB2DDA19AEDB816D2EB29B858078C3,
	TriggerZonePlaceObjsWithin__ctor_m1792410F3DC29504FFAE6FE23AA63B9CD378FC2A,
	HitBehaviorDestroyOnSelect_Start_m03982393F6BBF9DE650646E234DCAF7C61AAD6D7,
	HitBehaviorDestroyOnSelect_SetUpAudio_mD93DC64D8328AAAA98845906141AA51385E480E5,
	HitBehaviorDestroyOnSelect_PlayAudioOnHit_m1B43FB92F67C81B2C08821A6CD3635BC4D08D036,
	HitBehaviorDestroyOnSelect_PlayAnimationOnHit_mE7A67BA0DDCE69A60793D246085785847B2548C4,
	HitBehaviorDestroyOnSelect_TargetSelected_m055F5740FAF1A2982F3472382E607703E85E97DE,
	HitBehaviorDestroyOnSelect_HandleTargetGridIterator_mE33344F205174B8858B89911037480A1FC705740,
	HitBehaviorDestroyOnSelect__ctor_m97FE187C544C7E19027FD842FDD5ED83CDA5B2B2,
	RotateWithConstSpeedDir_RotateTarget_m6F7F96D35A2EA3DA69D84BC1061E6253A57866F3,
	RotateWithConstSpeedDir__ctor_m69E08EC7D91434F0D86B3FA794D43487711F6FF9,
	TargetGroupCreatorRadial_Start_mA67A53E699ED03944FBE419254D6A513882692D5,
	TargetGroupCreatorRadial_Update_m869304BD0C60D5C2293CFC5A1DFDCE70CFA1A040,
	TargetGroupCreatorRadial_HideTemplates_m4EC341589D7F1EC021643EDEE8B0E15BB2714BDD,
	TargetGroupCreatorRadial_CreateNewTargets_RadialLayout_mAAB8C7C599356B13F544ED0BAB0BB48A206F7617,
	TargetGroupCreatorRadial_get_InstantiatedObjects_m03DF17D7DF48E79E03B248E9A81A90AB35B7604B,
	TargetGroupCreatorRadial_GetRandomTemplate_m5C1D3E9F11457F01F5903208BCCCBD1888A63878,
	TargetGroupCreatorRadial_InstantiateRadialLayoutedTarget_mA815D00F954F6A2678FB4B931940A6B468FBDDE9,
	TargetGroupCreatorRadial_KeepConstantVisAngleTargetSize_m7209CBEE33800B771E5EAC909BE995885C93E3A4,
	TargetGroupCreatorRadial_KeepFacingTheCamera_m865CC37571EEC8A83BDA1A5DE36EC1E2D5E765CC,
	TargetGroupCreatorRadial__ctor_mD266490E90E7C0D6A10A182C811B588EA74798EF,
	TargetGroupIterator_get_HighlightColor_mB3A870016F53B0E8FA04EC12763CD7F5687BFC75,
	TargetGroupIterator_add_OnAllTargetsSelected_m875EFD163DC043D2C9ABDFA4521C213A466C2765,
	TargetGroupIterator_remove_OnAllTargetsSelected_m3D8A5A92ECBC361394691822D37CF083DFBD8C6A,
	TargetGroupIterator_add_OnTargetSelected_mCC0763436B47AC194697BE8BA5A7B518964BCAB2,
	TargetGroupIterator_remove_OnTargetSelected_m78422C1373EE8322D86D64059A3673BB63ACD3E7,
	TargetGroupIterator_Start_m6DEBCAFB61A8531080CD688616B4AF1A3AB49B0A,
	TargetGroupIterator_Update_m2AF5AC176AC94C130FBC8A665431DEC41637E030,
	TargetGroupIterator_ResetAmountOfTries_m9A760A2EDBA46109B4CDF0DF524677EB5C5EF546,
	TargetGroupIterator_ResetIterator_mD05DDCB3D1E5065425E0DEFEB80E29F50102A176,
	TargetGroupIterator_ProceedToNextTarget_m050D44D81CD90018D4478E83B0F6D214231C4C97,
	TargetGroupIterator_get_CurrentTarget_m15C58166B6CF52DF4DD734D0810D151EEF1D9D3C,
	TargetGroupIterator_get_PreviousTarget_m9529D1243E539C7FA1AA7ADC215AB472595D7574,
	TargetGroupIterator_get_CurrentTargetIsValid_m85CD0B51400A3C6509A7A266DC1E43710006E96D,
	TargetGroupIterator_SaveOriginalColors_mDE9F3C8541D8B3D7647189D2E7C9534C61155D3D,
	TargetGroupIterator_HighlightTarget_mEA6A5D5DF62C78E0F09E140113BCFFFAFEF8FA47,
	TargetGroupIterator_HighlightTarget_m9A3C0B0A8E0AC293F18D4233EC0B88D671400236,
	TargetGroupIterator_ShowVisualMarkerForCurrTarget_m3A3AF2369A7A2F918221D86A3F26278428F19E67,
	TargetGroupIterator_HideVisualMarkerForCurrTarget_mCBE553D7A61FE74A45437E4CB5498193319D8285,
	TargetGroupIterator_UnhighlightTarget_m940FC39C7331FA812533DDFCE345A1F1E8BC24E3,
	TargetGroupIterator_ShowHighlights_m01C00ABE482565898FDA8F0E55DB7E29606EE24D,
	TargetGroupIterator_HideHighlights_m0D319DC297B214046A2AB3CFA8549A1B7B77D763,
	TargetGroupIterator_Fire_OnAllTargetsSelected_m4E1E33D9A5F4513DF2A668A56378E55DCE18057B,
	TargetGroupIterator_Fire_OnTargetSelected_m36DDC03C8ACD570ADA90A0451ACF7B954F328E47,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m1A2624821E1018B620994F9846AA46C5E7D6F07F,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mA0343A25FA90D835F8D146D31A7AF2C5AE167594,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mEC1EA4F3941AFD7940732825AFB80A8E0BDF63C0,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m0E00CF1AF4DA7C3E69338AFA50704C4E7A33B909,
	TargetGroupIterator__ctor_m43437D832F241D3D259CE2152D734F81CB51123B,
	TargetGroupEventHandler__ctor_m0A0C790BB2C20720731817045008A44808CEC62D,
	TargetGroupEventHandler_Invoke_m42552E287FAB5C0CA92BD1DEABFE57B378045640,
	TargetGroupEventHandler_BeginInvoke_mAF8566533726FE201775742B0C0C7C7E59943ACC,
	TargetGroupEventHandler_EndInvoke_m7DCFDB6ECC7C4BE9B08B8FE154F5653FB9AB8562,
	ToggleGameObject_ShowIt_mB8DD93B97261826E515806E9AEEFACD2CCF6DE78,
	ToggleGameObject_HideIt_m8F26805E5594C1EDF7BBC446472BFBC0220383A3,
	ToggleGameObject_ShowIt_mBCA892FE92B3F44B55B5B01555AB20413B12ADB7,
	ToggleGameObject__ctor_m4B6464F3D9B71B314A267E36E5F2AF4F79B14130,
	DrawOnTexture_get_EyeTarget_m467BFE852712E26B472E0160E52F98FDCC07A847,
	DrawOnTexture_Start_m873FC4C38D3DEA3E68D9DFFCB96BF56B560A2291,
	DrawOnTexture_OnLookAt_m6612932FB5FFF6AF10F7810189EC90AE5EFEA4D4,
	DrawOnTexture_DrawAtThisHitPos_m5AC10A3A5EBA1778C830210A4D5481B43708E8D7,
	DrawOnTexture_DrawAt_m4618A4966C3EC88DF8AB105F64F5C8573719E8DE,
	DrawOnTexture_DrawAt2_m00A340F1DEB67185C5A6DFE98C71B81D5791B922,
	DrawOnTexture_DrawAt_m54DB30FA4835EE628574AFB21AE537B468C0924E,
	DrawOnTexture_ComputeHeatmapAt_m1E92BF158DB280BAC448ADB11CC2CC46BE372626,
	DrawOnTexture_ComputeHeatmapColorAt_mFC7718FC9B1C2353A3039B4AD236297B45AAF36F,
	DrawOnTexture_get_MyRenderer_mD4DE79E1D7D62B94819CEF5F47AE258557886B2D,
	DrawOnTexture_get_MyDrawTexture_m056DFBD24D6A71936784C166D4F7F321E08A27B6,
	DrawOnTexture_GetCursorPosInTexture_mB6B0A497C35C0482A282BDE8FAE1439D25D70CF1,
	DrawOnTexture__ctor_m80DCD7A503D81E2866DF388EDEF85BB3CE905504,
	U3CDrawAtU3Ed__19__ctor_m3452D4AAE063679249F23B34F44678B64B0B90C2,
	U3CDrawAtU3Ed__19_System_IDisposable_Dispose_m2CAA259EE47BF4A75D6A4A0A790291FCEB7C542F,
	U3CDrawAtU3Ed__19_MoveNext_m5B38BC9A1076E1AB2CA656A2B4B470D04F6C20E6,
	U3CDrawAtU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF4193E3A4C112AE16A64A0B944FA96085386D39D,
	U3CDrawAtU3Ed__19_System_Collections_IEnumerator_Reset_m3E856B0B07E6976D43E084468665197D8CF29673,
	U3CDrawAtU3Ed__19_System_Collections_IEnumerator_get_Current_m52270E6C535DCF743F6BC47C43A853B9246B87CB,
	U3CComputeHeatmapAtU3Ed__20__ctor_mCCF9506B4A8D95E5EEB1A1062CF5BFE1F8C6BE54,
	U3CComputeHeatmapAtU3Ed__20_System_IDisposable_Dispose_m3C8698642C4E061224C9CB51F43EC1B20A099AC4,
	U3CComputeHeatmapAtU3Ed__20_MoveNext_m090D08FEB14BBD387DF1834E7992773C0E86259E,
	U3CComputeHeatmapAtU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD1B4C2DA82FE197A2080AC016EB60EEBE0C88F92,
	U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_Reset_m69A13920489FCF0EA4532CCEA3DD91066B404841,
	U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_get_Current_m19328DB9FD53B9F0A6A43FCAE64A688C2614ED44,
	OnSelectVisualizerInputController_Awake_m5CA45CB519EFED6A175DDE3520CB1E7E3B9BF85E,
	OnSelectVisualizerInputController_OnTargetSelected_m573D51F994EE2AB4ED01C4C50D8AEB1926CDA1CA,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m168C4195481F6AB89052B13372BA31F6385763DD,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m14E473EDE2C4347D1650E0CC92334C85603ACA69,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mBB8D22DF9BC94B032E73E5342C747A4E0557781E,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mC6C01241EA498991ABA8BC7C9023089343CA3B66,
	OnSelectVisualizerInputController__ctor_m8665D68A78227437DF5A95CE9326D6BEB42C1F32,
	ParticleHeatmap_Start_m88EE3C760F408E9D5851D46401D567076AB3497E,
	ParticleHeatmap_SetParticle_m98EED2F6DFAF3D6639C3EC47A09E27ABEC4049FA,
	ParticleHeatmap_GetPositionOfParticle_m0B1D752555BDB67643E55F9BD3371D41EF9F33AF,
	ParticleHeatmap_DetermineNormalizedIntensity_mC71962E67254AD295016481FCAA86AE86AB12775,
	ParticleHeatmap_UpdateColorForAllParticles_mB6BD448702FEDF94C54D44C9BA241DE6CBB9EC9D,
	ParticleHeatmap_saturate_mD123C4FF18C094DA696535D58164D7C91CB5AFB3,
	ParticleHeatmap_DisplayParticles_mADC9C26BF7547F0EECBEFE35AD02232AB0B87F56,
	ParticleHeatmap_ShowHeatmap_mE1DBA3F299E2B2D25598BAA925536997C4A63F10,
	ParticleHeatmap_HideHeatmap_m118FDFF8D3C92F1CC17CE9C6C86034E17F2A0057,
	ParticleHeatmap__ctor_mA87FAEF3D38908724374FB60FC576838D5071DF1,
	ParticleHeatmapParticleData__ctor_m7C5BF207D19B830B82718265C8FB8E838F5BDD41,
	AudioFeedbackPlayer_get_Instance_mCBA53A3C17CF2CD06CA0343C8A1CE725F5779B6C,
	AudioFeedbackPlayer_set_Instance_mDA0E8C26EDD76B2A5E40D371F30AC064A31EBA6C,
	AudioFeedbackPlayer_Start_m3D194EF14E1921665959BBF582E89A566F7C185C,
	AudioFeedbackPlayer_SetupAudioSource_m86C9DFBDDBDF010BE6D71184CDF80230B99998C3,
	AudioFeedbackPlayer_PlaySound_m1039CF245B7D320EDA3AF6A3B4808788A3C71BC2,
	AudioFeedbackPlayer__ctor_mB0BCA6380697B49EEC8B4CF09254C55B2A5B36FD,
	FollowEyeGaze_Update_m3137E86CF43C4DABC470B943169184B34E4F931E,
	FollowEyeGaze__ctor_m5A9F005AA53DE29F950F3AA271249D50437928EF,
	SpeechVisualFeedback_get_MyTextMesh_mFBE0088647B5DCF41E11609E886E1889D8BCBB72,
	SpeechVisualFeedback_UpdateTextMesh_m1EF7E792946EA53258473177A1B6B743CFA3A170,
	SpeechVisualFeedback_ShowVisualFeedback_m93C60B8D5C9284C857D1107BEB0A3A1C8EE2489D,
	SpeechVisualFeedback_Update_mC8FAF719DFA65EBB8519922FF6EEF82A805D2B17,
	SpeechVisualFeedback_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_mF3FA085FF5F59D69723246BE5E5329FD81F2AD65,
	SpeechVisualFeedback__ctor_m6D25C50E7C642AD0CCBBE0AB78D8B353D335B6CF,
	BlendOut_Start_m1744429E7BC8A534AB2677101DFD6F1E87AE6F0E,
	BlendOut_InitialSetup_m91413838FA74302F088DB5FFCC5E30662D472F95,
	BlendOut_Engage_m964CDCA7BFE2A2EC5046C1E585DF994529F00F9D,
	BlendOut_Disengage_mD6FE95FB9EDDF8B4430F9A532E490B2C07DD54FB,
	BlendOut_DwellSucceeded_mF5E63829A3A80643D849EE85EC43DA68F3898AE4,
	BlendOut_Update_m15966AF5E569739ADE10B249F74778D20495A9F8,
	BlendOut_SlowlyBlendOut_m7601026AA0817505A52B0C68B11F81558B734580,
	BlendOut_ChangeTransparency_m59F8B52CF21A2E2119D3BB6D88DF844C89BB1E3D,
	BlendOut_ChangeTransparency_m662078A4265014EF9C294AB8C08C87F9C877AAA7,
	BlendOut_Materials_BlendOut_m8278EC0CDA86DC03BA67B7F0E276351AC5011E61,
	BlendOut__ctor_m5FD487435FDF3B1BDD97EC751B9CACCA5CF9FC62,
	ChangeSize_Start_m668E97DD9A42AB46CDB6F0B40E90E1A012F83166,
	ChangeSize_Engage_m401A40CC0C22E6FFAD03E881562E93DB6928AE90,
	ChangeSize_Disengage_m573685FF359DA0F5ECB5E7FA0C01F24CD80A9C91,
	ChangeSize_InitialSetup_m897F44338833B714CC0F3138CF60B62368C4C924,
	ChangeSize_Update_m7165D93EAFE0C52CBC65CBAA16DE8C2F76AE12B2,
	ChangeSize_OnLookAt_IncreaseTargetSize_mE9DDD81B8E1892B7B57F1FC0A4A10E379D8FED2A,
	ChangeSize_OnLookAway_ReturnToOriginalTargetSize_mA0F97D2DA5563AAB8F78BF2EAC2CA7DE755B31A3,
	ChangeSize_AreWeThereYet_m34F26A45E2DCE4BFFD75225EA6AB54791ED44EC7,
	ChangeSize__ctor_m8DBFDBA094FC9E6163CFD0184BD7E7CDB152B447,
	FaceUser_Start_mE441F5BA68CE27B4D9E66755308E698184A8BE1B,
	FaceUser_InitialSetup_m142B0A453F916BDC1B198E95009A8B64836E859C,
	FaceUser_Update_m38910082605F6A82C19B1FEF49959B6BA0314A4F,
	FaceUser_TurnToUser_mFFFEC3D92007051D1FEFC2778E59EFC3595305BB,
	FaceUser_ReturnToOriginalRotation_m634BF42CD7FDDA12B36F3E9D79A1C2E3CC8EED04,
	FaceUser_Engage_mE4ADC460DC6D03C6178974959810334EEAF27B08,
	FaceUser_Disengage_mFEB625D405929962CBDA7EF092B4D5C6B5F0B05D,
	FaceUser__ctor_m0C4446030572CB7661CD06AC3234BDD379814BE0,
	KeepFacingCamera_Awake_m17B46C03C0D0108DC0A01B8D3F01526C2CE5C160,
	KeepFacingCamera_Start_m9846E6CA36F19BD864FC02E86706EBBDD155C273,
	KeepFacingCamera_Update_mCD119CE5F0F2DA4872713CA4B08DC564840E3064,
	KeepFacingCamera__ctor_mDCE9A0BE451D9A6F2139A0AC65E3AC93AF52741A,
	LoadAdditiveScene_LoadScene_m0CF29706D86EECB6D045F3677C822846F7BEF9D0,
	LoadAdditiveScene_LoadScene_m719AE7192FB113C8ACDE3897322C95F072FA4606,
	LoadAdditiveScene_LoadNewScene_mA9B0F703148A35AE42F526F6B1D0D074BEF83C6B,
	LoadAdditiveScene__ctor_m56DA10D254B57FE7B93616BFBAE48793195E357B,
	LoadAdditiveScene__cctor_m25D6C56543EC29322963954D6E0C4199106653EC,
	U3CLoadNewSceneU3Ed__6__ctor_m7AB7244F6CA4375DA1E463300EDC1170CD8CFA56,
	U3CLoadNewSceneU3Ed__6_System_IDisposable_Dispose_m5FF2C084528B0704B251DBEA0D6BECA6D8B71BEB,
	U3CLoadNewSceneU3Ed__6_MoveNext_m9BE12E8D747D3DE27D3B75DFA491122A543901FF,
	U3CLoadNewSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9A59C746603E6F919F7E65B33B003181E837304,
	U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_Reset_mC7480A3BFA0D90A78B4D4A312001384BA163EA10,
	U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_get_Current_mFBB82D699D897C1EB00FE080178AA94A5B6C11A6,
	MoveWithCamera_Update_mD7074A5166DAE0942BBE3ADC3E815C531F4BC532,
	MoveWithCamera__ctor_m70A8DDBDE3028CB82A743E15F4B5A345AACEB71D,
	OnLoadStartScene_Start_m77030DC5D61416EE56A0F23D5597AA0A6ECE3CA1,
	OnLoadStartScene_LoadOnDevice_m499DCBB4C8EFE46A43AD30E9F2DBD641E7A58359,
	OnLoadStartScene_LoadNewScene_m86088B37DD8C9A64E39F10AD8B0A9011B61350F5,
	OnLoadStartScene__ctor_mA99E13D54949FA2F2660AC7F63F8F959D0C2BA6F,
	OnLookAtRotateByEyeGaze_OnEyeFocusStay_mFE2399F4CE91BECACDBE337489FDB93E279B368E,
	OnLookAtRotateByEyeGaze_RotateHitTarget_m96454A9E3FEB0983A9F332CFF3830BFB33AC10BD,
	OnLookAtRotateByEyeGaze_ClampAngleInDegree_m4A771E7693FF0EE775FF9005B9BA1AC589443018,
	OnLookAtRotateByEyeGaze__ctor_m9E94BE4D49F3240EC8DB09E6E41267871F06D87D,
	DwellSelection_Start_m9A630CFF908970997B2A13FF42F51376CA7DBEC9,
	DwellSelection_EtTarget_OnTargetSelected_mA0A700CA9D9CC6C333C8EFC74A393CCA3278635E,
	DwellSelection_EnableDwell_mAD42E96313AAF18EB38E69B870DBF98FA5D4DF53,
	DwellSelection_DisableDwell_m355C17B84690347BEB02B47402A274759D7EF745,
	DwellSelection_get_UseDwell_m1D058F5ABEC4C17FB86F938253D130236C93DA9D,
	DwellSelection_OnEyeFocusStart_m9303B20BF6E1051A7AD6D817CF00EB142518E680,
	DwellSelection_OnEyeFocusStay_mB88259BECF276CF96A2FA77180F9A7378407D9D0,
	DwellSelection_OnEyeFocusStop_mC94C67B81A99DC79B5E6B151F81FBC1341F4D298,
	DwellSelection_StartDwellFeedback_mDDA6055A6F241C77221A41068380231AB97E5349,
	DwellSelection_ResetDwellFeedback_mCB095C812A9C40B7A8F0201F485EE6444CD7F748,
	DwellSelection_get_SpeedSizeChangePerSecond_m4077FEDBAE4647E4118D2034216CE0085640D111,
	DwellSelection_Update_mF4AA63723902ED768C74EAD9C9740ABED44B691E,
	DwellSelection_ClampVector3_mA73A583F01C46F806B90CF6507AA039DF5F026F8,
	DwellSelection_UpdateTransparency_mDD03FEED8CB2EAC221E69EF3C6325A09704BE3B2,
	DwellSelection_LerpTransparency_m13149FA74B8B556445CA3CA77331C29471EDCCA8,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m44FF5711B3307F11C17FA7BB8F09760042BBD277,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m9F94E95B5A7FFA97A38307A9EFC7FDFA1DF8F5F8,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m1B178B56EFB131E5E6175804361DF52ECFB900C8,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m8DBEBE2A807C9B14344517C73658B05468AF8D23,
	DwellSelection_get_TextureShaderProperty_mC1C11397DAFF17C1A82AB093EEB017BD2852E2F0,
	DwellSelection_set_TextureShaderProperty_mBE7E90ECF8808AC33DF45E77307611284AFEFF7F,
	DwellSelection__ctor_mCFD10568F30920D008800DB87685647A166E5D8A,
	TargetEventArgs_get_HitTarget_m79731B1B64F57006419156123258483169AA9DD1,
	TargetEventArgs_set_HitTarget_mBDCCC4B4B8E6F554650DB774DB9EFE2B9F1B084F,
	TargetEventArgs__ctor_m9C9EEFD59B447E36A991D757AF2919D4D2B82594,
	ChangeRenderMode_ChangeRenderModes_mC125A77B940A37697754553384FD8D7C12D1E150,
	DoNotRender_Start_m3CB963F8058C4ABB4F263BB3156C13D9BC372EC5,
	DoNotRender__ctor_m92A4C218A60CFD9E8D40636C57F6533DC432C73D,
	EyeCalibrationChecker_Update_mC9BC31FFE361F9B2C6FD6C873BFFE430E2FD6B82,
	EyeCalibrationChecker__ctor_m70521FDDF27910A6B62DBA67CEDCD86B03A9F319,
	KeepThisAlive_get_Instance_m12A9C26E66BCAC799293E0EF8CC51E6834D8C7F5,
	KeepThisAlive_set_Instance_m2F579A8EFCC355FA206AD3919F875E46A8B3827F,
	KeepThisAlive_Awake_m94F659CDD8BECCDE0581B04A6ACDB26B978577E2,
	KeepThisAlive_Start_m76E57B1A8F849DC271763AB76B6893F1C5E56F0A,
	KeepThisAlive__ctor_m79E70C4D62F79AA2E1D68BFCD2C836A67C5C459C,
	StatusText_get_Instance_mCBB3E8CCF8874AB75906D5078D47ACAF6930D1F9,
	StatusText_Awake_mAE706EAC0908CAD3B0F10262CC28E27459E41445,
	StatusText_Start_mAA9B137FA243051B1164C368602A02FC405E97A3,
	StatusText_Log_mF3B438327AEE53C0BA8003D1D56662105A580A0F,
	StatusText__ctor_m8AFF8179D5E3B76A3938A58ACAD1ECF09442AD7D,
	EyeTrackingDemoUtils_GetValidFilename_m7D3381E8288741E7C1D6229E74BAEEC1AF4BF696,
	EyeTrackingDemoUtils_GetFullName_m0726B6420CB4AAB80CEF716965669572A5D1ED8C,
	EyeTrackingDemoUtils_GetFullName_m760F975A56FAF7E56716A9C7330B51F368D76ADF,
	EyeTrackingDemoUtils_Normalize_m941014ACB293B86F3460D81F06085E7C7DBE55E9,
	NULL,
	EyeTrackingDemoUtils_VisAngleInDegreesToMeters_m383345A2CC28FDDA40964B190C43E9BE035A2597,
	EyeTrackingDemoUtils_VisAngleInDegreesToMeters_m51014BC01BA4B552D137F455292C3E4F13418275,
	EyeTrackingDemoUtils_LoadNewScene_mA7052E4FDC288015737F723924477A29E9FD5CD2,
	EyeTrackingDemoUtils_LoadNewScene_m0AE63AECE54363C8902C0CCCF90D5170FC2EFEDC,
	EyeTrackingDemoUtils_GameObject_ChangeColor_m54D18DCFAC811DFDEC0FCDD817A9142ECA014862,
	EyeTrackingDemoUtils_GameObject_ChangeTransparency_mB51F7B9210D71C29F6DC7D5C938BA5D4BD37C8E6,
	EyeTrackingDemoUtils_GameObject_ChangeTransparency_m31E98A089CD6185CEA9F2AAF97B5C7EA93A550B2,
	EyeTrackingDemoUtils_Renderers_ChangeTransparency_m23EA9627CD35F3DF3F29910A249762628EE67B57,
	U3CLoadNewSceneU3Ed__8__ctor_mB392F101E0418B2AD32E95DCBC57083DDD7D9EB7,
	U3CLoadNewSceneU3Ed__8_System_IDisposable_Dispose_m1CE9B5CA891ECC02A581C59D07BB981D6EF7928B,
	U3CLoadNewSceneU3Ed__8_MoveNext_m22E33674E9BCC058A57BA951579425BD1B2C1E4F,
	U3CLoadNewSceneU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF5C96A2407CC0BC55FEAB004DEE4E564744403C7,
	U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_Reset_m8D4D220DF4E3AB63A857D813DACB0A852C5828E5,
	U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_get_Current_mAA4DB3CC0E90E566E1B391E592CDA3CDF1A681ED,
	OnLookAtShowHoverFeedback_Start_m64DE29022B8BAD03B7D0578ACD15E2D2C4245DD3,
	OnLookAtShowHoverFeedback_Update_m82A433551AFB41BB00E65F0C3DEB1CF3C46682F0,
	OnLookAtShowHoverFeedback_OnDestroy_m082E52D60012F44E2B94BEDCA6283652D6859EDD,
	OnLookAtShowHoverFeedback_NormalizedInterest_Dwell_m7242681E062C6E0B0FF848D5A8A76F5C84FAE6CA,
	OnLookAtShowHoverFeedback_NormalizedDisinterest_LookAway_m906C941DB572DEDC6522176B94994875DECC1299,
	OnLookAtShowHoverFeedback_DestroyLocalFeedback_m62F895D85F845F02040ADEADED339140E940E4C1,
	OnLookAtShowHoverFeedback_ShowFeedback_mB6738E6F8EF0657E36B317A684CF5173A9CC174C,
	OnLookAtShowHoverFeedback_ShowFeedback_Overlay_mAFA93AE6832FE97F78A75AA093D403874CE0962D,
	OnLookAtShowHoverFeedback_ShowFeedback_Highlight_m828016C6892FACB4F46EEC132909C9EDC39C10D7,
	OnLookAtShowHoverFeedback_TransitionAdjustedInterest_mBB31EB549232F23DE71E807F2FFF4DB13C9635EB,
	OnLookAtShowHoverFeedback_get_LookAwayTimeInMs_m3EFFD60140F144CD1B6C84BABBA079E9B06BE61A,
	OnLookAtShowHoverFeedback_get_DwellTimeInMs_m903B205BE77CFDB77DD2AF8D402DBABA46316811,
	OnLookAtShowHoverFeedback_OnLookAtStop_m0CB31B582D1F7DB41AAC9DBA16323936786142B4,
	OnLookAtShowHoverFeedback_OnLookAtStart_m625A7BBD02720DA79DADE81E27E58681887BF697,
	OnLookAtShowHoverFeedback_SaveOriginalColor_m6FE289F5EAAD733BDF667256BCE6B216B81ABDE2,
	OnLookAtShowHoverFeedback_GetColorsByProperty_m838F980316977B834901C11FE2B0943DA54B7A9D,
	OnLookAtShowHoverFeedback_BlendColors_m29808D7157CDD76EACE4E0E17D6F04761FC658E6,
	OnLookAtShowHoverFeedback_divBlendColor_m6E4403B34857DB217F58E216A9DF613ABE186511,
	OnLookAtShowHoverFeedback__ctor_m0F54940A8D11265C5DCE2D51303BF1170D236933,
	AsyncHelpers_RunSync_mD89BCA60E72B417FF35C83C8E9EC0E6C0E5588E7,
	NULL,
	ExclusiveSynchronizationContext_get_InnerException_mCFC704A020866CE27AD451ACB0AA5729448E06FE,
	ExclusiveSynchronizationContext_set_InnerException_m4AB464A291F79D5848A5A9EB296FD2B7DF450B0C,
	ExclusiveSynchronizationContext_Send_mD78553FD25A6B3DD25C24AD662376D792ED2C0F4,
	ExclusiveSynchronizationContext_Post_m4ECFF1199D4B2603EAC872EDABD9ED956873AC9F,
	ExclusiveSynchronizationContext_EndMessageLoop_m6C548C3CC4F111C370B79536860614767CEDFE8E,
	ExclusiveSynchronizationContext_BeginMessageLoop_m0E9BC45A55EBEB589021D4305B87E78B25EAB6C4,
	ExclusiveSynchronizationContext_CreateCopy_m6A0875CD991BC1DF040E632AA5DCC8336A54BA48,
	ExclusiveSynchronizationContext__ctor_m8C089CA17D4B9E33E726626ABCA65AF3C7C73F70,
	ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m5B8F1B4820BF0D41FFC62F7A98B8595B67F8F2BA,
	U3CU3Ec__DisplayClass0_0__ctor_m02B40EFB70D4BD0D542411C7ADF77EC6C48229B2,
	U3CU3Ec__DisplayClass0_0_U3CRunSyncU3Eb__0_mD7F01011FD2978D9E88CA54DC0110ACBC5E569C5,
	U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m62460AC0CF68DFACF7EC1172E850097F9DEE5645,
	U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m2DAE57EA7D64EB02ACB7EDF19035F855583D4439,
	NULL,
	NULL,
	NULL,
	NULL,
	BasicInputLogger_SetUserName_mD16A00E62E434C7338E979D3702B1D068FB15DAD,
	BasicInputLogger_SetSessionDescr_m6E4D34BC34D7C92DE4CF89CEFF42E53D187352E9,
	BasicInputLogger_get_LogDirectory_mDA6F95CA28011BCD49F1E56DBC7E94DFC1452EF6,
	NULL,
	NULL,
	BasicInputLogger_CreateNewLogFile_mD6C79CFC4D09BD9D6093FA1B7761328F852500ED,
	BasicInputLogger_CheckIfInitialized_mD37DDCA2A7480C7744A0088781DE0BC6A167565C,
	BasicInputLogger_ResetLog_m55555949F93B9911F414D3EF0C5C3510A896E6FD,
	BasicInputLogger_get_FormattedTimeStamp_mE980AA36630D3585592A3044695EE7C74F339034,
	BasicInputLogger_AddLeadingZeroToSingleDigitIntegers_m448C915ECE46D804079925AB74B7B70F9CBC20AA,
	BasicInputLogger_Append_mCCE2850E50A08D290CD10CEC31BDD95633D8556B,
	BasicInputLogger_LoadLogs_mDB20EE52F95C11817164AE3D01C20F570BE54D14,
	BasicInputLogger_SaveLogs_mBA991B292BFAEC0BE338B222AADDBE4FDA3C59A6,
	BasicInputLogger_get_Filename_mA90D7E48E689C63C9FE5053B7E4DD36411FD5EAD,
	BasicInputLogger_get_FilenameWithTimestamp_m2FA61565A7E50DF43077B5A883EAAA7E2D4CA5F9,
	BasicInputLogger_get_FilenameNoTimestamp_mFE4AD530DAB269FDFACDB243C6E504AE226C7837,
	BasicInputLogger_OnDestroy_m265A66E30EE4370B42657361ADCEF5203B50F336,
	BasicInputLogger__ctor_mF6D8393446C41AB9337D3E25DF6481B4ADB1DC66,
	U3CCreateNewLogFileU3Ed__14_MoveNext_mBE68DC2547DAEAB02399255517DD3DABB152AFCF,
	U3CCreateNewLogFileU3Ed__14_SetStateMachine_mBB528CB8977F8797016496EBEFF3139CC717A28C,
	U3CLoadLogsU3Ed__21_MoveNext_m465394392A1CC40B1F76EF054E462327CB481AB9,
	U3CLoadLogsU3Ed__21_SetStateMachine_m1FEF9DE9FB018C66BF156F8411185C92ACC6CF57,
	U3CSaveLogsU3Ed__22_MoveNext_m3FB430EA63A9310C00C2B2A80A0200677EB671B5,
	U3CSaveLogsU3Ed__22_SetStateMachine_m50C0515F078C93C7AA07C7F4B639D1816AE84C3F,
	CustomInputLogger_CustomAppend_mC3082A5143200FA5777BFE3596F67985350FE0BD,
	CustomInputLogger_CreateNewLog_m30A5E5B3FD7249E6848DC27F3464F26C13BB311B,
	CustomInputLogger_StartLogging_mED2A25A2CE4A16379E7FC9837EA0EF8FACFA219E,
	CustomInputLogger_StopLoggingAndSave_m28EBF1DDEAA1A281261E3001A3C0EB9E22BCD467,
	CustomInputLogger_CancelLogging_m6D2EB89F7D294D86A783BDB0F30F8913C541E8A7,
	CustomInputLogger__ctor_m68341B2FEAAC17DC4EE414FA8D6165F19AC4C497,
	InputPointerVisualizer_Start_mE03BAAAF070032DDFD1707E179C164FF58FFC273,
	InputPointerVisualizer_ResetVisualizations_m0F5A625385650DB837D0B0888781C998C0358F80,
	InputPointerVisualizer_InitPointClouds_mBB6DCD21230132A22D1930086C8EBDCFBD9D919F,
	InputPointerVisualizer_InitVisArrayObj_m7E25C4F30B12FAF52919C7A8B3D8C33522749DC1,
	InputPointerVisualizer_ResetVis_mDBC1E18CAFE00E7C383109AD13C9C230A1521C42,
	InputPointerVisualizer_ResetVis_m7AD448B6FCC5C78DB664AD1C4BCB52C6C0B7E52C,
	InputPointerVisualizer_ResetPointCloudVis_mC2998A43D80C42D2838C6CBDA55980626ACD46D9,
	InputPointerVisualizer_SetActive_DataVis_m320C46DED5F2C429A2C72AD8801E36E8B8AB57DD,
	InputPointerVisualizer_SetActive_DataVis_m92C617218D9974454C1D65EE3E0EB379580F761F,
	InputPointerVisualizer_SetActive_DataVis_mCDC1FB154A8565FD30E4592B940AD93504E7423C,
	InputPointerVisualizer_SetActive_PointCloudVis_m0BAFB659E2579B3D22EA4046541EEAD127217EBD,
	InputPointerVisualizer_UpdateDataVisPos_mF7364829B2CA249BB88DB54312D91A77FDC027BA,
	InputPointerVisualizer_UpdateVis_PointCloud_m813FB2BCDBAF3F04C8E9DEDA73BC464712E91172,
	InputPointerVisualizer_UpdateConnectorLines_m550407448267FE7D90D2AB8D52CBC69B751AA4CC,
	InputPointerVisualizer_GetPrevLineIndex_m16699A946300D62ECF52753B73039A0B9E06F187,
	InputPointerVisualizer_UpdateDataVis_mC57E3CF77932051D9E3958339917F53FC2AEB7FC,
	InputPointerVisualizer_PerformHitTest_m5338763DE47B3D9AB2DE8848F58622BE0E05EDE0,
	InputPointerVisualizer_Update_mF2F2993E82780A901562DC5ABD876294F4A1436B,
	InputPointerVisualizer_IsDwelling_m4FC2A6406E403CA7F71F5527C66E03E7EDA97E06,
	InputPointerVisualizer_get_AmountOfSamples_m45ED071FED4465F869AB67DAB1590C1AB8C3287B,
	InputPointerVisualizer_set_AmountOfSamples_m5994068C554E26F9432C9BAFB268AAE39B8A2905,
	InputPointerVisualizer_ToggleAppState_m0FA7B318E61EFA84A5E75C566D7BA2CA4EEBB63C,
	InputPointerVisualizer_PauseApp_mA3827A91ED15A89FF4AB2696EB329BCB04572A2A,
	InputPointerVisualizer_UnpauseApp_mDE63EC556A99F52378A35A7708F639D9F9CBB007,
	InputPointerVisualizer_SetAppState_m73E5B39D8BA915D6D52E858B0FAF1818BB324585,
	InputPointerVisualizer__ctor_m936E1933F44E64FBC07DEADFEA4D81602E06F5B7,
	LogStructure_GetHeaderColumns_m303412053AC45A43FB7688CAA6F2D61C0D472453,
	LogStructure_GetData_m01E628A5AABEE7FD5409C76975AB6E25F858BDB5,
	LogStructure__ctor_mBF7F2525B379518460C787DCEA666AE9D30684EB,
	LogStructureEyeGaze_get_EyeTrackingProvider_m61F2FD20116F0AA4585441077AB99A4C452C148F,
	LogStructureEyeGaze_GetHeaderColumns_m58D256A8BCFEF7AAAAF96548822F34CDF5FBEEDB,
	LogStructureEyeGaze_GetData_m250ED06546EED5314EBC38F6DB0AE7C4FAD289AC,
	LogStructureEyeGaze__ctor_mCEE67B1F064E42F371B7F68F48E9332935AB8A8F,
	UserInputPlayback_Start_mC667090401F44C52662F03B3C5D9FB0AB5F2297D,
	UserInputPlayback_ResetCurrentStream_m1348C34AECDD19020DD384D09EAFA4B2B5275175,
	UserInputPlayback_UWP_Load_m90DD8792E60ACDBC77A34E7D4A0ACA52C28FDFBC,
	UserInputPlayback_UWP_LoadNewFile_m950B4D6A913676FDC2EC1C916B0576C7B5DE80A5,
	UserInputPlayback_UWP_FileExists_mA6D81019DBEB76446532FDCE758A615EF612E561,
	UserInputPlayback_UWP_ReadData_mF915D6532E33AC4033624029D2BB53C28EB84CD6,
	UserInputPlayback_LoadNewFile_m9B5593EAA5CE8861955B49F0A3D748D0348638E9,
	UserInputPlayback_TryParseStringToVector3_m2405B9E525C2D5E1CF5B7859D4B8C15A7825B0C4,
	UserInputPlayback_ParseStringToFloat_mCB2D59245B32555F11D09E11A1F255ED9EE8FA46,
	UserInputPlayback_Load_mAECA47AF116374D7726712F08F4E2CFCE6CA1E33,
	UserInputPlayback_LoadInEditor_m8AF8F85982EA8941CD914C182A50DC96C4E678FE,
	UserInputPlayback_LoadInUWP_mFA032E94B5A1FA935658B64D1ECDA5FEAF6C25B8,
	UserInputPlayback_get_FileName_mBB9BF1CD012F107962E877CB7331AB87ACA72AD0,
	UserInputPlayback_set_IsPlaying_m023BD2B8382793CA011B97B6BD0D8B9021EB7C03,
	UserInputPlayback_get_IsPlaying_m24956047C048537124AA17206FFA4011B89949F9,
	UserInputPlayback_Play_m3F7CCE9AB584905B7631C06944AA745C1CE1FDC6,
	UserInputPlayback_Pause_m556F1156829B50B626CAD54653434781A63815F7,
	UserInputPlayback_Clear_m919F93574AC5F4190E1CECDA7D33CBD96EB919BA,
	UserInputPlayback_SpeedUp_mBAE45189BA7CEAC5FFA289060A643CF39BAFDBC1,
	UserInputPlayback_SlowDown_m4A626E35D8A4921E6677DE6F993A9196FEC391A2,
	UserInputPlayback_ShowAllAndFreeze_m8B42EE1823AB6B8FCC4F74C31CEAF0C6639646ED,
	UserInputPlayback_ShowAllAndFreeze_m19B7616E1F24B8E4C4A6FAFEB78FA891ED0894D6,
	UserInputPlayback_ShowHeatmap_m7A816D88BA6A89AB5415A7595B795F3079E9C020,
	UserInputPlayback_LoadingStatus_Hide_m3B145FFA3112A7428F7C0F839A75F83E528E4D5C,
	UserInputPlayback_LoadingStatus_Show_m02ED23C7BFD68559AFA9D1F99F8D8A709C7427FB,
	UserInputPlayback_UpdateLoadingStatus_m2C336BA7F96CB4D9379AF9CB6E2C5EC526988DDC,
	UserInputPlayback_Log_m3876C3A165ECE5E80C1590D6B408794C65BF7E7B,
	UserInputPlayback_PopulateHeatmap_m7297D7FB753B5C44B8AD7B3FBC9407D11878181E,
	UserInputPlayback_UpdateStatus_m707E890ECC0327E36F59E30D065212C6BF509FBD,
	UserInputPlayback_AddToCounter_m60C9688D18B4BCD6DBF4E639F56C408FC70F408C,
	UserInputPlayback_GetEyeRay_m9A80D2B933D77A9BC254F21BF19D9823892B2A34,
	UserInputPlayback_GetRay_mAC062B30B2E9A6BDC80B3665743F8EEEEE955C6E,
	UserInputPlayback_get_DataIsLoaded_m12D79CCECBFCD7ADA760A8865CE3685F05B6C8BD,
	UserInputPlayback_UpdateTimestampForNextReplay_m4705CE9D6611EDE732A85933107B757F1AF9682E,
	UserInputPlayback_UpdateEyeGazeSignal_m79DEFD0803D85B1210326D2D3EFCA82234F776C7,
	UserInputPlayback_UpdateHeadGazeSignal_m374514A7134E8B3056B4ED655364FDC625B953E1,
	UserInputPlayback_UpdateTargetingSignal_mCFE75108091E3B39263B86E3D9ED4C2CBC791688,
	UserInputPlayback_Update_m616F13984403A96DBB8D4A18BB4BECE965DDA387,
	UserInputPlayback_PlayNext_mEADD913728F4628DF229B10FD331EE3F576C804C,
	UserInputPlayback__ctor_m8EADDAE7D039D4BA559DFA8658739EED777AAC18,
	UserInputPlayback_U3CShowAllAndFreezeU3Eb__34_0_mADBC70CE9E9E8DB02676A4F6C446A8E614D8ACC3,
	U3CUWP_LoadU3Ed__12_MoveNext_m9D85D0A3795968BEC8AA55D56467300F3DD1F7C9,
	U3CUWP_LoadU3Ed__12_SetStateMachine_m2C4B3C455A8E03E867BE61D5B1BA0E4A63FC7DB8,
	U3CUWP_LoadNewFileU3Ed__13_MoveNext_m3113DF5B967063CD4CA6542ED9AAF57F9BB6E615,
	U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m08D7E1B935DF8D5DDE59EE125E5457EDF6EC3426,
	U3CUWP_FileExistsU3Ed__14_MoveNext_m9C9E5129BAEAF23D1AE693F6D4C27BDA11AF5359,
	U3CUWP_FileExistsU3Ed__14_SetStateMachine_m40DA4924B55A8B15F9E4BAB780B72948A8FD0C1D,
	U3CUWP_ReadDataU3Ed__15_MoveNext_mC6415C909253B839D77B2FA26344FBAB31987CBD,
	U3CUWP_ReadDataU3Ed__15_SetStateMachine_m7FB0514A5F50E95AEE67876D43CB68734CB6F2D4,
	U3CLoadInUWPU3Ed__21_MoveNext_m2CD40348FA310488ABBB418C2B433AB9C27D61C0,
	U3CLoadInUWPU3Ed__21_SetStateMachine_mA2CB12B355F31796EF31B4B349AD43BEC4AA16E4,
	U3CPopulateHeatmapU3Ed__42__ctor_mFEB8BC1B8438EDC42891740833DD8E8B6899D765,
	U3CPopulateHeatmapU3Ed__42_System_IDisposable_Dispose_m25C0C9052C7C7EA3EED60A7B232EB12B8DD6DB6B,
	U3CPopulateHeatmapU3Ed__42_MoveNext_m8FB1222539AC58D5758B7CA54584D9ABF06A80FC,
	U3CPopulateHeatmapU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DEBA026CE679FB88BD7FE72E67CCEE67E2E182E,
	U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_Reset_m1DAD006870E46A0DD9B39FE51E9A2CF30DA21617,
	U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_get_Current_mCDEE30E2F0FE5048403561399AE76FDB449366D0,
	U3CUpdateStatusU3Ed__43__ctor_mD576E82ABBED4B360863524A4F69B9BA7D8D09B3,
	U3CUpdateStatusU3Ed__43_System_IDisposable_Dispose_mD7FBADBEB5474CDA9F8FC54DE1AB4E60C1E4D4A3,
	U3CUpdateStatusU3Ed__43_MoveNext_mA417CB03A73D772820F828B4797733007A904340,
	U3CUpdateStatusU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD92EF9DB25465F34E86245F82272ADCEAE87D159,
	U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_Reset_m57A40C13F3D1AD1ACA7CCE9243F449C651E752A3,
	U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_get_Current_m216A1667EA3DEE80308E3AA09DCC9CC9AC6199CB,
	U3CAddToCounterU3Ed__44__ctor_m9472F6FFDFEF3B98CEB09D32B5F8C44B17C6D003,
	U3CAddToCounterU3Ed__44_System_IDisposable_Dispose_mC040F55582579BC4B84C95F6FF7BA79B6093F88E,
	U3CAddToCounterU3Ed__44_MoveNext_mA4E3D0F1235B6CD1307C2FBD1B2C4128739D089F,
	U3CAddToCounterU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFAB8863E56E1688FAC9D1A040A8A5B3362492A78,
	U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_Reset_m6CEDB25F9FB1FB8A1E990B25E617DB4225F05E29,
	U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_get_Current_m8DA98EE14D3229C805831FAE82F0590DF9186BC9,
	UserInputRecorder_get_Instance_mA42897FA3A7809865142C5CDFACEC81B96115736,
	UserInputRecorder_GetHeader_m4E1EF17483C22B42D63094C63255699ED2F176AC,
	UserInputRecorder_GetData_Part1_m1CD4A8771239C2B72B75271A33AF69CB5281AD38,
	UserInputRecorder_MergeObjArrays_m0F2A8FAF0C0414A2775A5EC49E509AEFD0E6210E,
	UserInputRecorder_GetFileName_m80A4298C1350FAFAA3000EB71EFAD41EA345529F,
	UserInputRecorder_LimitStringLength_mDAD5453704B180BAAEA5F8B47EFDEE9035FC5765,
	UserInputRecorder_GetStringFormat_m007CDF936438A9CB84EA612FDABA4BC396FE93F3,
	UserInputRecorder_UpdateLog_m05C644149530FB3511BB26934713EE775D90C575,
	UserInputRecorder_CustomAppend_mBAC7C3131EC4FAA890AE59F64821917BE339793D,
	UserInputRecorder_UpdateLog_m5B2E28D08848E13F92FED146ED3E5302C47E8E43,
	UserInputRecorder_Update_mEC43DBA63B0290ED1E5F68BD4E952E3E6E0A1261,
	UserInputRecorder_OnDestroy_m24C1FC84FCC3F5CD2AC648395FEA96AB1B3CB422,
	UserInputRecorder__ctor_m6D09773EE4B28256885E670CF176F2C6FA90C827,
	UserInputRecorderFeedback_PlayAudio_mB74774A018E1F222708E7F7182E7D30EF79B7A7A,
	UserInputRecorderFeedback_UpdateStatusText_m1EEAA1B0E8C6E49018D8B31B800AFCF6E8FC2772,
	UserInputRecorderFeedback_ResetStatusText_mEA67A8CDE68FE4BC92A7691CA6E1241C693FE768,
	UserInputRecorderFeedback_Update_m5B5CFE50E23741A3F9F071BAEB5FB7BAE6CC48E9,
	UserInputRecorderFeedback_StartRecording_m08B966EB4ECA894A3649829EA144D7F9A4F675E5,
	UserInputRecorderFeedback_StopRecording_m726CCCE3DAD9B568ED0CFDFA8612433C8FEC25BE,
	UserInputRecorderFeedback_LoadData_m033FF5F070D62978136ED080928D0088F32D114C,
	UserInputRecorderFeedback_StartReplay_mB2242EB195576153BEA6A8E6FD7DA48642BE9416,
	UserInputRecorderFeedback_PauseReplay_m780CAB3B11222EEE5935ED3BA0AFBF289CDAA6F8,
	UserInputRecorderFeedback__ctor_m52A8C8EFC72559AD4847AA794484C1272F728EBA,
	UserInputRecorderUIController_Start_m8F233705BC48CE2DC032E948BCFA613CA2B2A4B6,
	UserInputRecorderUIController_StartRecording_mFAC6974D4BF41107B75BA45259433014A3ED5DE8,
	UserInputRecorderUIController_StopRecording_mB2D99A729598F6B20F5244DBDD7767BEE5AD9C87,
	UserInputRecorderUIController_RecordingUI_Reset_m3C9EE2D4DCAFAE8729541FCCB28794CCB2EFFD04,
	UserInputRecorderUIController_LoadData_mA6F77791E1A8E34FCACF97C7DB5ACD3CEC35FA32,
	UserInputRecorderUIController_ReplayUI_SetActive_m9E796554BEE3F24703AB2601B6C831852014F107,
	UserInputRecorderUIController_StartReplay_m31728EA29978ACCD9814ED03915EB61C5C6F7BB7,
	UserInputRecorderUIController_PauseReplay_mD5C6F4C72418D514A62DC8F069D05666D03F5FBD,
	UserInputRecorderUIController_ResetPlayback_m3A5DAD96B6B195932FE40557E203A89D634A132A,
	UserInputRecorderUIController__ctor_m12D57C4EC7ABA1263A66F606BE36AFC02DE2AE6D,
	DirectionalIndicatorController_OnBecameInvisible_m81BAC96F24A8B8FD38A5A1B399194F4E2E9CF353,
	DirectionalIndicatorController__ctor_mAF93D393485CD7937E4F0A049CAE0D80D17AA0C2,
	ExplodeViewController_set_IsPunEnabled_m9B9F54E404B03478F23B116EBDA6D22729947B1C,
	ExplodeViewController_Start_mEBCA23758FC99609617CEE99E5CC6AD1A8E8DD28,
	ExplodeViewController_Update_m68272F7CABAE057B7402902EACC742A5FD11B266,
	ExplodeViewController_ToggleExplodedView_m59FB613ADA832339B088AF41B27DF25C59EB6E68,
	ExplodeViewController_Toggle_mC19D29CA70BFBE320678DC27565EED8AE3775011,
	ExplodeViewController_add_OnToggleExplodedView_m7B7C12027D19E16A6E136FB760FB843493F87FB5,
	ExplodeViewController_remove_OnToggleExplodedView_m6D5BED15516C033624058FA746BD3E7A630B738B,
	ExplodeViewController__ctor_m6B9698AB8508AB45BA9EFE8C003A368F956A39C9,
	ExplodeViewControllerDelegate__ctor_m00CF199628C294EC4C78DB6BCFE9CA99F31E1187,
	ExplodeViewControllerDelegate_Invoke_m9475EC90F8EABC05D3124071E6AC55FA93C56D16,
	ExplodeViewControllerDelegate_BeginInvoke_m4CE6E375D997DAAA0E3FAFF36F4700838C51D958,
	ExplodeViewControllerDelegate_EndInvoke_m073ACDB10BEA116E55E112E082F114D15ADECD76,
	PartAssemblyController_set_IsPunEnabled_m3B2755FC9F412A9A80F00E881A43C9E3D71259BD,
	PartAssemblyController_Start_m7662EBE3717584F93527DFF47EBFF7B17E3CAE74,
	PartAssemblyController_SetPlacement_m3753B2A0342CD8A54914FFAD1B38D887F1C504C9,
	PartAssemblyController_Set_m78C7CE4286BD0961DA1318A24C6EA8692CAF18CF,
	PartAssemblyController_ResetPlacement_m87A7AB3C4C5D3C539EDAB43011E775BC5C6D1646,
	PartAssemblyController_Reset_m3F1C2A8DEC7F886D75DB1B37F406AA7932399AB5,
	PartAssemblyController_CheckPlacement_mCD2226DB9D0976415A580C7B4D161A5B0C087D96,
	PartAssemblyController_add_OnResetPlacement_m05594F373CAAF81B52DCE6B53F7080535AF36A6C,
	PartAssemblyController_remove_OnResetPlacement_mED257FFBB650024D91B8441D3BD45A8173FCFED2,
	PartAssemblyController_add_OnSetPlacement_mB02D746A05D3A8CF8260832887C55D53D74649DC,
	PartAssemblyController_remove_OnSetPlacement_m6EDB130CABBA27EF6539D4A22EF2B451462458BA,
	PartAssemblyController__ctor_m4C1B83FE2E3CF9F06A2AF8F5D366BBCDECA4B5AC,
	PartAssemblyControllerDelegate__ctor_m7F2B8D7A6D04834696B8F6F71E84D644D99B68EF,
	PartAssemblyControllerDelegate_Invoke_mC5F3437F064E32947FFAB510C33A36A1AD8A059C,
	PartAssemblyControllerDelegate_BeginInvoke_m7671440389FDAA059E71ADFF712A82E1AD7B8CEB,
	PartAssemblyControllerDelegate_EndInvoke_m09C63B12E14C3681A69C6DC62AFFE9C77685B03C,
	U3CCheckPlacementU3Ed__25__ctor_mD99D1F68A1B42D45D5E0303134EB8CDEF442F9B0,
	U3CCheckPlacementU3Ed__25_System_IDisposable_Dispose_m0A3FD3FF6019150BD5AA2392C53BC217DB95477F,
	U3CCheckPlacementU3Ed__25_MoveNext_mBEFDAA4E1B5E6C090C1C516910FE692C516319AE,
	U3CCheckPlacementU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE0F540979B60424322B424EC85CC6A7F660C014C,
	U3CCheckPlacementU3Ed__25_System_Collections_IEnumerator_Reset_m9CFCADBFA8E2719A5D2EAED052816115B65083D4,
	U3CCheckPlacementU3Ed__25_System_Collections_IEnumerator_get_Current_mC1774CBAA3E10DA18E8343A526ACBA70553C607B,
	PlacementHintsController_set_IsPunEnabled_m1364F13B088427C7258B0AED579757B594B74A0E,
	PlacementHintsController_Start_m306723AF3759666445826208B8BD76902741002F,
	PlacementHintsController_TogglePlacementHints_mF5A22F14A17D362A2FAE71EFD314A4BB14D7A28E,
	PlacementHintsController_Toggle_m5285332F7C240D90986D60F15827EC5718A9FE7D,
	PlacementHintsController_add_OnTogglePlacementHints_m4BE517DA85E4BC9F742A80F7863D428A2F4DC931,
	PlacementHintsController_remove_OnTogglePlacementHints_mED93E45F3C670BA58DDF275DE9C576B48D51024C,
	PlacementHintsController__ctor_m789398D4FEC0B3548A616A086A9CD5121B51DED0,
	PlacementHintsControllerDelegate__ctor_m6483805F57E4800C0D83C9D905A5C2CE71432B84,
	PlacementHintsControllerDelegate_Invoke_mFB26FE30BE1AE37EF71E6B55861A49F1125F9554,
	PlacementHintsControllerDelegate_BeginInvoke_m29B9BE46FFF16EF20BC950660E42F1E0FE71F841,
	PlacementHintsControllerDelegate_EndInvoke_m158B6288E2B765CD86F64CB51EBE247426631117,
	DebugWindow_Start_mE4DB4982DB85A70C2B2E0DB3528249AC0F326FA2,
	DebugWindow_OnDestroy_mCF6E877DFC0EFBA82A32169CAA288EC49FDA5775,
	DebugWindow_HandleLog_m979E071474C595A8B1848C110C45F0B946EE5BBC,
	DebugWindow__ctor_m283AC06C16E0FB054BF57249D8CB64F24E82FA2D,
	DisableDiagnosticsSystem__ctor_m586116B337F12DF5FE5CB804EECAD6D504D33DB6,
};
extern void U3CStartAzureSessionU3Ed__11_MoveNext_m096E8B5FFFFF21934958199F5E704B0A35030F86_AdjustorThunk (void);
extern void U3CStartAzureSessionU3Ed__11_SetStateMachine_mC14F49D42F4196CE29AC0BB8D170C8822B39EDBA_AdjustorThunk (void);
extern void U3CStopAzureSessionU3Ed__12_MoveNext_mB56F54C704AA91C36474482A1D72AD4ADCCECDBF_AdjustorThunk (void);
extern void U3CStopAzureSessionU3Ed__12_SetStateMachine_m7026DDC7EDB620D4ADDD1EC4AB67208C3067FEC1_AdjustorThunk (void);
extern void U3CCreateAzureAnchorU3Ed__13_MoveNext_mC928D721B690A52C63E28A6F87DCA0B85C4016EB_AdjustorThunk (void);
extern void U3CCreateAzureAnchorU3Ed__13_SetStateMachine_m1CB417E804D0F9776B38FE512446F426683AEC70_AdjustorThunk (void);
extern void U3CDeleteAzureAnchorU3Ed__16_MoveNext_m57179936926C706B648D169308C85A22C5F69FD9_AdjustorThunk (void);
extern void U3CDeleteAzureAnchorU3Ed__16_SetStateMachine_m81A308F54E85613A92D46E6623D04E5ADFD383C5_AdjustorThunk (void);
extern void U3CStartProgressBehaviorU3Ed__5_MoveNext_m843CD393E4AEA0499AC8C62E669E7FE3FE902BFB_AdjustorThunk (void);
extern void U3CStartProgressBehaviorU3Ed__5_SetStateMachine_m96AD4A4F814E2FF54ADAC5BE1C8586F426D9754A_AdjustorThunk (void);
extern void U3CStartProgressBehaviorU3Ed__8_MoveNext_m62CDA140A7821419665E435C3EA5D8FB59D572AF_AdjustorThunk (void);
extern void U3CStartProgressBehaviorU3Ed__8_SetStateMachine_m19D69CC62B297B3B0B8BAD06E33CEDF96E276168_AdjustorThunk (void);
extern void U3CAsyncMethodU3Ed__9_MoveNext_m575F58B2568865FC9699CCBF4B5BA4F73A3416DE_AdjustorThunk (void);
extern void U3CAsyncMethodU3Ed__9_SetStateMachine_m0424443862F055A35A9F8D61AAD3A0CD2E1D4270_AdjustorThunk (void);
extern void U3CStartProgressBehaviorU3Ed__4_MoveNext_m9A41723CF8D8E4172E0C693C410C7CA572BA0F70_AdjustorThunk (void);
extern void U3CStartProgressBehaviorU3Ed__4_SetStateMachine_m741306E6BDBB3CB825E7EDD5CD8966E7E1C2E19B_AdjustorThunk (void);
extern void U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m62460AC0CF68DFACF7EC1172E850097F9DEE5645_AdjustorThunk (void);
extern void U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m2DAE57EA7D64EB02ACB7EDF19035F855583D4439_AdjustorThunk (void);
extern void U3CCreateNewLogFileU3Ed__14_MoveNext_mBE68DC2547DAEAB02399255517DD3DABB152AFCF_AdjustorThunk (void);
extern void U3CCreateNewLogFileU3Ed__14_SetStateMachine_mBB528CB8977F8797016496EBEFF3139CC717A28C_AdjustorThunk (void);
extern void U3CLoadLogsU3Ed__21_MoveNext_m465394392A1CC40B1F76EF054E462327CB481AB9_AdjustorThunk (void);
extern void U3CLoadLogsU3Ed__21_SetStateMachine_m1FEF9DE9FB018C66BF156F8411185C92ACC6CF57_AdjustorThunk (void);
extern void U3CSaveLogsU3Ed__22_MoveNext_m3FB430EA63A9310C00C2B2A80A0200677EB671B5_AdjustorThunk (void);
extern void U3CSaveLogsU3Ed__22_SetStateMachine_m50C0515F078C93C7AA07C7F4B639D1816AE84C3F_AdjustorThunk (void);
extern void U3CUWP_LoadU3Ed__12_MoveNext_m9D85D0A3795968BEC8AA55D56467300F3DD1F7C9_AdjustorThunk (void);
extern void U3CUWP_LoadU3Ed__12_SetStateMachine_m2C4B3C455A8E03E867BE61D5B1BA0E4A63FC7DB8_AdjustorThunk (void);
extern void U3CUWP_LoadNewFileU3Ed__13_MoveNext_m3113DF5B967063CD4CA6542ED9AAF57F9BB6E615_AdjustorThunk (void);
extern void U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m08D7E1B935DF8D5DDE59EE125E5457EDF6EC3426_AdjustorThunk (void);
extern void U3CUWP_FileExistsU3Ed__14_MoveNext_m9C9E5129BAEAF23D1AE693F6D4C27BDA11AF5359_AdjustorThunk (void);
extern void U3CUWP_FileExistsU3Ed__14_SetStateMachine_m40DA4924B55A8B15F9E4BAB780B72948A8FD0C1D_AdjustorThunk (void);
extern void U3CUWP_ReadDataU3Ed__15_MoveNext_mC6415C909253B839D77B2FA26344FBAB31987CBD_AdjustorThunk (void);
extern void U3CUWP_ReadDataU3Ed__15_SetStateMachine_m7FB0514A5F50E95AEE67876D43CB68734CB6F2D4_AdjustorThunk (void);
extern void U3CLoadInUWPU3Ed__21_MoveNext_m2CD40348FA310488ABBB418C2B433AB9C27D61C0_AdjustorThunk (void);
extern void U3CLoadInUWPU3Ed__21_SetStateMachine_mA2CB12B355F31796EF31B4B349AD43BEC4AA16E4_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[34] = 
{
	{ 0x06000053, U3CStartAzureSessionU3Ed__11_MoveNext_m096E8B5FFFFF21934958199F5E704B0A35030F86_AdjustorThunk },
	{ 0x06000054, U3CStartAzureSessionU3Ed__11_SetStateMachine_mC14F49D42F4196CE29AC0BB8D170C8822B39EDBA_AdjustorThunk },
	{ 0x06000055, U3CStopAzureSessionU3Ed__12_MoveNext_mB56F54C704AA91C36474482A1D72AD4ADCCECDBF_AdjustorThunk },
	{ 0x06000056, U3CStopAzureSessionU3Ed__12_SetStateMachine_m7026DDC7EDB620D4ADDD1EC4AB67208C3067FEC1_AdjustorThunk },
	{ 0x06000059, U3CCreateAzureAnchorU3Ed__13_MoveNext_mC928D721B690A52C63E28A6F87DCA0B85C4016EB_AdjustorThunk },
	{ 0x0600005A, U3CCreateAzureAnchorU3Ed__13_SetStateMachine_m1CB417E804D0F9776B38FE512446F426683AEC70_AdjustorThunk },
	{ 0x0600005B, U3CDeleteAzureAnchorU3Ed__16_MoveNext_m57179936926C706B648D169308C85A22C5F69FD9_AdjustorThunk },
	{ 0x0600005C, U3CDeleteAzureAnchorU3Ed__16_SetStateMachine_m81A308F54E85613A92D46E6623D04E5ADFD383C5_AdjustorThunk },
	{ 0x060002C3, U3CStartProgressBehaviorU3Ed__5_MoveNext_m843CD393E4AEA0499AC8C62E669E7FE3FE902BFB_AdjustorThunk },
	{ 0x060002C4, U3CStartProgressBehaviorU3Ed__5_SetStateMachine_m96AD4A4F814E2FF54ADAC5BE1C8586F426D9754A_AdjustorThunk },
	{ 0x060002CA, U3CStartProgressBehaviorU3Ed__8_MoveNext_m62CDA140A7821419665E435C3EA5D8FB59D572AF_AdjustorThunk },
	{ 0x060002CB, U3CStartProgressBehaviorU3Ed__8_SetStateMachine_m19D69CC62B297B3B0B8BAD06E33CEDF96E276168_AdjustorThunk },
	{ 0x060002CC, U3CAsyncMethodU3Ed__9_MoveNext_m575F58B2568865FC9699CCBF4B5BA4F73A3416DE_AdjustorThunk },
	{ 0x060002CD, U3CAsyncMethodU3Ed__9_SetStateMachine_m0424443862F055A35A9F8D61AAD3A0CD2E1D4270_AdjustorThunk },
	{ 0x060002D0, U3CStartProgressBehaviorU3Ed__4_MoveNext_m9A41723CF8D8E4172E0C693C410C7CA572BA0F70_AdjustorThunk },
	{ 0x060002D1, U3CStartProgressBehaviorU3Ed__4_SetStateMachine_m741306E6BDBB3CB825E7EDD5CD8966E7E1C2E19B_AdjustorThunk },
	{ 0x0600047B, U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m62460AC0CF68DFACF7EC1172E850097F9DEE5645_AdjustorThunk },
	{ 0x0600047C, U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m2DAE57EA7D64EB02ACB7EDF19035F855583D4439_AdjustorThunk },
	{ 0x06000493, U3CCreateNewLogFileU3Ed__14_MoveNext_mBE68DC2547DAEAB02399255517DD3DABB152AFCF_AdjustorThunk },
	{ 0x06000494, U3CCreateNewLogFileU3Ed__14_SetStateMachine_mBB528CB8977F8797016496EBEFF3139CC717A28C_AdjustorThunk },
	{ 0x06000495, U3CLoadLogsU3Ed__21_MoveNext_m465394392A1CC40B1F76EF054E462327CB481AB9_AdjustorThunk },
	{ 0x06000496, U3CLoadLogsU3Ed__21_SetStateMachine_m1FEF9DE9FB018C66BF156F8411185C92ACC6CF57_AdjustorThunk },
	{ 0x06000497, U3CSaveLogsU3Ed__22_MoveNext_m3FB430EA63A9310C00C2B2A80A0200677EB671B5_AdjustorThunk },
	{ 0x06000498, U3CSaveLogsU3Ed__22_SetStateMachine_m50C0515F078C93C7AA07C7F4B639D1816AE84C3F_AdjustorThunk },
	{ 0x060004E9, U3CUWP_LoadU3Ed__12_MoveNext_m9D85D0A3795968BEC8AA55D56467300F3DD1F7C9_AdjustorThunk },
	{ 0x060004EA, U3CUWP_LoadU3Ed__12_SetStateMachine_m2C4B3C455A8E03E867BE61D5B1BA0E4A63FC7DB8_AdjustorThunk },
	{ 0x060004EB, U3CUWP_LoadNewFileU3Ed__13_MoveNext_m3113DF5B967063CD4CA6542ED9AAF57F9BB6E615_AdjustorThunk },
	{ 0x060004EC, U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m08D7E1B935DF8D5DDE59EE125E5457EDF6EC3426_AdjustorThunk },
	{ 0x060004ED, U3CUWP_FileExistsU3Ed__14_MoveNext_m9C9E5129BAEAF23D1AE693F6D4C27BDA11AF5359_AdjustorThunk },
	{ 0x060004EE, U3CUWP_FileExistsU3Ed__14_SetStateMachine_m40DA4924B55A8B15F9E4BAB780B72948A8FD0C1D_AdjustorThunk },
	{ 0x060004EF, U3CUWP_ReadDataU3Ed__15_MoveNext_mC6415C909253B839D77B2FA26344FBAB31987CBD_AdjustorThunk },
	{ 0x060004F0, U3CUWP_ReadDataU3Ed__15_SetStateMachine_m7FB0514A5F50E95AEE67876D43CB68734CB6F2D4_AdjustorThunk },
	{ 0x060004F1, U3CLoadInUWPU3Ed__21_MoveNext_m2CD40348FA310488ABBB418C2B433AB9C27D61C0_AdjustorThunk },
	{ 0x060004F2, U3CLoadInUWPU3Ed__21_SetStateMachine_mA2CB12B355F31796EF31B4B349AD43BEC4AA16E4_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1369] = 
{
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	3477,
	6358,
	7782,
	5609,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	7925,
	3471,
	7925,
	2692,
	6358,
	3471,
	7925,
	2692,
	6358,
	3471,
	7925,
	2692,
	6358,
	3471,
	7925,
	2692,
	6358,
	3471,
	7925,
	2692,
	6358,
	3471,
	7925,
	2692,
	6358,
	3471,
	7925,
	2692,
	6358,
	3471,
	7925,
	2692,
	6358,
	7925,
	6358,
	7925,
	6358,
	7925,
	7925,
	7925,
	6358,
	7925,
	6358,
	7925,
	7925,
	7925,
	11763,
	7925,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6308,
	7925,
	7639,
	7925,
	7782,
	7925,
	7782,
	7925,
	7925,
	6358,
	7925,
	7925,
	7925,
	7925,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	1530,
	7925,
	1530,
	7925,
	7782,
	6358,
	7782,
	6358,
	7782,
	6358,
	7782,
	6358,
	7782,
	6358,
	7925,
	7925,
	6358,
	6358,
	3557,
	3557,
	1722,
	1722,
	1749,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	7925,
	5609,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	6358,
	5609,
	5609,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7782,
	7782,
	7925,
	11763,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	6308,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6308,
	7925,
	7925,
	7925,
	3557,
	3557,
	1722,
	1722,
	1749,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	6358,
	7925,
	6358,
	6358,
	6358,
	6358,
	6308,
	7925,
	7925,
	7925,
	7925,
	6308,
	7925,
	7925,
	7925,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	6358,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	6358,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	6358,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	6358,
	7782,
	7925,
	7925,
	2442,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	5609,
	7782,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7782,
	6358,
	7639,
	6214,
	7925,
	6358,
	6358,
	6358,
	6358,
	7925,
	7925,
	7925,
	6358,
	7925,
	7925,
	7925,
	7925,
	6358,
	7925,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	6358,
	5603,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	4810,
	7925,
	6308,
	7925,
	6358,
	6358,
	3477,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	1752,
	1244,
	6474,
	7925,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	7782,
	7782,
	7925,
	11763,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7782,
	6358,
	7782,
	6358,
	7782,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	7925,
	7925,
	6358,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	7925,
	7925,
	6358,
	6358,
	6358,
	7925,
	6358,
	7925,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7841,
	6409,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	0,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6214,
	7925,
	7925,
	6358,
	7925,
	7925,
	3477,
	7925,
	7925,
	7782,
	6358,
	7782,
	6358,
	7731,
	6308,
	7639,
	6214,
	7731,
	6308,
	7782,
	6358,
	7925,
	7925,
	2690,
	6358,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7782,
	6358,
	6308,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7731,
	6308,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	0,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	7782,
	7782,
	6358,
	7925,
	6358,
	6358,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	6358,
	7782,
	7782,
	6358,
	7925,
	6358,
	6358,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	7925,
	7782,
	1242,
	7925,
	6308,
	7925,
	7639,
	7925,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	7782,
	6358,
	7782,
	6358,
	7782,
	6358,
	7782,
	6358,
	7782,
	6358,
	7782,
	6358,
	7925,
	7925,
	7925,
	0,
	7925,
	7925,
	7925,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	7925,
	7925,
	7925,
	5595,
	7925,
	7925,
	6358,
	7925,
	6358,
	7925,
	7925,
	7925,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	7925,
	7925,
	7925,
	7782,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	7925,
	7913,
	7925,
	6409,
	6409,
	7925,
	7925,
	6214,
	7925,
	7925,
	7925,
	7925,
	7925,
	6474,
	7925,
	6409,
	7925,
	7925,
	7925,
	7925,
	7925,
	7782,
	6358,
	6409,
	6409,
	7925,
	5825,
	7925,
	7925,
	5594,
	0,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7639,
	7925,
	1523,
	5163,
	7925,
	7925,
	7925,
	7910,
	3541,
	7639,
	7925,
	7782,
	6358,
	7639,
	7925,
	6409,
	1523,
	7925,
	5163,
	7925,
	7925,
	3541,
	7639,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	7925,
	7782,
	7731,
	7731,
	7731,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	6358,
	7925,
	7925,
	7639,
	7639,
	7639,
	5833,
	7925,
	7925,
	7925,
	7925,
	6474,
	7841,
	5718,
	7639,
	6474,
	7925,
	7925,
	7925,
	6474,
	1396,
	6358,
	7925,
	7925,
	6358,
	7925,
	7925,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	5718,
	7925,
	7925,
	7639,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7782,
	7782,
	1787,
	7925,
	7925,
	7925,
	7641,
	6358,
	6358,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7782,
	7782,
	7639,
	7925,
	7925,
	6217,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	7925,
	3471,
	7925,
	2692,
	6358,
	7925,
	7925,
	6214,
	7925,
	7782,
	7925,
	7925,
	6474,
	3609,
	1801,
	5624,
	1509,
	1418,
	7782,
	7782,
	3891,
	7925,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	6358,
	6358,
	6358,
	6358,
	7925,
	7925,
	6474,
	3892,
	2733,
	7925,
	5720,
	7925,
	7925,
	7925,
	7925,
	7925,
	11717,
	11540,
	7925,
	5609,
	6358,
	7925,
	7925,
	7925,
	7782,
	6358,
	6358,
	7925,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6409,
	3538,
	1769,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	4636,
	7925,
	7925,
	7925,
	7925,
	3619,
	3619,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6358,
	5609,
	7925,
	11763,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	1523,
	7925,
	7925,
	3477,
	7925,
	7925,
	7639,
	7925,
	7925,
	7925,
	7925,
	7925,
	7913,
	7925,
	1544,
	6409,
	1523,
	6358,
	6358,
	6358,
	6358,
	7782,
	6358,
	7925,
	7782,
	6358,
	6358,
	10816,
	7925,
	7925,
	7925,
	7925,
	11717,
	11540,
	7925,
	7925,
	7925,
	11717,
	7925,
	7925,
	3456,
	7925,
	11327,
	11327,
	10482,
	9701,
	0,
	10646,
	10570,
	11327,
	10495,
	9284,
	10826,
	9982,
	9982,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	7925,
	7925,
	7925,
	7841,
	7841,
	7925,
	6409,
	6409,
	6409,
	5720,
	7664,
	7664,
	7925,
	7925,
	7925,
	2692,
	1432,
	1523,
	7925,
	11540,
	0,
	7782,
	6358,
	3477,
	3477,
	7925,
	7925,
	7782,
	7925,
	6358,
	7925,
	6358,
	7925,
	6358,
	0,
	0,
	0,
	0,
	6358,
	6358,
	7782,
	0,
	0,
	7925,
	7925,
	7925,
	7782,
	5603,
	4513,
	7925,
	7925,
	7782,
	7782,
	7782,
	7925,
	7925,
	7925,
	6358,
	7925,
	6358,
	7925,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	1602,
	1602,
	7925,
	6180,
	6180,
	6308,
	507,
	2828,
	2828,
	1097,
	1097,
	504,
	2442,
	6382,
	3893,
	7925,
	7639,
	7731,
	6308,
	7925,
	7925,
	7925,
	6214,
	7925,
	7782,
	1493,
	7925,
	7782,
	7782,
	1493,
	7925,
	7925,
	7925,
	7782,
	5609,
	2692,
	5609,
	6358,
	1079,
	5718,
	7925,
	7925,
	7925,
	7782,
	6214,
	7639,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	3469,
	7925,
	7925,
	7925,
	3160,
	6358,
	7782,
	5613,
	5613,
	3890,
	165,
	7639,
	6358,
	3477,
	3477,
	88,
	7925,
	7925,
	7925,
	7782,
	7925,
	6358,
	7925,
	6358,
	7925,
	6358,
	7925,
	6358,
	7925,
	6358,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	11717,
	7782,
	7782,
	2692,
	7782,
	2690,
	11327,
	1752,
	6358,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	7925,
	6214,
	7925,
	6214,
	7925,
	7925,
	2846,
	7925,
	7925,
	7925,
	6214,
	7925,
	7925,
	7925,
	7925,
	6358,
	6358,
	7925,
	3471,
	7925,
	2692,
	6358,
	6214,
	7925,
	7925,
	7925,
	7925,
	7925,
	7782,
	6358,
	6358,
	6358,
	6358,
	7925,
	3471,
	7925,
	2692,
	6358,
	6308,
	7925,
	7639,
	7782,
	7925,
	7782,
	6214,
	7925,
	7925,
	7925,
	6358,
	6358,
	7925,
	3471,
	7925,
	2692,
	6358,
	7925,
	7925,
	1749,
	7925,
	7925,
};
static const Il2CppTokenRangePair s_rgctxIndices[7] = 
{
	{ 0x020000CD, { 16, 4 } },
	{ 0x020000CE, { 20, 13 } },
	{ 0x06000227, { 0, 1 } },
	{ 0x06000261, { 1, 2 } },
	{ 0x06000308, { 3, 6 } },
	{ 0x0600044C, { 9, 2 } },
	{ 0x0600046F, { 11, 5 } },
};
extern const uint32_t g_rgctx_PointerUtils_GetPointerBehavior_TisT_t59C52AE5003CC8F162005149D8E67CEC2550E180_m72C04E09013B8C56F3A5F31908D803E569341EA0;
extern const uint32_t g_rgctx_GameObject_AddComponent_TisT_t027252E7A8FED42D76F6309C82F9A31C5A89454A_m62A04AA144013D94688B0F523F45F560B7A0ACFB;
extern const uint32_t g_rgctx_T_t027252E7A8FED42D76F6309C82F9A31C5A89454A;
extern const uint32_t g_rgctx_EqualityComparer_1_get_Default_m0E235AD172C41A00AAEB4769A0BAB1E336867DCE;
extern const uint32_t g_rgctx_EqualityComparer_1_t484373C319C65E4D996B7D3CB8998CD7B1F68834;
extern const uint32_t g_rgctx_EqualityComparer_1_t484373C319C65E4D996B7D3CB8998CD7B1F68834;
extern const uint32_t g_rgctx_TU26_t7B1A73418A6569A493603BB790BA90D5046D5DFF;
extern const uint32_t g_rgctx_T_t288B02B4504219A670A1EAB74FBE6B087A2EB645;
extern const uint32_t g_rgctx_EqualityComparer_1_Equals_m0822E872F3EEF82E80D0A1C702F35ECAC160D211;
extern const uint32_t g_rgctx_TU5BU5D_tB4DD69C076957E4D3DD5DFA51D1436ECD83680BF;
extern const uint32_t g_rgctx_T_t2B9F73B59FE73B3182FED1A3F6F18C186EFC9C10;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass1_0_1_t29C600D064813778BFF0E4DF0FFF5404C4818BC6;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass1_0_1__ctor_m6C68DD2875AF2B7E2BA1D997E6B08A667F62FC95;
extern const uint32_t g_rgctx_Func_1_t2FE1E9B54DB9349543EFC3889EADA7AF1F69EBDA;
extern const uint32_t g_rgctx_T_tB55A26A94C19BAF48ED8B14A4ED2CF19CCEDE0D8;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass1_0_1_U3CRunSyncU3Eb__0_m7CBF5F3DEFB8859BB1644B46461AF8FC2F624A1D;
extern const uint32_t g_rgctx_U3CU3CRunSyncU3Eb__0U3Ed_tF03F472BE8C21AE71C06B3DB76F4A4D1B03639E0;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass1_0_1_tDED68484D245B0AB07651BF0CA54E0BC610C3A6D;
extern const uint32_t g_rgctx_AsyncVoidMethodBuilder_Start_TisU3CU3CRunSyncU3Eb__0U3Ed_tF03F472BE8C21AE71C06B3DB76F4A4D1B03639E0_m92B4BC2632AD9B3D7033235AE21E6BD36990541B;
extern const uint32_t g_rgctx_U3CU3CRunSyncU3Eb__0U3EdU26_tBE5387D9E9C8CE86903B8CD37249D18A84188F56;
extern const uint32_t g_rgctx_U3CU3CRunSyncU3Eb__0U3Ed_tE77782D3E8D25CF1221F302BA46EF2E4D6C9D295;
extern const uint32_t g_rgctx_U3CU3Ec__DisplayClass1_0_1_t434A34AF6A49124ED22958D32379807950797F41;
extern const uint32_t g_rgctx_Func_1_t81A708C6947FCEDE5A9AA0F7B91D466EDE9E6F8D;
extern const uint32_t g_rgctx_Func_1_Invoke_m8AD389C4F12CFB14C03550BEA818A56774406F9A;
extern const uint32_t g_rgctx_Task_1_tCF067F3819204F7C0C935B3481B61B82E1AE45F4;
extern const uint32_t g_rgctx_Task_1_GetAwaiter_mA2DC6EEDF9DD6B40AFD7FA23125E85F3DA24C361;
extern const uint32_t g_rgctx_TaskAwaiter_1_tC89F64E74E8282C77943BB4293FB62F2A80CCE55;
extern const uint32_t g_rgctx_TaskAwaiter_1_get_IsCompleted_m04AC6EEE69237C9FE7DB81A3ED41160BED5BBBA9;
extern const uint32_t g_rgctx_AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_1_tC89F64E74E8282C77943BB4293FB62F2A80CCE55_TisU3CU3CRunSyncU3Eb__0U3Ed_tE77782D3E8D25CF1221F302BA46EF2E4D6C9D295_m18DD586B386C30EF2E00C865D07EA8AA7F2A116C;
extern const uint32_t g_rgctx_TaskAwaiter_1U26_t766FCC4781EAC81768B5432ADB8982A13E061E72;
extern const uint32_t g_rgctx_U3CU3CRunSyncU3Eb__0U3EdU26_tF2FA36506843E88D62CB9DC85FC0B8721D368078;
extern const uint32_t g_rgctx_TaskAwaiter_1_GetResult_m82636F6D078E237025070B93D18271A5170E24B4;
extern const uint32_t g_rgctx_T_tCC0194E4AE89B7C955C2BFB2EA5E78CD075905CC;
static const Il2CppRGCTXDefinition s_rgctxValues[33] = 
{
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_PointerUtils_GetPointerBehavior_TisT_t59C52AE5003CC8F162005149D8E67CEC2550E180_m72C04E09013B8C56F3A5F31908D803E569341EA0 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_GameObject_AddComponent_TisT_t027252E7A8FED42D76F6309C82F9A31C5A89454A_m62A04AA144013D94688B0F523F45F560B7A0ACFB },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t027252E7A8FED42D76F6309C82F9A31C5A89454A },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_get_Default_m0E235AD172C41A00AAEB4769A0BAB1E336867DCE },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t484373C319C65E4D996B7D3CB8998CD7B1F68834 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_EqualityComparer_1_t484373C319C65E4D996B7D3CB8998CD7B1F68834 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU26_t7B1A73418A6569A493603BB790BA90D5046D5DFF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t288B02B4504219A670A1EAB74FBE6B087A2EB645 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_EqualityComparer_1_Equals_m0822E872F3EEF82E80D0A1C702F35ECAC160D211 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TU5BU5D_tB4DD69C076957E4D3DD5DFA51D1436ECD83680BF },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_t2B9F73B59FE73B3182FED1A3F6F18C186EFC9C10 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass1_0_1_t29C600D064813778BFF0E4DF0FFF5404C4818BC6 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass1_0_1__ctor_m6C68DD2875AF2B7E2BA1D997E6B08A667F62FC95 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t2FE1E9B54DB9349543EFC3889EADA7AF1F69EBDA },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tB55A26A94C19BAF48ED8B14A4ED2CF19CCEDE0D8 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_U3CU3Ec__DisplayClass1_0_1_U3CRunSyncU3Eb__0_m7CBF5F3DEFB8859BB1644B46461AF8FC2F624A1D },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3CRunSyncU3Eb__0U3Ed_tF03F472BE8C21AE71C06B3DB76F4A4D1B03639E0 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass1_0_1_tDED68484D245B0AB07651BF0CA54E0BC610C3A6D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncVoidMethodBuilder_Start_TisU3CU3CRunSyncU3Eb__0U3Ed_tF03F472BE8C21AE71C06B3DB76F4A4D1B03639E0_m92B4BC2632AD9B3D7033235AE21E6BD36990541B },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3CRunSyncU3Eb__0U3EdU26_tBE5387D9E9C8CE86903B8CD37249D18A84188F56 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3CRunSyncU3Eb__0U3Ed_tE77782D3E8D25CF1221F302BA46EF2E4D6C9D295 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3Ec__DisplayClass1_0_1_t434A34AF6A49124ED22958D32379807950797F41 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Func_1_t81A708C6947FCEDE5A9AA0F7B91D466EDE9E6F8D },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Func_1_Invoke_m8AD389C4F12CFB14C03550BEA818A56774406F9A },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_Task_1_tCF067F3819204F7C0C935B3481B61B82E1AE45F4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_Task_1_GetAwaiter_mA2DC6EEDF9DD6B40AFD7FA23125E85F3DA24C361 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TaskAwaiter_1_tC89F64E74E8282C77943BB4293FB62F2A80CCE55 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TaskAwaiter_1_get_IsCompleted_m04AC6EEE69237C9FE7DB81A3ED41160BED5BBBA9 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_1_tC89F64E74E8282C77943BB4293FB62F2A80CCE55_TisU3CU3CRunSyncU3Eb__0U3Ed_tE77782D3E8D25CF1221F302BA46EF2E4D6C9D295_m18DD586B386C30EF2E00C865D07EA8AA7F2A116C },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_TaskAwaiter_1U26_t766FCC4781EAC81768B5432ADB8982A13E061E72 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_U3CU3CRunSyncU3Eb__0U3EdU26_tF2FA36506843E88D62CB9DC85FC0B8721D368078 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_TaskAwaiter_1_GetResult_m82636F6D078E237025070B93D18271A5170E24B4 },
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_T_tCC0194E4AE89B7C955C2BFB2EA5E78CD075905CC },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1369,
	s_methodPointers,
	34,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	7,
	s_rgctxIndices,
	33,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
