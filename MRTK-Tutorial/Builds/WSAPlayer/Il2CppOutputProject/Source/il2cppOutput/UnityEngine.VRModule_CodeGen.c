﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.XR.XRSettings::set_renderViewportScale(System.Single)
extern void XRSettings_set_renderViewportScale_m96E308EEAE4B92F92ACD2866E3958070FA3E5313 (void);
// 0x00000002 System.Void UnityEngine.XR.XRSettings::set_renderViewportScaleInternal(System.Single)
extern void XRSettings_set_renderViewportScaleInternal_m938F25CB6C2F43493823E5C00A9A42EC721110B3 (void);
// 0x00000003 System.Single UnityEngine.XR.XRDevice::get_refreshRate()
extern void XRDevice_get_refreshRate_mCDE089722DEF165AC260B04BBC248620C697A5DD (void);
// 0x00000004 System.Void UnityEngine.XR.XRDevice::DisableAutoXRCameraTracking(UnityEngine.Camera,System.Boolean)
extern void XRDevice_DisableAutoXRCameraTracking_m1243FCAD2AC9C4D5C2E551255A1B2BA266E12A52 (void);
// 0x00000005 System.Void UnityEngine.XR.XRDevice::InvokeDeviceLoaded(System.String)
extern void XRDevice_InvokeDeviceLoaded_mBE2198DE44A72E2F5059566C46B9907D82782790 (void);
static Il2CppMethodPointer s_methodPointers[5] = 
{
	XRSettings_set_renderViewportScale_m96E308EEAE4B92F92ACD2866E3958070FA3E5313,
	XRSettings_set_renderViewportScaleInternal_m938F25CB6C2F43493823E5C00A9A42EC721110B3,
	XRDevice_get_refreshRate_mCDE089722DEF165AC260B04BBC248620C697A5DD,
	XRDevice_DisableAutoXRCameraTracking_m1243FCAD2AC9C4D5C2E551255A1B2BA266E12A52,
	XRDevice_InvokeDeviceLoaded_mBE2198DE44A72E2F5059566C46B9907D82782790,
};
static const int32_t s_InvokerIndices[5] = 
{
	11547,
	11547,
	11747,
	10808,
	11540,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_VRModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VRModule_CodeGenModule = 
{
	"UnityEngine.VRModule.dll",
	5,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
